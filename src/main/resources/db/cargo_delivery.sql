-- DROP TABLE `order`;
-- DROP DATABASE `cargo_delivery`;

CREATE DATABASE IF NOT EXISTS `cargo_delivery`;
USE `cargo_delivery`;

CREATE TABLE IF NOT EXISTS `role` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` ENUM('user', 'manager') NOT NULL,
  PRIMARY KEY (`id`));
  
CREATE TABLE IF NOT EXISTS `wallet` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `value` DOUBLE NOT NULL,
  PRIMARY KEY (`id`)); 

 CREATE TABLE IF NOT EXISTS `account_details` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `surname` VARCHAR(45) NOT NULL,
  `email` VARCHAR(45) NOT NULL,
  `phone` VARCHAR(45) NOT NULL,
  `city` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`)); 

CREATE TABLE IF NOT EXISTS `account` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `login` VARCHAR(45) NOT NULL UNIQUE,
  `password` VARCHAR(45) NOT NULL,
  `role_id` INT NOT NULL,
  `wallet_id` INT NOT NULL,
  `account_details_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`role_id`) REFERENCES `role` (`id`),
  FOREIGN KEY (`wallet_id`) REFERENCES `wallet` (`id`),
  FOREIGN KEY (`account_details_id`) REFERENCES `account_details` (`id`));

CREATE TABLE IF NOT EXISTS `city` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`));
  
CREATE TABLE IF NOT EXISTS `route` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `city_id_from` INT NOT NULL,
  `city_id_to` INT NOT NULL,
  `distance` INT NOT NULL,
  `tariff` DOUBLE NOT NULL,
  UNIQUE(`city_id_from`, `city_id_to`),
  PRIMARY KEY (`id`),
  FOREIGN KEY (`city_id_from`) REFERENCES `city` (`id`) ON DELETE CASCADE,
  FOREIGN KEY (`city_id_to`) REFERENCES `city` (`id`) ON DELETE CASCADE);
  
  CREATE TABLE IF NOT EXISTS `cargo_type` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `ratio` DOUBLE NOT NULL,
  PRIMARY KEY (`id`));
  
  CREATE TABLE IF NOT EXISTS `language` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `code` VARCHAR(15) NOT NULL,
  PRIMARY KEY (`id`));
  
  CREATE TABLE IF NOT EXISTS `cargo_type_translator` (
  `cargo_type_id` INT NOT NULL,
  `language_id` INT NOT NULL,
  `translation` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`cargo_type_id`, `language_id`),
  FOREIGN KEY (`cargo_type_id`) REFERENCES `cargo_type` (`id`) ON DELETE CASCADE,
  FOREIGN KEY (`language_id`) REFERENCES `language` (`id`) ON DELETE CASCADE);
  
CREATE TABLE IF NOT EXISTS `city_translator` (
  `city_id` INT NOT NULL,
  `language_id` INT NOT NULL,
  `translation` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`city_id`, `language_id`),
  FOREIGN KEY (`city_id`) REFERENCES `city` (`id`) ON DELETE CASCADE,
  FOREIGN KEY (`language_id`) REFERENCES `language` (`id`) ON DELETE CASCADE);  

CREATE TABLE IF NOT EXISTS `order` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `account_id` INT NOT NULL,
  `address` VARCHAR(45) NOT NULL,
  `cargo_type_id` INT NOT NULL,
  `weight` DOUBLE NOT NULL,
  `volume` INT NOT NULL,
  `delivery_date` DATE NOT NULL,
  `status` ENUM('new', 'processed', 'paid', 'delivered') NOT NULL,
  `price` DOUBLE NULL,
  `route_id` INT NOT NULL,
  `create_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),  
  FOREIGN KEY (`account_id`) REFERENCES `account` (`id`),
  FOREIGN KEY (`route_id`) REFERENCES `route` (`id`),
  FOREIGN KEY (`cargo_type_id`) REFERENCES `cargo_type` (`id`) ON DELETE CASCADE);