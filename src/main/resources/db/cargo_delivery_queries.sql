-- routs with city names
SELECT route.id, city_from.name AS city_from, city_to.name AS city_to , route.distance, route.tariff 
FROM route 
JOIN city AS city_from ON route.city_id_from = city_from.id
JOIN city AS city_to ON route.city_id_to = city_to.id
-- WHERE city_id_to = 8
-- ORDER BY city_from ASC
LIMIT 15 OFFSET 30;

-- routes with city names by language
SELECT route.id, route.city_id_from, route.city_id_to, route.distance, route.tariff,
city_from.translation AS city_from, city_to.translation AS city_to 
FROM route 
JOIN `city_translator` AS city_from ON `route`.`city_id_from` = city_from.`city_id` AND city_from.`language_id` = 2
JOIN `city_translator` AS city_to ON `route`.`city_id_to` = city_to.`city_id` AND city_to.`language_id` = 2
WHERE city_id_to = 8
ORDER BY city_from ASC
LIMIT 15 OFFSET 0;

-- routs with city names without JOIN operation
SELECT route.id, city_from.name, city_to.name , route.distance, route.tariff 
FROM route, city AS city_from, city AS city_to
WHERE route.city_id_from = city_from.id AND route.city_id_to = city_to.id
ORDER BY route.id DESC;

-- Delete route by id
DELETE FROM `route` WHERE `id` = 99;

-- add user (account + account_details + wallet one transaction)
INSERT `account`(`login`, `password`, `role_id`, `wallet_id`, `account_details_id`) VALUES
('Dmitriy', '12345', (SELECT `id` FROM `role` WHERE `name`='manager'), 1, 1);

INSERT `account_details`(`name`, `surname`, `email`, `phone`, `city`) VALUES
('Дмитрирй', 'Пупков', 'pupkov@gmail.com', '+380974589856', 'Киев');

INSERT `wallet`(`value`) VALUES (0);

-- refill wallet
UPDATE `wallet`
SET `value` = `value` + 120
WHERE `id` = 3;

-- create order
INSERT `order` (`account_id`, `address`, `cargo_type`, `weight`, `volume`, `delivery_date`, `status`, `price`, `route_id`) VALUES
(3,'ул. Кротова 53/21','книги', 30.2, 1, '2023-01-05','new', 420.56, 1);

-- process order
UPDATE `order`
SET `status` = 'processed'
WHERE id = 2;

-- pay for the delivery invoice
UPDATE `order`
SET `status` = 'paid'
WHERE id = 2;

UPDATE `wallet`
SET `value` = `value` - 120
WHERE `id` = 3;

UPDATE `wallet`
SET `value` = `value` + 120
WHERE `id` = 1;

-- find all orders
SELECT `order`.`id`, `order`.`account_id`, `order`.`address`, `order`.`cargo_type_id`,
 `order`.`weight`, `order`.`volume`, `order`.`status`, `order`.`price`, `order`.`delivery_date`, `order`.`route_id`,
 `cargo_type_translator`.`translation` AS cargo_type,
`account_details`.`name`, `account_details`.`surname`,
`city_from`.`name` AS city_from, `city_to`.`name` AS city_to 
FROM `order` 
INNER JOIN `cargo_type_translator` ON `order`.`cargo_type_id` = `cargo_type_translator`.`cargo_type_id` AND `cargo_type_translator`.`language_id` = 2
INNER JOIN `account` ON `order`.`account_id` = `account`.`id`
INNER JOIN `account_details` ON `account`.`account_details_id` = `account_details`.`id`
INNER JOIN `route` ON `order`.`route_id` = `route`.`id`
INNER JOIN `city` AS city_from ON `route`.`city_id_from` = city_from.id
INNER JOIN `city` AS city_to ON `route`.`city_id_to` = city_to.id
ORDER BY `order`.`id`;

-- find all orders with city names
SELECT `order`.`id`, `order`.`account_id`, `order`.`address`, `order`.`cargo_type_id`, 
`order`.`weight`, `order`.`volume`, `order`.`status`, `order`.`price`, `order`.`delivery_date`, `order`.`route_id`,
`cargo_type_translator`.`translation` AS cargo_type,
`city_from`.`name` AS city_from, `city_to`.`name` AS city_to, `route`.`distance` 
FROM `order`
INNER JOIN `cargo_type_translator` ON `order`.`cargo_type_id` = `cargo_type_translator`.`cargo_type_id` AND `cargo_type_translator`.`language_id` = 1 
INNER JOIN `route` ON `order`.`route_id` = `route`.`id`
INNER JOIN `city` AS city_from ON `route`.`city_id_from` = city_from.id
INNER JOIN `city` AS city_to ON `route`.`city_id_to` = city_to.id
WHERE city_from.`name` LIKE '%%' -- AND status = 'delivered'
ORDER BY id ASC;

-- find all orders with transated city names
SELECT `order`.`id`, `order`.`account_id`, `order`.`address`, `order`.`cargo_type_id`,
`order`.`weight`, `order`.`volume`, `order`.`status`, `order`.`price`, `order`.`delivery_date`, `order`.`route_id`, 
`route`.`city_id_from`, `route`.`city_id_to`, 
`cargo_type_translator`.`translation` AS cargo_type, 
city_from.translation AS city_from, city_to.translation AS city_to 
-- `city_from`.`name` AS city_from, `city_to`.`name` AS city_to, `route`.`distance` 
FROM `order` 
INNER JOIN `cargo_type_translator` ON `order`.`cargo_type_id` = `cargo_type_translator`.`cargo_type_id` AND `cargo_type_translator`.`language_id` = 1 
INNER JOIN `route` ON `order`.`route_id` = `route`.`id` 
-- INNER JOIN `city` AS city_from ON `route`.`city_id_from` = city_from.id 
-- INNER JOIN `city` AS city_to ON `route`.`city_id_to` = city_to.id 
JOIN `city_translator` AS city_from ON `route`.`city_id_from` = city_from.`city_id` AND city_from.`language_id` = 1
JOIN `city_translator` AS city_to ON `route`.`city_id_to` = city_to.`city_id` AND city_to.`language_id` = 1
WHERE `order`.`account_id` = 3  
ORDER BY  city_to desc
LIMIT 15 OFFSET 0;

-- find all orders with transated city names by city and direction
SELECT `order`.`id`, `order`.`account_id`, `order`.`address`, `order`.`cargo_type_id`, 
`order`.`weight`, `order`.`volume`, `order`.`status`, `order`.`price`, `order`.`delivery_date`, `order`.`route_id`, 
`cargo_type_translator`.`translation` AS cargo_type, 
`account`.`login`, 
`account_details`.`name`, `account_details`.`surname`, `account_details`.`email`, `account_details`.`phone`, 
`route`.`city_id_from`, `route`.`city_id_to`, `route`.`distance`, 
city_from.translation AS city_from, city_to.translation AS city_to 
FROM `order` 
INNER JOIN `cargo_type_translator` ON `order`.`cargo_type_id` = `cargo_type_translator`.`cargo_type_id` AND `cargo_type_translator`.`language_id` = 1 
INNER JOIN `account` ON `order`.`account_id`= `account`.`id`
INNER JOIN `account_details` ON `account`.`account_details_id` = `account_details`.`id`
INNER JOIN `route` ON `order`.`route_id` = `route`.`id` 
INNER JOIN `city_translator` AS city_from ON `route`.`city_id_from` = city_from.`city_id` AND city_from.`language_id` = 1 
INNER JOIN `city_translator` AS city_to ON `route`.`city_id_to` = city_to.`city_id` AND city_to.`language_id` = 1 
WHERE city_from.translation LIKE '%dni%' 
ORDER BY city_to 
LIMIT 15 OFFSET 0;

-- find route by id
SELECT * FROM `route` WHERE `id` = 47;

-- find order by id
SELECT * FROM `order` WHERE `id` = 3;

-- find wallet by id
SELECT * FROM `wallet` WHERE `id` = 4;

-- find account by id
SELECT * FROM `account` WHERE `id` = 2;

-- find city id by city name
SELECT id FROM `city` WHERE `name` = 'Львов';

-- find city by id
SELECT * FROM `city` WHERE `id` = 2;

-- find number of orders with city like
SELECT COUNT(`order`.`id`) FROM `order` 
INNER JOIN `route` ON `order`.`route_id` = `route`.`id`
INNER JOIN `city` AS city_from ON `route`.`city_id_from` = city_from.id
WHERE city_from.`name` LIKE '%Дніпро%';
-- WHERE route_id = (SELECT id FROM `route` WHERE city_id_from = (SELECT id FROM `city` WHERE `name` LIKE '%іван%' ));

-- find cargo type by name
SELECT * FROM `cargo_type` WHERE `name` = 'parcel';

-- find all cargo types with translate
SELECT * FROM `cargo_type`
LEFT JOIN `cargo_type_translator` ON `cargo_type`.`id` = `cargo_type_translator`.`cargo_type_id`
WHERE  `cargo_type_translator`.`language_id` = 2; 

-- find language by code
SELECT * FROM `language` WHERE `code` = 'uk';

-- find cargo type by id and language id
SELECT * FROM `cargo_type`
JOIN `cargo_type_translator` ON `cargo_type`.`id` = `cargo_type_translator`.`cargo_type_id`
WHERE `cargo_type`.`id` = 2 AND `cargo_type_translator`.`language_id` = 2;

-- find number of accounts by role
SELECT COUNT(id) FROM `account` WHERE `role_id` = (SELECT id FROM `role` WHERE `name` = 'manager');

-- find all accounts by role
SELECT `account`.`id`, `account`.`login`, `account`.`role_id`, `account`.`wallet_id`, `account`.`account_details_id`,
                    `role`.`name` as role, 
                   `wallet`.`value`,
                   `account_details`.`name`, `account_details`.`surname`, `account_details`.`email`, `account_details`.`phone`, `account_details`.`city` 
                    FROM `account` 
                    INNER JOIN `account_details` ON `account`.`account_details_id` = `account_details`.`id`
                    INNER JOIN `role` ON `account`.`role_id` = `role`.`id`
                    INNER JOIN `wallet` ON `account`.`wallet_id` = `wallet`.`id`
                    -- WHERE `account`.`role_id` = (SELECT id FROM `role` WHERE `name` = 'user' ) 
                    WHERE `role`.`name` = 'user' 
                    ORDER BY surname  
                    LIMIT 100 OFFSET 0;

-- find column if exist
SELECT COUNT(id) FROM `route` WHERE `city_id_from` = 5 AND `city_id_to` = 6;