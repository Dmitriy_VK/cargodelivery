<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ tag import="org.finaltask.entity.Status"%>
<%@ attribute name="orderStatus" required="true" type="org.finaltask.entity.Status" %>

<fmt:setLocale value="${sessionScope.locale}" scope="session" />
<fmt:setBundle basename="lang" />

<p>
    <c:choose>
       <c:when test="${pageScope.orderStatus eq Status.NEW}">
          <fmt:message key="status.new" />
       </c:when>
       <c:when test="${pageScope.orderStatus eq Status.PROCESSED}"> 
          <fmt:message key="status.processed" />
       </c:when>
       <c:when test="${pageScope.orderStatus eq Status.PAID}">
          <fmt:message key="status.paid" />
       </c:when>
       <c:when test="${pageScope.orderStatus eq Status.DELIVERED}">
          <fmt:message key="status.delivered" />
       </c:when>
        <c:otherwise>
            Nothing
        </c:otherwise>
    </c:choose>
</p>