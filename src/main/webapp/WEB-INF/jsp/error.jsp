<%@ page isErrorPage="true" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<fmt:setLocale value="${sessionScope.locale}" scope="session" />
<fmt:setBundle basename="lang" />

<html lang="en">
<head>
    <title>CargoDelivery</title>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/bootstrap.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/bootstrap-icons-1.10.3/bootstrap-icons.css">
        <script src="${pageContext.request.contextPath}/static/js/jquery-3.6.0.js"></script>
        <script src="${pageContext.request.contextPath}/static/js/bootstrap.bundle.js"></script>
</head>
<body class="bg-info">
  <jsp:include page="fragments/header.jsp"/>

<main>
    <div class="container">
     <div class="row justify-content-center text-danger">
        <c:choose>
            <c:when test="${errorAccess eq 'true'}">
                <fmt:message key="errorAccess" />
            </c:when>
            <c:when test="${not empty error}">
                <p>${error}</p>
            </c:when>
            <c:otherwise>
                UNEXPECTED ERROR
                ${empty javax.servlet.error.status_code}
            </c:otherwise>
        </c:choose>
        </div>
    </div>
</main>

    <jsp:include page="fragments/footer.jsp"/>

</body>
</html>