<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<c:set var="language" value="${sessionScope.locale}" scope="session" />
<fmt:setLocale value="${language}" scope="session"/>
<fmt:setBundle basename="lang"/>

<html>
<head>
    <title>CargoDelivery</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/bootstrap.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/bootstrap-icons-1.10.3/bootstrap-icons.css">
    <script src="${pageContext.request.contextPath}/static/js/jquery-3.6.0.js"></script>
    <script src="${pageContext.request.contextPath}/static/js/bootstrap.bundle.js"></script>
</head>
<body class="bg-info">
	<jsp:include page="fragments/header.jsp"/>
	<main>
		<div class="container">
		    <div class="row justify-content-center">
		        <c:choose>
                       <c:when test="${not empty sessionScope.calcResult}">
		    	<table class="table table-bordered border-primary table-sm w-auto">
							<thead>
								<tr>
									<th scope="col" colspan="2">
										<fmt:message key="calcResult.result"/>
									</th>
								</tr>
							</thead>
							<tbody>
				
								<tr>
									<td><fmt:message key="page.route" /></td>
									<td>
										${sessionScope.calcResult.departureCity}
				        				<i class="bi bi-arrow-right"></i>
				        				<c:out value="${sessionScope.calcResult.destinationCity}"/>
									</td>
								</tr>
								<tr>
									<td><fmt:message key="payPage.distance" /></td>
									<td>
										<c:out value="${sessionScope.calcResult.distance}"/>
									</td>
								</tr>
								<tr>
									<td><fmt:message key="payPage.cargoType" /></td>
									<td>
										<c:out value="${sessionScope.calcResult.cargoType.name}"/>
									</td>
								</tr>
								<tr>
									<td><fmt:message key="payPage.volume" /></td>
									<td>
										<c:out value="${sessionScope.calcResult.volume}"/>
									</td>
								</tr>
								<tr>
									<td><fmt:message key="payPage.weight" /></td>
									<td>
										<c:out value="${sessionScope.calcResult.weight}"/>
									</td>
								</tr>
								<tr>
									<td><fmt:message key="payPage.price" /></td>
									<td>
										<c:out value="${sessionScope.calcResult.costOfDelivery}"/> <fmt:message key="user.menu.currency"/>
									</td>
								</tr>
							</tbody>
						</table>
						</c:when>
						<c:otherwise>
                                  <fmt:message key="calcResult.wrong_data" />
                        </c:otherwise>
                      </c:choose>
			</div>
		</div>
	</main>
	<jsp:include page="fragments/footer.jsp"/>
</body>
</html>