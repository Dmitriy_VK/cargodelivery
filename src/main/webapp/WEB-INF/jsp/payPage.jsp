<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<fmt:setLocale value="${sessionScope.locale}" scope="session" />
<fmt:setBundle basename="lang" />

<html>
<head>
<title>CargoDelivery</title>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/static/css/bootstrap.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/static/css/bootstrap-icons-1.10.3/bootstrap-icons.css">
<script
	src="${pageContext.request.contextPath}/static/js/jquery-3.6.0.js"></script>
<script
	src="${pageContext.request.contextPath}/static/js/bootstrap.bundle.js"></script>
</head>

<body class="bg-info text-center">
	<jsp:include page="fragments/header.jsp" />

	<main>
		<div class="container mt-2">
			<div class="row justify-content-center">
				<c:choose>
					<c:when test="${requestScope.successfulPayment eq 'true'}">
						<p class="rounded-pill">
							<fmt:message key="payPage.successfulPayment" />
						</p>
					</c:when>
					<c:otherwise>
						<table class="table table-bordered border-primary table-sm w-auto">
							<thead>
								<tr>
									<th scope="col" colspan="2"><fmt:message
											key="payPage.title" /></th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td><fmt:message key="payPage.customer" /></td>
									<td>${order.account.accountDetails.name}
										${order.account.accountDetails.surname}</td>
								</tr>
								<tr>
									<td><fmt:message key="page.route" /></td>
									<td>${order.route.cityFrom.name}
										${order.route.cityTo.name}</td>
								</tr>
								<tr>
									<td><fmt:message key="payPage.distance" /></td>
									<td>${order.route.distance}</td>
								</tr>
								<tr>
									<td><fmt:message key="payPage.cargoType" /></td>
									<td>${order.cargoType}</td>
								</tr>
								<tr>
									<td><fmt:message key="payPage.volume" /></td>
									<td>${order.volume}</td>
								</tr>
								<tr>
									<td><fmt:message key="payPage.weight" /></td>
									<td>${order.weight} <fmt:message key="payPage.weight.mesurement"/></td>
								</tr>
								<tr>
									<td><fmt:message key="payPage.price" /></td>
									<td>${order.price} <fmt:message key="user.menu.currency"/></td>
								</tr>
								<tr>
									<td><fmt:message key="payPage.deliveryDate" /></td>
									<td>${order.deliveryDate}</td>
								</tr>
								<tr>
									<td colspan="2"><a class="btn btn-primary"
										href="payOrder?orderId=${order.id}" role="button"> <fmt:message
												key="payPage.payOrder" />
									</a> <a class="btn btn-primary" href="viewOrders" role="button">
											<fmt:message key="payPage.cancel" />
									</a></td>
								</tr>
							</tbody>
						</table>
					</c:otherwise>
				</c:choose>
			</div>
		</div>
	</main>

	<jsp:include page="fragments/footer.jsp" />

</body>
</html>