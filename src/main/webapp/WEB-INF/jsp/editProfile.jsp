<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<fmt:setLocale value="${sessionScope.locale}" scope="session" />
<fmt:setBundle basename="lang" />

<html>
<head>
<title>CargoDelivery</title>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/static/css/bootstrap.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/static/css/bootstrap-icons-1.10.3/bootstrap-icons.css">
<script
	src="${pageContext.request.contextPath}/static/js/jquery-3.6.0.js"></script>
<script
	src="${pageContext.request.contextPath}/static/js/bootstrap.bundle.js"></script>
</head>
<body class="bg-info">
	<jsp:include page="fragments/header.jsp" />

	<main class="container">
		
		<div class="row justify-content-center">
			<c:choose>
                <c:when test="${not empty requestScope.accountProfile}">
			<div class="col col-lg-4">
				<h4 class="text-center mb-3">
					<fmt:message key="editProfile.edit" />
				</h4>

				<form action="editProfile" method="post">
					<input type="hidden" name="formData" value="formData">
					<div class="row g-3">
					
						<div class="col-sm-6">							
							<label for="firstName" class="form-label">
							    <fmt:message key="registration.first_name" />
							</label>
							<input type="text" class="form-control" id="firstName" name="firstName" value="${requestScope.accountProfile.accountDetails.name}">
							<c:if test="${requestScope.error_firstName}">
								<div class="text-danger">
									<fmt:message key="registration.error_firstName" />
								</div>
							</c:if>
						</div>

						<div class="col-sm-6">							
							<label for="lastName" class="form-label">
								<fmt:message key="registration.last_name" />
							</label>
							<input type="text" class="form-control" id="lastName" name="lastName" value="${requestScope.accountProfile.accountDetails.surname}">
							<c:if test="${requestScope.error_lastName}">
								<div class="text-danger">
									<fmt:message key="registration.error_lastName" />
								</div>
							</c:if>
						</div>

						<div class="col-12">
							<label for="username" class="form-label">
								<fmt:message key="registration.username" />
							</label>
							
								<input type="text" class="form-control" id="username" value="${requestScope.accountProfile.login}" name="username">
							
							<c:if test="${requestScope.error_userName}">
								<div class="text-danger">
									<fmt:message key="registration.error_userName" />
								</div>
							</c:if>
						</div>

						<div class="col-12">
							<fmt:message key="registration.password_field" var="password_field" />
							<label for="password" class="form-label">
								<fmt:message key="registration.password" />
							</label>
							<input type="text" class="form-control" id="password" placeholder="${password_field}" name="password">
							<c:if test="${requestScope.error_password}">
								<div class="text-danger">
									<fmt:message key="registration.error_password" />
								</div>
							</c:if>
						</div>

						<div class="col-12">
							<fmt:message key="registration.password_repeat_field" var="password_repeat_field" />
							<label for="re_pass" class="form-label">
								<fmt:message key="registration.password_repeat" />
							</label>
							<input type="text" class="form-control" id="re_pass" name="re_pass" placeholder="${password_repeat_field}">
							<c:if test="${requestScope.error_re_password}">
								<div class="text-danger">
									<fmt:message key="registration.error_re_password" />
								</div>
							</c:if>
						</div>

						<div class="col-12">
							<label for="email" class="form-label">
								<fmt:message key="registration.email" />
							</label>
							<input type="text" class="form-control" id="email" name="email"	value="${requestScope.accountProfile.accountDetails.email}">
							<c:if test="${requestScope.error_email}">
								<div class="text-danger">
									<fmt:message key="registration.error_email" />
								</div>
							</c:if>
						</div>

						<div class="col-12">
							<label for="phone" class="form-label">
								<fmt:message key="registration.phone" />
							</label>
							<input type="text" class="form-control" id="phone" name="phone"	value="${requestScope.accountProfile.accountDetails.phone}">
							<c:if test="${requestScope.error_phone}">
								<div class="text-danger">
									<fmt:message key="registration.error_phone" />
								</div>
							</c:if>
						</div>

						<div class="col-12">
							<label for="city" class="form-label">
								<fmt:message key="registration.city" />
							</label>
							<input type="text" class="form-control" id="city" name="city" value="${requestScope.accountProfile.accountDetails.city}">
							<c:if test="${requestScope.error_cityName}">
								<div class="text-danger">
									<fmt:message key="registration.error_cityName" />
								</div>
							</c:if>
						</div>
					</div>

					<hr class="my-4">

					<button class="w-100 btn btn-primary btn-lg" type="submit">
						<fmt:message key="editProfile.edit" />
					</button>
				</form>
				</div>
				</c:when>
						<c:otherwise>
                                  <fmt:message key="viewProfile.no_data" />
                        </c:otherwise>
                </c:choose>
			</div>
	</main>

	<jsp:include page="fragments/footer.jsp" />

</body>
</html>