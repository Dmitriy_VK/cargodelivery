<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ page import="org.finaltask.entity.RoleEnum"%>
<%@ page import="org.finaltask.entity.Status"%>
<%@ taglib prefix="ctag" uri="cargotags" %>
<%@ taglib prefix="st" tagdir="/WEB-INF/tags" %>

<fmt:setLocale value="${sessionScope.locale}" scope="session" />
<fmt:setBundle basename="lang" />

<html>
<head>
<title>CargoDelivery</title>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/static/css/bootstrap.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/static/css/bootstrap-icons-1.10.3/bootstrap-icons.css">
<script
	src="${pageContext.request.contextPath}/static/js/jquery-3.6.0.js"></script>
<script
	src="${pageContext.request.contextPath}/static/js/bootstrap.bundle.js"></script>
</head>
<body class="bg-info">
	<jsp:include page="fragments/header.jsp" />

	<main>
		<div class="container">
			<c:choose>
				<c:when test="${empty accountList}">
                	<div class="row justify-content-center">
                		<fmt:message key="view.accounts.empty_accountList" />
                	</div>
                </c:when>
				<c:otherwise>

					<form action="viewUsers">
						<div class="row justify-content-start">
							<div class="col-md-auto p-2">
								<label for="selnum"><fmt:message key="home.show" /></label>
							</div>
							<div class="col-md-auto p-2">
								<select name="recordsPerPage" class="form-control form-control-sm" id="selnum">
									<option selected disabled>
										<c:out value="${recordsPerPage}" />
									</option>
									<option value="5">5</option>
									<option value="10">10</option>
									<option value="15">15</option>
									<option value="20">20</option>
								</select>
							</div>
							<div class="col-md-auto p-2">
								<fmt:message key="home.entries" />
							</div>
								<input type="hidden" name="orderBy" value="${orderBy}">
								<input type="hidden" name="sortOrder" value="${sortOrder}">
								<input type="hidden" name="role" value="${role}">
							<div class="col-md-auto p-2">
                    			<button type="submit" class="btn btn-primary mb-2">
                    				<fmt:message key="button.submit"/>
                    			</button>
                 			</div>
						</div>
					</form>
		</div>

		<div class="container">
			<div class="table-responsive">
				<table
					class="table table-light table-striped table-bordered align-middle">
					<thead>
						<tr class="text-nowrap">
							<th scope="col">#</th>
							<th scope="col">
							    <a	href="viewAccounts?orderBy=login&recordsPerPage=${recordsPerPage}&role=${role}" class="text-decoration-none">
                            	    <i class="bi bi-sort-alpha-down"></i>
                                </a>
                                <fmt:message key="table.login" />
                                <a href="viewAccounts?orderBy=login&sortOrder=DESC&recordsPerPage=${recordsPerPage}&role=${role}" class="text-decoration-none">
                            	    <i class="bi bi-sort-alpha-down-alt"></i>
                                </a>
							</th>
							<th scope="col">
							    <fmt:message key="table.name" />
							</th>
							<th scope="col">
							    <a	href="viewAccounts?orderBy=surname&recordsPerPage=${recordsPerPage}&role=${role}" class="text-decoration-none">
                                     <i class="bi bi-sort-alpha-down"></i>
                                </a>
							    <fmt:message key="table.surname" />
							    <a href="viewAccounts?orderBy=surname&sortOrder=DESC&recordsPerPage=${recordsPerPage}&role=${role}" class="text-decoration-none">
                                    <i class="bi bi-sort-alpha-down-alt"></i>
                                </a>
							</th>
							<th scope="col"><fmt:message key="table.role" /></th>
							<th scope="col"><fmt:message key="table.wallet" /></th>
							<th scope="col"><fmt:message key="table.email" /></th>
							<th scope="col"><fmt:message key="table.phone" /></th>
							<th scope="col">
							    <a	href="viewAccounts?orderBy=city&recordsPerPage=${recordsPerPage}&role=${role}" class="text-decoration-none">
                                    <i class="bi bi-sort-alpha-down"></i>
                                </a>
							    <fmt:message key="table.city" />
							    <a href="viewAccounts?orderBy=city&sortOrder=DESC&recordsPerPage=${recordsPerPage}&role=${role}" class="text-decoration-none">
                                    <i class="bi bi-sort-alpha-down-alt"></i>
                                </a>
							</th>
						</tr>
					</thead>
					<tbody>

						<c:forEach var="account" items="${accountList}" varStatus="status">
							<tr>
								<td scope="row">${status.count + recordsPerPage * currentPage - recordsPerPage}</td>
								<td>${account.login}</td>
								<td>${account.accountDetails.name}</td>
								<td>${account.accountDetails.surname}</td>
								<td>${account.role.name}</td>
								<td>${account.wallet.value}</td>
								<td>${account.accountDetails.email}</td>
								<td>${account.accountDetails.phone}</td>
								<td>${account.accountDetails.city}</td>
							</tr>
						</c:forEach>

					</tbody>
				</table>
			</div>

			<nav>
				<ul class="pagination justify-content-center">
					<c:if test="${currentPage ne 1}">
						<li class="page-item">
							<c:url value="viewDateReport" var="prUrl">
								<c:param name="recordsPerPage" value="${recordsPerPage}" />
								<c:param name="currentPage" value="${currentPage-1}" />
								<c:param name="orderBy" value="${orderBy}" />
								<c:param name="sortOrder" value="${sortOrder}" />
								<c:param name="deliveryDate" value="${deliveryDate}" />
							</c:url>
							<a class="page-link" href='<c:out value="${prUrl}"/>'>
								<fmt:message key="pagination.previous" />
							</a>
						</li>
					</c:if>

					<c:forEach begin="1" end="${numOfPages}" var="i">
						<c:choose>
							<c:when test="${currentPage eq i}">
								<li class="page-item active"><a class="page-link"> ${i}
								</a></li>
							</c:when>
							<c:otherwise>
								<li class="page-item"><a class="page-link"
									href="viewDateReport?recordsPerPage=${recordsPerPage}&currentPage=${i}&orderBy=${orderBy}&sortOrder=${sortOrder}&deliveryDate=${deliveryDate}">${i}</a>
								</li>
							</c:otherwise>
						</c:choose>
					</c:forEach>

					<c:if test="${currentPage lt numOfPages}">
						<li class="page-item"><a class="page-link"
							href="viewDateReport?recordsPerPage=${recordsPerPage}&currentPage=${currentPage+1}&orderBy=${orderBy}&sortOrder=${sortOrder}&deliveryDate=${deliveryDate}"><fmt:message
									key="pagination.next" /></a></li>
					</c:if>
				</ul>
			</nav>
			</c:otherwise>
			</c:choose>
		</div>
	</main>

	<jsp:include page="fragments/footer.jsp" />
</body>
</html>