<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<fmt:setLocale value="${sessionScope.locale}" scope="session" />
<fmt:setBundle basename="lang" />

<html>
<head>
<title>CargoDelivery</title>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/static/css/bootstrap.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/static/css/bootstrap-icons-1.10.3/bootstrap-icons.css">
<script
	src="${pageContext.request.contextPath}/static/js/jquery-3.6.0.js"></script>
<script
	src="${pageContext.request.contextPath}/static/js/bootstrap.bundle.js"></script>
</head>
<body class="bg-info text-center">
	<c:import url="fragments/header.jsp" />

	<main>
		<div class="container mt-2">
			<div class="row justify-content-md-center">
				<c:choose>
					<c:when test="${not empty requestScope.orderResult}">
						<c:choose>
						
							<c:when test="${ requestScope.orderResult eq 'true' }">
								<div class="text-center">
									<fmt:message key="makeOrderForm.success" />
								</div>
							</c:when>
							
							<c:when test="${ requestScope.orderResult eq 'false' }">
								<div class="text-center">
									<fmt:message key="makeOrderForm.failure" />
								</div>
							</c:when>

							<c:otherwise>
								<fmt:message key="Strange error" />
							</c:otherwise>
							
						</c:choose>
					</c:when>
					<c:when test="${not empty sessionScope.route}">
						<div class="col-md-3">
							<h4 class="mb-3">
								<fmt:message key="form.order.title" />
							</h4>
							<p>
								<c:out value="${sessionScope.route.cityFrom.name}" />
								<i class="bi bi-arrow-right"></i>
								<c:out value="${sessionScope.route.cityTo.name}" />
								<c:out value="${sessionScope.route.distance}" />
								<fmt:message key="form.km" />
							</p>
							<form action="processOrder" method="post">

								<input type="hidden" name="formData" value="formData">

								<div class="mb-3">
									<c:if test="${requestScope.error_cargoType}">
										<div class="text-danger">
											<fmt:message key="form.error_cargoType" />
										</div>
									</c:if>
									<label for="cargoType" class="form-label">
									    <fmt:message key="form.cargo_type" />
									</label>
									 <select class="form-select" id="cargoType" name="cargoTypeId">
										<option selected disabled>
										    Open this select menu
										</option>
										<c:forEach var="cargoType" items="${sessionScope.cargoTypes}">
										    <option value="${cargoType.id}">
										        ${cargoType.name}
										    </option>
										</c:forEach>
									</select>
								</div>

								<div class="mb-3">

									<label for="weight" class="form-label"><fmt:message
											key="form.weight" /></label> <input type="text" class="form-control"
										id="weight" name="weight">
									<c:if test="${requestScope.error_weight}">
										<div class="text-danger">
											<fmt:message key="form.error_weight" />
										</div>
									</c:if>

								</div>

								<div class="mb-3">

									<label for="volume" class="form-label"><fmt:message
											key="form.volume" /></label> <input type="text" class="form-control"
										id="volume" name="volume">

									<c:if test="${requestScope.error_volume}">
										<div class="text-danger">
											<fmt:message key="form.error_volume" />
										</div>
									</c:if>

								</div>

								<div class="mb-3">
									<label for="address" class="form-label"><fmt:message
											key="form.address" /></label> <input type="text"
										class="form-control" id="address" name="address">
									<c:if test="${requestScope.error_address}">
										<div class="text-danger">
											<fmt:message key="form.error_address" />
										</div>
									</c:if>
								</div>

								<div class="mb-3">
									<label for="deliveryDate" class="form-label"><fmt:message
											key="form.delivery_date" /></label> <input type="text"
										class="form-control" id="deliveryDate" name="deliveryDate">
									<c:if test="${requestScope.error_deliveryDate}">
										<div class="text-danger">
											<fmt:message key="form.error_delivery_date" />
										</div>
									</c:if>
									<c:if test="${requestScope.error_compareDate}">
										<div class="text-danger">
											<fmt:message key="form.error_compare_date" />
										</div>
									</c:if>
								</div>

								<input type="hidden" name="routeId" value="${sessionScope.route.id}">


								<hr class="my-4">

								<button class="w-auto btn btn-primary btn-lg" type="submit">
									<fmt:message key="form.makeOrder" />
								</button>
							</form>
						</div>
					</c:when>
					<c:otherwise>
                              No information to display
                    </c:otherwise>
				</c:choose>
			</div>
		</div>
	</main>

	<c:import url="fragments/footer.jsp" />

</body>
</html>