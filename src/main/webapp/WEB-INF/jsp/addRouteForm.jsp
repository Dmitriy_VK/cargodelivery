<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<fmt:setLocale value="${sessionScope.locale}" scope="session" />
<fmt:setBundle basename="lang" />

<html>
<head>
<title>CargoDelivery</title>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/static/css/bootstrap.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/static/css/bootstrap-icons-1.10.3/bootstrap-icons.css">
<script
	src="${pageContext.request.contextPath}/static/js/jquery-3.6.0.js"></script>
<script
	src="${pageContext.request.contextPath}/static/js/bootstrap.bundle.js"></script>
</head>
<body class="bg-info text-center">
	<c:import url="fragments/header.jsp" />

	<main>
		<div class="container mt-2">
			<div class="row justify-content-md-center">
				<c:choose>

					<c:when test="${not empty requestScope.cityList}">
						<div class="col-md-3">
							<h4 class="mb-3">
								<fmt:message key="form.addRoute.title" />
							</h4>

							<form action="addRouteProcess" method="post">

								<input type="hidden" name="formData" value="formData">

								<div class="mb-3">
									<c:if test="${requestScope.error_route_exist}">
										<div class="text-danger">
											<fmt:message key="form.addRoute.error_route_exist" />
										</div>
									</c:if>
                                    <c:if test="${requestScope.error_same_city}">
										<div class="text-danger">
											<fmt:message key="form.addRoute.error_city" />
										</div>
									</c:if>

									<label for="cityFrom" class="form-label">
									    <fmt:message key="table.city_from" />
									</label>

									 <select class="form-select" id="cityFrom" name="cityFromId">
										<option selected disabled>
										    Open this select menu
										</option>
										<c:forEach var="cityFrom" items="${requestScope.cityList}">
										    <option value="${cityFrom.id}">
										        ${cityFrom.name}
										    </option>
										</c:forEach>
									</select>
								</div>

								<div class="mb-3">

                                    <label for="cityTo" class="form-label">
                                        <fmt:message key="table.city_to" />
                                    </label>

                                    <select class="form-select" id="cityTo" name="cityToId">
                                        <option selected disabled>
                                			Open this select menu
                                	    </option>
                                        <c:forEach var="cityTo" items="${requestScope.cityList}">
                                		    <option value="${cityTo.id}">
                                		        ${cityTo.name}
                                	        </option>
                                		</c:forEach>
                                	</select>

                                </div>


								<div class="mb-3">

									<label for="distance" class="form-label">
									    <fmt:message key="table.distance" />
									</label>
									<input type="text" class="form-control" id="distance" name="distance">
									<c:if test="${requestScope.error_distance}">
										<div class="text-danger">
											<fmt:message key="form.addRoute.error_distance" />
										</div>
									</c:if>

								</div>

								<div class="mb-3">

									<label for="tariff" class="form-label">
									    <fmt:message key="form.volume" />
									</label>

									<input type="text" class="form-control" id="tariff" name="tariff">

									<c:if test="${requestScope.error_tariff}">
										<div class="text-danger">
											<fmt:message key="form.addRoute.error_tariff" />
										</div>
									</c:if>

								</div>

								<hr class="my-4">

								<button class="w-auto btn btn-primary btn-lg" type="submit">
									<fmt:message key="form.addRoute.button" />
								</button>
							</form>
						</div>
					</c:when>
					<c:otherwise>
                              No information to display
                    </c:otherwise>
				</c:choose>
			</div>
		</div>
	</main>

	<c:import url="fragments/footer.jsp" />

</body>
</html>