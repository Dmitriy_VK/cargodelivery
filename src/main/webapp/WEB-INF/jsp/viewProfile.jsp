<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<c:set var="language" value="${sessionScope.locale}" scope="session" />
<fmt:setLocale value="${language}" scope="session"/>
<fmt:setBundle basename="lang"/>

<html>
<head>
    <title>CargoDelivery</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/bootstrap.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/bootstrap-icons-1.10.3/bootstrap-icons.css">
    <script src="${pageContext.request.contextPath}/static/js/jquery-3.6.0.js"></script>
    <script src="${pageContext.request.contextPath}/static/js/bootstrap.bundle.js"></script>
</head>
<body class="bg-info">
	<jsp:include page="fragments/header.jsp"/>
	<main>
		<div class="container">
		    <div class="row justify-content-center">
		        <c:choose>
                       <c:when test="${not empty requestScope.accountProfile}">
                       <div class="text-center">
                            <p class="fw-bold">
                       		    <fmt:message key="viewProfile.profile"/>
                       		</p>
                       	</div>
		    	<table class="table table-bordered border-primary table-sm w-auto">
							<tbody>
								<tr>
									<td><fmt:message key="viewProfile.login" /></td>
									<td>${requestScope.accountProfile.login}</td>
								</tr>
								<tr>
									<td><fmt:message key="viewProfile.role" /></td>
									<td>${requestScope.accountProfile.role.name}</td>
								</tr>
								<tr>
									<td><fmt:message key="viewProfile.wallet" /></td>
									<td>${requestScope.accountProfile.wallet.value}</td>
								</tr>
								<tr>
									<td><fmt:message key="viewProfile.name" /></td>
									<td>${requestScope.accountProfile.accountDetails.name}</td>
								</tr>
								<tr>
									<td><fmt:message key="viewProfile.surname" /></td>
									<td>${requestScope.accountProfile.accountDetails.surname}</td>
								</tr>
								<tr>
									<td><fmt:message key="viewProfile.email" /></td>
									<td>${requestScope.accountProfile.accountDetails.email}</td>
								</tr>
								<tr>
									<td><fmt:message key="viewProfile.phone" /></td>
									<td>${requestScope.accountProfile.accountDetails.phone}</td>
								</tr>
								<tr>
									<td><fmt:message key="viewProfile.city" /></td>
									<td>${requestScope.accountProfile.accountDetails.city}</td>
								</tr>
							</tbody>
						</table>
						<div class="text-center">
                            <a class="btn btn-outline-primary me-2 w-auto" href="viewProfile?action=edit">
		    			        <fmt:message key="viewProfile.edit_profile" />
						    </a>
						</div>
						</c:when>
						<c:otherwise>
                             <fmt:message key="viewProfile.no_data" />
                        </c:otherwise>
                      </c:choose>
			</div>
		</div>
	</main>
	<jsp:include page="fragments/footer.jsp"/>
</body>
</html>