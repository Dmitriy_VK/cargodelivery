<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ page import="org.finaltask.entity.RoleEnum"%>

<fmt:setLocale value="${sessionScope.locale}" scope="session" />
<fmt:setBundle basename="lang" />

<html>
<head>
<title>CargoDelivery</title>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/static/css/bootstrap.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/static/css/bootstrap-icons-1.10.3/bootstrap-icons.css">
<script
	src="${pageContext.request.contextPath}/static/js/jquery-3.6.0.js"></script>
<script
	src="${pageContext.request.contextPath}/static/js/bootstrap.bundle.js"></script>
</head>
<body class="bg-info">
	<jsp:include page="fragments/header.jsp" />

	<main class="container">
		<c:if test="${not empty requestScope.regResult}">
			<c:choose>
				<c:when test="${ requestScope.regResult eq 'true' }">
					<div class="text-center">
						<fmt:message key="registration.success" />
					</div>
				</c:when>
				<c:when test="${ requestScope.regResult eq 'false' }">
					<div class="text-center">
						<fmt:message key="registration.failure" />
					</div>
				</c:when>

				<c:otherwise>
					<c:out value="Strange error" />
				</c:otherwise>
			</c:choose>
		</c:if>

		<c:choose>

        	<c:when test="${ sessionScope.account.role.name eq RoleEnum.USER }">
        	    <div class="text-center">
            		<fmt:message key="registration.user.logout" />
            	</div>
            </c:when>

            <c:otherwise>
		<div class="row justify-content-center">
			<div class="col col-lg-4">
				<h4 class="mb-3">
					<fmt:message key="registration.title" />
				</h4>
				<form action="registration" method="post">
					<input type="hidden" name="formData" value="formData">
					<div class="row g-3">
						<div class="col-sm-6">
							<fmt:message key="registration.first_name_field" var="first_name_field" />
							<label for="firstName" class="form-label">
							    <fmt:message key="registration.first_name" />
							</label>
							<input type="text" class="form-control" id="firstName" name="firstName" placeholder="${first_name_field}">
							<c:if test="${requestScope.error_firstName}">
								<div class="text-danger">
									<fmt:message key="registration.error_firstName" />
								</div>
							</c:if>
						</div>

						<div class="col-sm-6">
							<fmt:message key="registration.last_name_field" var="last_name_field" />
							<label for="lastName" class="form-label"><fmt:message
									key="registration.last_name" /></label> <input type="text"
								class="form-control" id="lastName" name="lastName" placeholder="${last_name_field}">
							<c:if test="${requestScope.error_lastName}">
								<div class="text-danger">
									<fmt:message key="registration.error_lastName" />
								</div>
							</c:if>
						</div>

						<div class="col-12">
						    <fmt:message key="registration.username_field" var="username_field" />
							<label for="username" class="form-label"><fmt:message
									key="registration.username" /></label>
							
								<input type="text" class="form-control" id="username"
									placeholder="${username_field}" name="username">
							
							<c:if test="${requestScope.error_userName}">
								<div class="text-danger">
									<fmt:message key="registration.error_userName" />
								</div>
							</c:if>
                            <c:if test="${requestScope.error_userExist}">
								<div class="text-danger">
									<fmt:message key="registration.error_userExist" />
								</div>
							</c:if>
						</div>

						<div class="col-12">
							<fmt:message key="registration.password_field" var="password_field" />
							<label for="password" class="form-label"><fmt:message
									key="registration.password" /></label> <input type="text"
								class="form-control" id="password" placeholder="${password_field}"
								name="password">
							<c:if test="${requestScope.error_password}">
								<div class="text-danger">
									<fmt:message key="registration.error_password" />
								</div>
							</c:if>
						</div>

						<div class="col-12">
							<fmt:message key="registration.password_repeat_field" var="password_repeat_field" />
							<label for="re_pass" class="form-label"><fmt:message
									key="registration.password_repeat" /></label> <input type="text"
								class="form-control" id="re_pass" name="re_pass"
								placeholder="${password_repeat_field}">
							<c:if test="${requestScope.error_re_password}">
								<div class="text-danger">
									<fmt:message key="registration.error_re_password" />
								</div>
							</c:if>
						</div>

						<div class="col-12">
							<fmt:message key="registration.email_field" var="email_field" />
							<label for="email" class="form-label"><fmt:message key="registration.email" /></label> <input
								type="text" class="form-control" id="email" name="email"
								placeholder="${email_field}">
							<c:if test="${requestScope.error_email}">
								<div class="text-danger">
									<fmt:message key="registration.error_email" />
								</div>
							</c:if>
						</div>

						<div class="col-12">
							<fmt:message key="registration.phone_field" var="phone_field" />
							<label for="phone" class="form-label"><fmt:message key="registration.phone" /></label> <input
								type="text" class="form-control" id="phone" name="phone"
								placeholder="${phone_field}">
							<c:if test="${requestScope.error_phone}">
								<div class="text-danger">
									<fmt:message key="registration.error_phone" />
								</div>
							</c:if>
						</div>

						<div class="col-12">
							<fmt:message key="registration.city_field" var="city_field" />
							<label for="city" class="form-label"><fmt:message key="registration.city" /></label> <input
								type="text" class="form-control" id="city" name="city"
								placeholder="${city_field}">
							<c:if test="${requestScope.error_cityName}">
								<div class="text-danger">
									<fmt:message key="registration.error_cityName" />
								</div>
							</c:if>
						</div>

					</div>

					<hr class="my-4">

					<c:if test="${empty sessionScope.account}">
						<input type="hidden" name="role" value="user">
					</c:if>

					<c:if test="${not empty sessionScope.account and sessionScope.account.role.name eq RoleEnum.MANAGER}">
						<input type="hidden" name="role" value="manager">
					</c:if>

					<button class="w-100 btn btn-primary btn-lg" type="submit"><fmt:message key="registration.register" /></button>
				</form>
			</div>
		</div>
		</c:otherwise>
		</c:choose>
	</main>

	<jsp:include page="fragments/footer.jsp" />

</body>
</html>