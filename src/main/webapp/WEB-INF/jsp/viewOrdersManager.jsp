<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ page import="org.finaltask.entity.RoleEnum"%>
<%@ page import="org.finaltask.entity.Status"%>
<%@ taglib prefix="ctag" uri="cargotags"%>
<%@ taglib prefix="st" tagdir="/WEB-INF/tags" %>

<fmt:setLocale value="${sessionScope.locale}" scope="session" />
<fmt:setBundle basename="lang" />

<html>
<head>
<title>CargoDelivery</title>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/static/css/bootstrap.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/static/css/bootstrap-icons-1.10.3/bootstrap-icons.css">
<script
	src="${pageContext.request.contextPath}/static/js/jquery-3.6.0.js"></script>
<script
	src="${pageContext.request.contextPath}/static/js/bootstrap.bundle.js"></script>
</head>
<body class="bg-info">
	<jsp:include page="fragments/header.jsp" />

	<main>
		<div class="container">
			<form action="viewOrders">
				<div class="row justify-content-start">
					<div class="col-md-auto p-2">
						<label for="selnum"><fmt:message key="home.show" /></label>
					</div>
					<div class="col-md-auto p-2">
						<select name="recordsPerPage" class="form-control form-control-sm"
							id="selnum">
							<option selected disabled>
								<c:out value="${recordsPerPage}" />
							</option>
							<option value="5">5</option>
							<option value="10">10</option>
							<option value="15">15</option>
							<option value="20">20</option>
						</select>
					</div>
					<div class="col-md-auto p-2">
						<fmt:message key="home.entries" />
					</div>
					<input type="hidden" name="orderBy" value="${orderBy}">
					<input type="hidden" name="sortOrder" value="${sortOrder}">
					<input type="hidden" name="whereName" value="${whereName}">
					<input type="hidden" name="whereValue" value="${whereValue}">
					<div class="col-md-auto p-2">
						<button type="submit" class="btn btn-primary mb-2">
							<fmt:message key="button.submit" />
						</button>
					</div>
				</div>
			</form>
		</div>

		<div class="container">
			<div class="table-responsive">
				<table
					class="table table-light table-striped table-bordered align-middle">
					<thead>
						<tr class="text-nowrap">
							<th scope="col">#</th>
							<th scope="col"><fmt:message key="table.login" /></th>
							<th scope="col"><fmt:message key="table.name" /></th>
							<th scope="col">
								<a href="viewOrders?orderBy=city_from&recordsPerPage=${recordsPerPage}&whereName=${whereName}&whereValue=${whereValue}"
									class="text-decoration-none">
								<i class="bi bi-sort-alpha-down"></i>
								</a> 
									<fmt:message key="table.city_from" />
								<a href="viewOrders?orderBy=city_from&sortOrder=DESC&recordsPerPage=${recordsPerPage}&whereName=${whereName}&whereValue=${whereValue}"
									class="text-decoration-none">
								<i class="bi bi-sort-alpha-down-alt"></i>
								</a>
							</th>
							<th scope="col"><a
								href="viewOrders?orderBy=city_to&recordsPerPage=${recordsPerPage}&whereName=${whereName}&whereValue=${whereValue}"
								class="text-decoration-none"> <i
									class="bi bi-sort-alpha-down"></i>
							</a> <fmt:message key="table.city_to" /> <a
								href="viewOrders?orderBy=city_to&sortOrder=DESC&recordsPerPage=${recordsPerPage}&whereName=${whereName}&whereValue=${whereValue}"
								class="text-decoration-none"> <i
									class="bi bi-sort-alpha-down-alt"></i>
							</a>
							</th>
							<th scope="col"><fmt:message key="table.address" /></th>
							<th scope="col"><fmt:message key="table.cargoType" /></th>
							<th scope="col"><fmt:message key="table.distance" /></th>
							<th scope="col"><fmt:message key="table.weight" /></th>
							<th scope="col"><fmt:message key="table.volume" /></th>
							<th scope="col"><fmt:message key="table.cost" /></th>
							<th scope="col"><fmt:message key="table.status" /></th>
							<th scope="col"><a
								href="viewOrders?orderBy=delivery_date&recordsPerPage=${recordsPerPage}&whereName=${whereName}&whereValue=${whereValue}"
								class="text-decoration-none"> <i
									class="bi bi-sort-alpha-down"></i>
							</a> <fmt:message key="table.deliveryDate" /> <a
								href="viewOrders?orderBy=delivery_date&sortOrder=DESC&recordsPerPage=${recordsPerPage}&whereName=${whereName}&whereValue=${whereValue}"
								class="text-decoration-none"> <i
									class="bi bi-sort-alpha-down-alt"></i>
							</a></th>
							<th><fmt:message key="table.order_creation_time" /></th>
							<th scope="col"><fmt:message key="table.action" /></th>
						</tr>
					</thead>
					<tbody>

						<c:forEach var="order" items="${orders}" varStatus="status">
							<tr>
								<td scope="row"><c:out
										value="${status.count + recordsPerPage * currentPage - recordsPerPage}" /></td>
								<td>${order.account.login}</td>
								<td>${order.account.accountDetails.name}<br>
									${order.account.accountDetails.surname}
								</td>
								<td>${order.route.cityFrom.name}</td>
								<td>${order.route.cityTo.name}</td>
								<td>${order.address}</td>
								<td>${order.cargoType.name}</td>
								<td>${order.route.distance}</td>
								<td>${order.weight}</td>
								<td>${order.volume}</td>
								<td>${order.price}</td>
								<td><a
									href="viewOrders?recordsPerPage=${recordsPerPage}&orderBy=${orderBy}&sortOrder=${sortOrder}&whereName=status&whereValue=${order.orderStatus}"
									class="text-decoration-none">
									    <st:status orderStatus="${order.orderStatus}" />
									</a>
								</td>
								<td><ctag:date_view date="${order.deliveryDate}" /></td>
								<td>${order.createTime}</td>
								<td>
									<form method="post" action="changeStatus">
										<input type="hidden" name="orderId" value="${order.id}">
										<input type="hidden" name="orderBy" value="${orderBy}">
										<input type="hidden" name="sortOrder" value="${sortOrder}">
										<input type="hidden" name="whereName" value="${whereName}">
										<input type="hidden" name="whereValue" value="${whereValue}">
										<input type="hidden" name="recordsPerPage" value="${recordsPerPage}">
										<input type="hidden" name="currentPage" value="${currentPage}">
											<select name="status">
												<option value="" selected>Open this select menu</option>
												<c:forEach var="value" items="${Status.values()}">
													<c:if test="${value ne order.orderStatus and value ne Status.PAID}">
														<option value="${value}">${value}</option>
													</c:if>
												</c:forEach>
											</select>
										<button type="submit" class="btn btn-primary">
											<fmt:message key="table.changeStatus" />
										</button>
									</form>
								</td>
							</tr>
						</c:forEach>

					</tbody>
				</table>
			</div>

			<nav>
				<ul class="pagination justify-content-center">
					<c:if test="${currentPage ne 1}">
						<li class="page-item"><c:url value="viewOrders" var="prUrl">
								<c:param name="recordsPerPage" value="${recordsPerPage}" />
								<c:param name="currentPage" value="${currentPage-1}" />
								<c:param name="orderBy" value="${orderBy}" />
								<c:param name="sortOrder" value="${sortOrder}" />
								<c:param name="whereName" value="${whereName}" />
								<c:param name="whereValue" value="${whereValue}" />
							</c:url> <a class="page-link" href='<c:out value="${prUrl}"/>'><fmt:message
									key="pagination.previous" /></a></li>
					</c:if>

					<c:forEach begin="1" end="${numOfPages}" var="i">
						<c:choose>
							<c:when test="${currentPage eq i}">
								<li class="page-item active"><a class="page-link"> ${i}
								</a></li>
							</c:when>
							<c:otherwise>
								<li class="page-item"><a class="page-link"
									href="viewOrders?recordsPerPage=${recordsPerPage}&currentPage=${i}&orderBy=${orderBy}&sortOrder=${sortOrder}&whereName=${whereName}&whereValue=${whereValue}">${i}</a>
								</li>
							</c:otherwise>
						</c:choose>
					</c:forEach>

					<c:if test="${currentPage lt numOfPages}">
						<li class="page-item"><a class="page-link"
							href="viewOrders?recordsPerPage=${recordsPerPage}&currentPage=${currentPage+1}&orderBy=${orderBy}&sortOrder=${sortOrder}&whereName=${whereName}&whereValue=${whereValue}"><fmt:message
									key="pagination.next" /></a></li>
					</c:if>
				</ul>
			</nav>

		</div>
	</main>

	<jsp:include page="fragments/footer.jsp" />
</body>
</html>