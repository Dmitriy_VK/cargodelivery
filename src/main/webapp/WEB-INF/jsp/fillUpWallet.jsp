<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<fmt:setLocale value="${sessionScope.locale}" scope="session" />
<fmt:setBundle basename="lang" />

<html>
<head>
<title>CargoDelivery</title>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/static/css/bootstrap.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/static/css/bootstrap-icons-1.10.3/bootstrap-icons.css">
<script
	src="${pageContext.request.contextPath}/static/js/jquery-3.6.0.js"></script>
<script
	src="${pageContext.request.contextPath}/static/js/bootstrap.bundle.js"></script>
</head>

<body class="bg-info text-center">
	<jsp:include page="fragments/header.jsp" />

	<main>
	    <div class="container  mt-4">
	    <div class="row justify-content-center">
		<c:choose>
        	<c:when test="${empty sessionScope.account}">
        		<p class="rounded-pill">
        			<fmt:message key="fillUpWalletPage.noLogin" />
        		</p>
        	</c:when>
        	<c:otherwise>
        		<c:if test="${requestScope.fillUpWalletResult eq 'true'}">
					<p class="rounded-pill">
						<fmt:message key="fillUpWalletPage.success" /> <c:out value="${amountOfMoney}" /> <fmt:message key="fillUpWalletPage.hryvna" />
					</p>
				</c:if>
				<table class="table table-bordered border-primary table-sm w-auto">
					<thead>
						<tr>
							<th scope="col"><fmt:message key="fillUpWalletPage.title" /> ${sessionScope.account.login}</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>
							<form action="fillUpWallet" method="post">
							<input type="hidden" name="formData" value="formData">

                                <label for="amount" class="form-label"><fmt:message key="fillUpWalletPage.amount" /></label>
                                <input type="text" class="form-control" id="amount" name="amount">
                                <c:if test="${requestScope.error_amountOfMoney}">
                                	<div class="text-danger">
                                		<p>
                                			<fmt:message key="fillUpWallet.error_amountOfMoney" />
                                		</p>
                                	</div>
                                </c:if>

                              <button type="submit" class="btn btn-primary mt-2"><fmt:message key="fillUpWalletPage.submit" /></button>
                            </form>
                            </td>
						</tr>

					</tbody>
				</table>
            </c:otherwise>
		</c:choose>
		</div>
	</main>

	<jsp:include page="fragments/footer.jsp" />

</body>
</html>