<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ page import="org.finaltask.entity.Status"%>

<fmt:setLocale value="${sessionScope.locale}" scope="session" />
<fmt:setBundle basename="lang" />

<div class="row justify-content-center border-bottom">

	<div class="col-md-auto">
    	<div class="dropdown">
            <button class="btn btn-outline-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-bs-toggle="dropdown">
                <fmt:message key="manager.menu" /><span class="caret"></span>
            </button>
            <div class="dropdown-menu">
              <a class="dropdown-item" href="registration"><fmt:message key="manager.menu.registration" /></a>
              <a class="dropdown-item" href="viewProfile?action=view"><fmt:message key="user.menu.viewProfile" /></a>
              <a class="dropdown-item" href="viewProfile?action=edit"><fmt:message key="user.menu.editProfile" /></a>
              <a class="dropdown-item" href="viewAccounts?role=user"><fmt:message key="user.menu.viewUsers" /></a>
              <a class="dropdown-item" href="viewAccounts?role=manager"><fmt:message key="user.menu.viewManagers" /></a>
              <a class="dropdown-item" href="addRoute"><fmt:message key="manager.menu.addRoute" /></a>
            </div>
        </div>
    </div>

	<div class="col-md-auto">
		<form method="post" action="viewOrders">
			<input type="hidden" name="whereName" value="status"> <select
				name="whereValue">
				<option value="all" selected><fmt:message
						key="manager.menu.showAllOrders" /></option>
				<c:forEach var="value" items="${Status.values()}">
					<option value="${value}">${value}</option>
				</c:forEach>
			</select>
			<button type="submit" class="btn btn-primary">
				<fmt:message key="manager.menu.button" />
			</button>
		</form>
	</div>

	<div class="col-md-auto">
		<form method="get" action="viewDateReport">
			<c:choose>
				<c:when test="${requestScope.error_deliveryDate}">
					<div class="text-danger">
						<fmt:message key="error.error_deliveryDate" />
					</div>
				</c:when>
				<c:otherwise>
					<fmt:message key="manager.menu.view_date_report" />
				</c:otherwise>
			</c:choose>
			<div class="d-flex">
				<input class="form-control w-auto" type="text" placeholder="01.01.2001" name="deliveryDate">
				<button class="btn btn-success text-nowrap" type="submit">
					<fmt:message key="manager.menu.button" />
				</button>
			</div>
		</form>
	</div>

	<div class="col-md-auto">

		<form method="get" action="viewCityReport">
			<c:choose>
				<c:when test="${requestScope.error_city}">
					<div class="text-danger">
						<p>
							<fmt:message key="error.error_deliveryDate" />
						</p>
					</div>
				</c:when>
				<c:otherwise>
					<fmt:message key="manager.menu.view_city_report" />
				</c:otherwise>
			</c:choose>
			<div class="d-flex flex-row">
				<input class="form-control w-auto" type="text"
					placeholder="Enter ciy name" name="whereValue">
				<button class="btn btn-success text-nowrap" type="submit">
					<fmt:message key="manager.menu.button" />
				</button>
			</div>
			<div class="form-check text-start">
				<input type="radio" class="form-check-input" name="whereName" value="city_from" id="radio1" autocomplete="off" checked>
				<label class="form-check-label" for="radio1">
					<fmt:message key="manager.menu.departureCity" />
				</label>
			</div>
			<div class="form-check text-start">
				 <input type="radio" class="form-check-input" name="whereName" value="city_to" id="radio2" autocomplete="off">
				 <label	class="form-check-label" for="radio2">
				 	<fmt:message key="manager.menu.destinationCity" />
				</label>
			</div>
		</form>
	</div>

	<div class="col-md-auto">
		<div class="badge bg-primary text-nowrap" style="width: 8rem;">
			<fmt:message key="manager.menu.wallet" /> :</br>
			<c:out value="${sessionScope.account.wallet.value}" />
			<fmt:message key="user.menu.currency" />

		</div>
	</div>
</div>

