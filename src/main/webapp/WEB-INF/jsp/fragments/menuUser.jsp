<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ page import="org.finaltask.entity.Status"%>

<fmt:setLocale value="${sessionScope.locale}" scope="session" />
<fmt:setBundle basename="lang" />
<div class="row  justify-content-center border-bottom">

    <div class="col-md-auto">
    	<div class="dropdown">
            <button class="btn btn-outline-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-bs-toggle="dropdown">
                <fmt:message key="user.menu.profile" /><span class="caret"></span>
            </button>
            <div class="dropdown-menu">
              <a class="dropdown-item" href="viewProfile?action=view"><fmt:message key="user.menu.viewProfile" /></a>
              <a class="dropdown-item" href="viewProfile?action=edit"><fmt:message key="user.menu.editProfile" /></a>
            </div>
        </div>
    </div>

	<div class="col-md-auto">	
		<a class="btn btn-outline-primary me-2" href="home">
		    <fmt:message key="user.menu.makeOrder" />
		</a>
	</div>

	<div class="col-md-auto">
				<form method="post" action="viewOrders">
				    <select name="status">
                        <option value="" selected>Open this select menu</option>
                        <c:forEach var="value" items="${Status.values()}">
                            <option value="${value}">${value}</option>
                        </c:forEach>
                    </select>
                    <button type="submit" class="btn btn-outline-primary"><fmt:message key="user.menu.viewOrders"/></button>
				</form>
	</div>
	
	<div class="col-md-auto">
		 <a class="btn btn-outline-primary me-2" href="fillUpWallet"> <fmt:message
				key="user.menu.fillUpWallet" /></a>
	</div>
					
	<div class="col-md-auto">
        <div class="badge bg-primary text-wrap" style="width: 8rem;">
            <fmt:message key="user.menu.wallet"/>: <br>
            <c:out value="${sessionScope.account.wallet.value}"/> <fmt:message key="user.menu.currency"/></button>
        </div>
     </div>
	
</div>

