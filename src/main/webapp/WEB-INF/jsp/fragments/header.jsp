<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ page import="org.finaltask.entity.RoleEnum"%>

<fmt:setLocale value="${sessionScope.locale}" scope="session" />
<fmt:setBundle basename="lang" />
<header class="container">
	<div class="d-flex flex-wrap align-items-center justify-content-center justify-content-md-between py-3 mb-4 border-bottom">
		<a href="home" class="d-flex align-items-center col-md-3 mb-2 mb-md-0 text-dark text-decoration-none">
			<fmt:message key="fragments.mainPage" />
		</a>

		<c:if test="${empty sessionScope.account}">
			<div class="col-md-3 text-end">
				<a class="btn btn-outline-primary me-2" href="login">
				    <fmt:message key="fragments.login" />
				</a>
				<a class="btn btn-outline-primary me-2" href="registration">
				    <fmt:message key="fragments.sign_up" />
				</a>
			</div>
		</c:if>

		<c:if test="${not empty sessionScope.account}">
			<div class="badge bg-primary text-wrap" style="width: 8rem;">
				<fmt:message key="fragments.account_info" />
				<c:out value="${sessionScope.account.role.name}" />
				<c:out value="${sessionScope.account.login}" />
			</div>
			<div class="col-md-3 text-end">
				<a class="btn btn-outline-primary me-2" href="logout"><fmt:message
						key="fragments.logout" /></a>
			</div>
		</c:if>

		<button type="button" class="btn btn-dark dropdown-toggle"
			data-bs-toggle="dropdown">
			<fmt:message key="fragments.language" />
		</button>

		<ul class="dropdown-menu">
			<li><c:url value="locale" var="enUrl">
					<c:param name="lang" value="en" />
				</c:url> <a class="dropdown-item" href='<c:out value="${enUrl}"/>'><fmt:message
						key="fragments.english" /></a></li>
			<li><c:url value="locale" var="ukUrl">
					<c:param name="lang" value="uk" />
				</c:url> <a class="dropdown-item" href='<c:out value="${ukUrl}"/>'><fmt:message
						key="fragments.ukrainian" /></a></li>
		</ul>
	</div>

	<div>
		<c:if
			test="${not empty sessionScope.account and sessionScope.account.role.name eq RoleEnum.USER}">
			<c:import url="fragments/menuUser.jsp" />
		</c:if>
		<c:if
			test="${not empty sessionScope.account and sessionScope.account.role.name eq RoleEnum.MANAGER}">
			<c:import url="fragments/menuManager.jsp" />
		</c:if>
	</div>
</header>

