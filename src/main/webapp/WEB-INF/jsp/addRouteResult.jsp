<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<fmt:setLocale value="${sessionScope.locale}" scope="session" />
<fmt:setBundle basename="lang" />

<html>
<head>
<title>CargoDelivery</title>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/static/css/bootstrap.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/static/css/bootstrap-icons-1.10.3/bootstrap-icons.css">
<script
	src="${pageContext.request.contextPath}/static/js/jquery-3.6.0.js"></script>
<script
	src="${pageContext.request.contextPath}/static/js/bootstrap.bundle.js"></script>
</head>
<body class="bg-info text-center">
	<c:import url="fragments/header.jsp" />

	<main>
		<div class="container mt-2">
			<div class="row justify-content-md-center">
				<c:choose>

					<c:when test="${not empty requestScope.result}">

                        <c:if test="${requestScope.result eq 'true'}">
							<div class="text-danger">
								<fmt:message key="addRouteResult.success" />
							</div>
						</c:if>

                        <c:if test="${requestScope.result eq 'false'}">
							<div class="text-danger">
								<fmt:message key="addRouteResult.failure" />
							</div>
						</c:if>

					</c:when>

					<c:otherwise>
                              No information to display
                    </c:otherwise>

				</c:choose>
			</div>
		</div>
	</main>

	<c:import url="fragments/footer.jsp" />

</body>
</html>