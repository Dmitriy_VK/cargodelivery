<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<fmt:setLocale value="${sessionScope.locale}" scope="session" />
<fmt:setBundle basename="lang" />

<html>
<head>
<title>CargoDelivery</title>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/static/css/bootstrap.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/static/css/bootstrap-icons-1.10.3/bootstrap-icons.css">
<script
	src="${pageContext.request.contextPath}/static/js/jquery-3.6.0.js"></script>
<script
	src="${pageContext.request.contextPath}/static/js/bootstrap.bundle.js"></script>
</head>
<body class="bg-info">
	<jsp:include page="fragments/header.jsp" />
	<main class="container">
	<div class="row justify-content-center">
	<div class="col-md-auto">
	<div class="form-signin">
		<c:if test="${requestScope.user_exist}">
			<div class="text-danger">
				<p>
					<fmt:message key="login.info_user_exist" />
				</p>
				<p>
					<c:out value="${sessionScope.account.login}" />
				</p>
			</div>
		</c:if>
		<form action="login" method="post">
			<input type="hidden" name="formData" value="formData">

			<h1 class="h3 mb-3 fw-normal"><fmt:message key="login.signin" /></h1>

			<div class="form-floating">
				<input type="text" class="form-control" id="floatingInput"
					placeholder="login" name="userName"> <label for="floatingInput"><fmt:message key="login.login" /></label>
				<c:if test="${requestScope.error_userName}">
					<div class="text-danger">
						<p>
							<fmt:message key="login.error_userName" />
						</p>
					</div>
				</c:if>
			</div>

			<div class="form-floating">
				<input type="password" class="form-control" id="floatingPassword"
					placeholder="Password" name="password"> <label for="floatingPassword"><fmt:message key="login.password" /></label>
				<c:if test="${requestScope.error_password}">
					<div class="text-danger">
						<p>
							<fmt:message key="login.error_password" />
						</p>
					</div>
				</c:if>
			</div>


			<button class="w-100 btn btn-lg btn-primary" type="submit"><fmt:message key="login.button.signin" /></button>

		</form>
	</div>
	</div>
	</div>
	</main>
	
	<jsp:include page="fragments/footer.jsp" />

</body>
</html>