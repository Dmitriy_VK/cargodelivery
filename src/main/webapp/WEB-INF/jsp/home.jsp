<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ page import="org.finaltask.entity.RoleEnum"%>

<fmt:setLocale value="${sessionScope.locale}" scope="session"/>
<fmt:setBundle basename="lang"/>

<html>
<head>
    <title>CargoDelivery</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/bootstrap.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/bootstrap-icons-1.10.3/bootstrap-icons.css">
    <script src="${pageContext.request.contextPath}/static/js/jquery-3.6.0.js"></script>
    <script src="${pageContext.request.contextPath}/static/js/bootstrap.bundle.js"></script>
</head>
<body class="bg-info">
  <jsp:include page="fragments/header.jsp"/>

<main>
    <div class="container">
        <form action="home" >
            <div class="row justify-content-start">
                 <div class="col-md-auto p-2">
                    <label for="selnum"><fmt:message key="home.show"/></label>
                 </div>
                 <div class="col-md-auto p-2">
                    <select name="recordsPerPage" class="form-control form-control-sm" id="selnum">
                        <option selected disabled><c:out value="${recordsPerPage}"/></option>
                        <option value="5">5</option>
                        <option value="10">10</option>
                        <option value="15">15</option>
                        <option value="20">20</option>
                    </select>
                 </div>
                 <div class="col-md-auto p-2">
                    <fmt:message key="home.entries"/>
                 </div>
                    <input type="hidden" name="orderBy" value="${orderBy}">
                    <input type="hidden" name="sortOrder" value="${sortOrder}">
                    <input type="hidden" name="where" value="${where}">
                    <input type="hidden" name="cityId" value="${cityId}">
                 <div class="col-md-auto p-2">
                    <button type="submit" class="btn btn-primary mb-2"><fmt:message key="button.submit"/></button>
                 </div>
            </div>
        </form>
    </div>

<div class="container">

<table class="table table-light table-striped table-bordered">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">
        <a href="home?orderBy=city_from&recordsPerPage=${recordsPerPage}&where=${where}&cityId=${cityId}" class="text-decoration-none">
            <i class="bi bi-sort-alpha-down"></i>
        </a>
            <fmt:message key="table.city_from"/>
         <a href="home?orderBy=city_from&sortOrder=DESC&recordsPerPage=${recordsPerPage}&where=${where}&cityId=${cityId}" class="text-decoration-none">
            <i class="bi bi-sort-alpha-down-alt"></i>
        </a>
        </th>
      <th scope="col">
        <a href="home?orderBy=city_to&recordsPerPage=${recordsPerPage}&where=${where}&cityId=${cityId}" class="text-decoration-none">
                    <i class="bi bi-sort-alpha-down"></i>
                </a>
            <fmt:message key="table.city_to"/>
       <a href="home?orderBy=city_to&sortOrder=DESC&recordsPerPage=${recordsPerPage}&where=${where}&cityId=${cityId}" class="text-decoration-none">
                   <i class="bi bi-sort-alpha-down-alt"></i>
               </a>
        </th>
      <th scope="col"><fmt:message key="table.distance"/></th>
      <th scope="col"><fmt:message key="table.action"/></th>
      <c:if test="${not empty sessionScope.account and sessionScope.account.role.name eq RoleEnum.USER}">
      	<th scope="col"><fmt:message key="table.makeOrder"/></th>
      </c:if>
    </tr>
  </thead>
  <tbody>

   <c:forEach var="route" items="${routes}" varStatus="status">
   <tr>
         <th scope="row"><c:out value="${status.count + recordsPerPage * currentPage - recordsPerPage}"/></th>
         <td>
            <a href="home?recordsPerPage=${recordsPerPage}&orderBy=${orderBy}&sortOrder=${sortOrder}&where=city_id_from&cityId=${route.cityFrom.id}"
            class="text-decoration-none">
                ${route.cityFrom.name}
            </a>
         </td>
         <td>
            <a href="home?recordsPerPage=${recordsPerPage}&orderBy=${orderBy}&sortOrder=${sortOrder}&where=city_id_to&cityId=${route.cityTo.id}"
                     class="text-decoration-none">
                ${route.cityTo.name}
            </a>
         </td>
         <td>${route.distance} <fmt:message key="form.km"/></td>
         <td>
         	<a class="btn btn-secondary btn-sm" href="calculateForm?id=${route.id}" role="button">
         		<fmt:message key="table.calculate"/>
         	</a>
         </td>
         <c:if test="${not empty sessionScope.account and sessionScope.account.role.name eq  RoleEnum.USER}">
         	<td>
         		<a class="btn btn-secondary btn-sm" href="makeOrder?routeId=${route.id}&accountId=${sessionScope.account.id}" role="button">
         			<fmt:message key="table.makeOrder"/>
         		</a>
         	</td>
         </c:if>
     </tr>
      </c:forEach>

  </tbody>
</table>
</div>

<nav>
  <ul class="pagination justify-content-center">
            <c:if test="${currentPage ne 1}">
                <li class="page-item">
                    <c:url value="home" var="prUrl">
                        <c:param name="recordsPerPage" value="${recordsPerPage}"/>
                        <c:param name="page" value="${currentPage-1}"/>
                        <c:param name="orderBy" value="${orderBy}"/>
                        <c:param name="sortOrder" value="${sortOrder}"/>
                        <c:param name="where" value="${where}"/>
                        <c:param name="cityId" value="${cityId}"/>
                    </c:url>
                    <a class="page-link" href='<c:out value="${prUrl}"/>'><fmt:message key="pagination.previous"/></a>
                </li>
            </c:if>

            <c:forEach begin="1" end="${numOfPages}" var="i">
                <c:choose>
                    <c:when test="${currentPage eq i}">
                        <li class="page-item active">
                        <a class="page-link"> ${i} </a>
                        </li>
                    </c:when>
                    <c:otherwise>
                        <li class="page-item"><a class="page-link"
                            href="home?recordsPerPage=${recordsPerPage}&page=${i}&orderBy=${orderBy}&sortOrder=${sortOrder}&where=${where}&cityId=${cityId}">${i}</a>
                        </li>
                    </c:otherwise>
                </c:choose>
            </c:forEach>

            <c:if test="${currentPage lt numOfPages}">
                <li class="page-item"><a class="page-link"
                    href="home?recordsPerPage=${recordsPerPage}&page=${currentPage+1}&orderBy=${orderBy}&sortOrder=${sortOrder}&where=${where}&cityId=${cityId}"><fmt:message key="pagination.next"/></a>
                </li>
            </c:if>
        </ul>
</nav>

</main>

    <jsp:include page="fragments/footer.jsp"/>


</body>
</html>