<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<fmt:setLocale value="${sessionScope.locale}" scope="session" />
<fmt:setBundle basename="lang" />

<html>
<head>
<title>CargoDelivery</title>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/static/css/bootstrap.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/static/css/bootstrap-icons-1.10.3/bootstrap-icons.css">
<script
	src="${pageContext.request.contextPath}/static/js/jquery-3.6.0.js"></script>
<script
	src="${pageContext.request.contextPath}/static/js/bootstrap.bundle.js"></script>
</head>
<body class="bg-info">
	<jsp:include page="fragments/header.jsp" />

	<main>

		<c:choose>
			<c:when test="${not empty sessionScope.routeCalcPage and not empty sessionScope.cargoTypeList}">
				<div class="container">
					<div class="row justify-content-center">
						<div class="col-md-auto">
							<fmt:message key="page.route" />
						</div>
					</div>
					<div class="row justify-content-center">
						<div class="col-md-auto">
							<p>
								<c:out value="${sessionScope.routeCalcPage.cityFrom.name}" />
								<i class="bi bi-arrow-right"></i>
								<c:out value="${sessionScope.routeCalcPage.cityTo.name}" />
								<c:out value="${routeCalcPage.distance}" />
								<fmt:message key="form.km" />
							</p>
						</div>
					</div>
				</div>

				<div class="container">
					<div class="row justify-content-center">
						<div class="col-md-auto">
							<form action="calculate" method="post">
								<div class="col">
									<div class="mb-3">
										<input type="hidden" name="formData" value="formData">
										<label for="cargoType" class="form-label">
										    <fmt:message key="form.cargo_type" />
										</label>
										<select class="form-select" id="cargoType" name="cargoTypeId">
											<option selected disabled>
											    Open this select menu
											</option>
											<c:forEach var="cargoType" items="${sessionScope.cargoTypeList}">
                                                <option value="${cargoType.id}">
                                            		${cargoType.name}
                                            	</option>
                                            </c:forEach>
										</select>
										<c:if test="${requestScope.error_cargoType}">
											<div class="text-danger">
												<fmt:message key="form.error_cargoType" />
											</div>
										</c:if>
									</div>
									<div class="mb-3">
										<label for="weight" class="form-label"><fmt:message
												key="form.weight" /></label> <input type="text"
											class="form-control" id="weight" name="weight">
										<c:if test="${requestScope.error_weight}">
											<div class="text-danger">
												<fmt:message key="form.error_weight" />
											</div>
										</c:if>
									</div>
									<div class="mb-3">
										<label for="volume" class="form-label"><fmt:message
												key="form.volume" /></label> <input type="text"
											class="form-control" id="volume" name="volume">
										<c:if test="${requestScope.error_volume}">
											<div class="text-danger">
												<fmt:message key="form.error_volume" />
											</div>
										</c:if>
									</div>
									<input type="hidden" name="routeId" value="${route.id}">
									<button type="submit" class="btn btn-primary">
										<fmt:message key="calcForm.calculate" />
									</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</c:when>
			<c:otherwise>
			    <div class="container">
                	<div class="row justify-content-center text-danger">
				        <fmt:message key="calcForm.wrong_data" />
				    </div>
				</div>
			</c:otherwise>
		</c:choose>
	</main>
	<jsp:include page="fragments/footer.jsp" />
</body>
<c:remove var="error_volume" scope="session" />
</html>