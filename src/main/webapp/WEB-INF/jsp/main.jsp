<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>CargoDelivery</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/bootstrap.css">
</head>
<body>

<header>
    <jsp:include page="fragments/header.jsp"/>
</header>

<div class="container-fluid" id="pageBody">
    <c:choose>
        <c:when test="${ResourcesWithAsideMenu.RESOURCES.contains(requestScope['javax.servlet.forward.servlet_path'])
                        and main_block ne '404.jsp' and main_block ne '500.jsp'}">
            <div class="row">
                <aside class="col-sm-3">
                    <jsp:include page="aside_blocks/aside_menu.jsp"/>
                </aside>
                <main class="col-sm-9">
                    <jsp:include page="main_blocks/${main_block}"/>
                </main>
            </div>
        </c:when>
        <c:otherwise>
            <main class="col-sm-6 offset-sm-3">
                <jsp:include page="main_blocks/${main_block}"/>
            </main>
        </c:otherwise>
    </c:choose>
</div>

<footer>
    <jsp:include page="fragments/footer.jsp"/>
</footer>

</body>
</html>