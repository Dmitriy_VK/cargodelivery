package org.finaltask.utils;

import org.finaltask.entity.Route;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Cost {
    public static double calculate(double weight, int volume, Route route, Double ratio) {
        return new BigDecimal(weight
                * volume
                * route.getDistance()
                * route.getTariff()
                * ratio
                / 100)
                .setScale(2, RoundingMode.CEILING).doubleValue();
    }
}
