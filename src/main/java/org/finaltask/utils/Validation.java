package org.finaltask.utils;


import java.util.regex.Pattern;

public class Validation {
	private final static String REGEX_EMAIL = "^([\\w\\-.]+)@([\\w\\-.]+)\\.([a-zA-Z]{2,5})$";
    private final static String REGEX_PASSWORD = "^(\\w{7,20})$";
    private final static String REGEX_NAME = "^[a-zA-Zа-яА-Яє-їЄ-Ї\\s]{2,20}$";
    private final static String REGEX_LOGIN = "^[A-Za-z0-9]+([A-Za-z0-9]*|[._-]?[A-Za-z0-9]+)*$";
    private final static String REGEX_PHONE = "^[+]?[(]?[0-9]{3}[)]?[-\\s.]?[0-9]{3}[-\\s.]?[0-9]{4,6}$";
    private final static String REGEX_MONEY = "^\\d*\\.?\\d{0,2}$";
    private final static String REGEX_DATE = "^(0[1-9]|[12][0-9]|3[01])\\.(0[1-9]|1[012])\\.(19|20)\\d\\d$";
    private final static String REGEX_WEIGHT = "^\\.0*[1-9]\\d*$|^0(\\.0*[1-9]\\d*)$|^[1-9]\\d*?(\\.\\d+)?$";
    private final static String REGEX_VOLUME = "^[1-9]\\d*$";

    

    public static boolean validateEmail(String email) {
        return Pattern.matches(REGEX_EMAIL, email);
    }

    public static boolean validatePassword(String password) {
        return Pattern.matches(REGEX_PASSWORD, password);
    }

    public static boolean validateName(String name) {
        return Pattern.matches(REGEX_NAME, name);
    }
    
    public static boolean validateLogin(String login) {
        return Pattern.matches(REGEX_LOGIN, login);
    }
    public static boolean validatePhone(String phone) {
        return Pattern.matches(REGEX_PHONE, phone);
    }
    public static boolean validateMoney(String money) {
        return Pattern.matches(REGEX_MONEY, money);
    }
    public static boolean validateDate(String date) {
        return Pattern.matches(REGEX_DATE, date);
    }
    public static boolean validateWeight(String weight) {
        return Pattern.matches(REGEX_WEIGHT, weight);
    }
    public static boolean validateVolume(String volume) {
        return Pattern.matches(REGEX_VOLUME, volume);
    }
}
