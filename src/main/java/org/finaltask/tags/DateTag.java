package org.finaltask.tags;

import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;
import java.sql.Date;
import java.text.DateFormat;
import java.util.Locale;

public class DateTag extends TagSupport {
    private Date date;

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public int doStartTag() throws JspException {
        try {
            Locale locale;
            HttpSession session = pageContext.getSession();
            if (session != null && session.getAttribute("locale") != null) {
                locale = new Locale((String) session.getAttribute("locale"));
            } else {
                locale = new Locale("en");
            }
            DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.DEFAULT, locale);
            String dateFormatted = dateFormat.format(date);
            pageContext.getOut().write("<p>" + dateFormatted + "</p>");
        } catch (IOException e) {
            throw new JspException(e.getMessage());
        }
        return SKIP_BODY;
    }
}
