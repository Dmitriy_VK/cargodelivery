package org.finaltask.dao.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.finaltask.dao.AbstractDao;
import org.finaltask.dao.DBException;
import org.finaltask.entity.City;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CityDaoImpl extends AbstractDao<City> {

    private static final Logger logger = LogManager.getLogger(CityDaoImpl.class);

    static final String ID = "id";
    static final String NAME = "name";
    static final String SQL_FIND_ALL = "SELECT * FROM `city`";
    static final String SQL_FIND_ID_BY_NAME = "SELECT id FROM `city` WHERE `name` = ?";
    static final String SQL_FIND_CITY_BY_ID = "SELECT * FROM `city` WHERE `id` = ?";

    public CityDaoImpl(Connection connection) {
        super(connection);
    }

    @Override
    public List<City> findAll() throws DBException {
        Statement statement = null;
        ResultSet resultSet = null;
        List<City> cityList = new ArrayList<>();

        try {
            statement = connection.createStatement();
            resultSet = statement.executeQuery(SQL_FIND_ALL);

            while (resultSet.next()) {
                City city = new City();

                city.setId(resultSet.getLong(ID));
                city.setName(resultSet.getString(NAME));

                cityList.add(city);
            }

        } catch (SQLException e) {
            logger.error("Error while connecting to table city", e);
            throw new DBException("Failed to connect table city", e);
        } finally {
            close(resultSet);
            close(statement);
        }
        return cityList;
    }

    @Override
    public City findEntityById(long id) throws DBException {
        PreparedStatement prdStatement = null;
        ResultSet resultSet = null;
        City city = new City();

        try {
            prdStatement = connection.prepareStatement(SQL_FIND_CITY_BY_ID);
            prdStatement.setLong(1, id);
            resultSet = prdStatement.executeQuery();

            while (resultSet.next()) {
                city.setId(resultSet.getLong(ID));
                city.setName(resultSet.getString(NAME));
            }

        } catch (SQLException e) {
            logger.error("Error while connecting to table city", e);
            throw new DBException("Failed to connect table city", e);
        } finally {
            close(resultSet);
            close(prdStatement);
        }
        return city;
    }

    @Override
    public boolean delete(long id) throws DBException {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean delete(City entity) throws DBException {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean create(City entity) throws DBException {
        throw new UnsupportedOperationException();
    }

    @Override
    public City update(City entity) throws DBException {
        throw new UnsupportedOperationException();
    }

    public int findIdByName(String name) throws DBException {
        PreparedStatement prdStatement = null;
        ResultSet resultSet = null;
        int id = 0;

        try {
            prdStatement = connection.prepareStatement(SQL_FIND_ID_BY_NAME);
            prdStatement.setString(1, name);
            resultSet = prdStatement.executeQuery();

            if (resultSet.next()) {
                id = resultSet.getInt(ID);
            }

        } catch (SQLException e) {
            logger.error("Error while connecting to table city", e);
            throw new DBException("Failed to connect table city", e);
        } finally {
            close(resultSet);
            close(prdStatement);
        }
        return id;
    }
}
