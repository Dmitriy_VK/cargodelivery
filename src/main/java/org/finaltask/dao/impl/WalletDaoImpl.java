package org.finaltask.dao.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.finaltask.dao.AbstractDao;
import org.finaltask.dao.DBException;
import org.finaltask.entity.Wallet;

import java.sql.*;
import java.util.List;

public class WalletDaoImpl extends AbstractDao<Wallet> {
	
	private static final Logger logger = LogManager.getLogger(WalletDaoImpl.class);
	
    static final String ID = "id";
    public static final String VALUE = "value";
    static final String SQL_ADD_WALLET = "INSERT wallet(value) VALUE(?)";
    static final String SQL_UPDATE_WALLET = "UPDATE `wallet` SET `value` = ? WHERE `id` = ?";
    static final String SQL_FIND_WALLET_BY_ID = "SELECT * FROM `wallet` WHERE `id` = ?";

    public WalletDaoImpl(Connection connection) {
        super(connection);
    }

    @Override
    public List<Wallet> findAll() throws DBException {
        throw new UnsupportedOperationException();
    }

    @Override
    public Wallet findEntityById(long id) throws DBException {
        PreparedStatement prdStatement = null;
        ResultSet resultSet = null;
        Wallet wallet = new Wallet();

        try {
            prdStatement = connection.prepareStatement(SQL_FIND_WALLET_BY_ID);
            prdStatement.setLong(1, id);
            resultSet = prdStatement.executeQuery();

            while (resultSet.next()) {
                wallet.setId(resultSet.getLong(ID));
                wallet.setValue(resultSet.getDouble(VALUE));
            }
        } catch (SQLException e) {
        	logger.error("Error while connecting to table wallet", e);
            throw new DBException("Failed to connect table wallet", e);
        } finally {
            close(resultSet);
            close(prdStatement);
        }
        return wallet;
    }

    @Override
    public boolean delete(long id) throws DBException {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean delete(Wallet entity) throws DBException {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean create(Wallet wallet) throws DBException {

        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            preparedStatement = connection.prepareStatement(SQL_ADD_WALLET, Statement.RETURN_GENERATED_KEYS);

            preparedStatement.setDouble(1, wallet.getValue());

            if (preparedStatement.executeUpdate() > 0) {
                resultSet = preparedStatement.getGeneratedKeys();
                if (resultSet.next()) {
                    wallet.setId(resultSet.getLong(1));
                }
                return true;
            }
        } catch (SQLException e) {
        	logger.error("Error while connecting to table wallet", e);
            throw new DBException("Failed to connect table wallet", e);
        } finally {
            close(resultSet);
            close(preparedStatement);
        }

        return false;
    }

    @Override
    public Wallet update(Wallet wallet) throws DBException {
        PreparedStatement prdStatement = null;

        try {
            prdStatement = connection.prepareStatement(SQL_UPDATE_WALLET);
            int i = 1;
            prdStatement.setDouble(i++, wallet.getValue());
            prdStatement.setLong(i, wallet.getId());
            prdStatement.executeUpdate();
        } catch (SQLException e) {
        	logger.error("Error while connecting to table wallet", e);
            throw new DBException("Failed to connect table wallet", e);
        } finally {
            close(prdStatement);
        }
        return wallet;
    }
}
