package org.finaltask.dao.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.finaltask.dao.AbstractDao;
import org.finaltask.dao.DBException;
import org.finaltask.entity.CargoType;
import org.finaltask.entity.Language;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class LanguageDaoImpl extends AbstractDao<Language> {
    private static final Logger logger = LogManager.getLogger(LanguageDaoImpl.class);
    static final String ID = "id";
    static final String NAME = "name";
    static final String CODE = "code";
    static final String SQL_FIND_LANGUAGE_BY_LANGUAGE_CODE = "SELECT * FROM `language` WHERE `code` = ?";
    public LanguageDaoImpl(Connection connection) {
        super(connection);
    }

    @Override
    public List<Language> findAll() throws DBException {
        throw new UnsupportedOperationException();
    }

    @Override
    public Language findEntityById(long id) throws DBException {
        throw new UnsupportedOperationException();
    }

    public Language findEntityByLanguageCode(String languageCode) throws DBException {
        PreparedStatement prdStatement = null;
        ResultSet resultSet = null;
        Language language = null;
        try {
            prdStatement = connection.prepareStatement(SQL_FIND_LANGUAGE_BY_LANGUAGE_CODE);
            prdStatement.setString(1, languageCode);
            resultSet = prdStatement.executeQuery();

            if (resultSet.next()) {
                language = new Language();
                language.setId(resultSet.getLong(ID));
                language.setName(resultSet.getString(NAME));
                language.setCode(resultSet.getString(CODE));
            }

        } catch (SQLException e) {
            logger.error("Error while connecting to table language", e);
            throw new DBException("Failed to connect table language", e);
        } finally {
            close(resultSet);
            close(prdStatement);
        }
        return language;
    }

    @Override
    public boolean delete(long id) throws DBException {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean delete(Language entity) throws DBException {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean create(Language entity) throws DBException {
        throw new UnsupportedOperationException();
    }

    @Override
    public Language update(Language entity) throws DBException {
        throw new UnsupportedOperationException();
    }
}
