package org.finaltask.dao.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.finaltask.dao.AbstractDao;
import org.finaltask.dao.DBException;
import org.finaltask.entity.City;
import org.finaltask.entity.Route;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class RouteDaoImpl extends AbstractDao<Route> {

    private static final Logger logger = LogManager.getLogger(RouteDaoImpl.class);

    static final String ID = "id";
    static final String CITY_FROM = "city_from";
    static final String CITY_TO = "city_to";
    static final String DISTANCE = "distance";
    static final String TARIFF = "tariff";
    static final String CITY_ID_FROM = "city_id_from";
    static final String CITY_ID_TO = "city_id_to";
    static final String SQL_ADD_ROUTE =
            "INSERT `route` (`city_id_from`, `city_id_to`, `distance`, `tariff`) " +
                    "VALUE(?, ?, ?, ?)";
    static final String SQL_FIND_ROUTS_WITH_CITY_NAMES =
            "SELECT route.id, route.city_id_from, route.city_id_to, route.distance, route.tariff, " +
                    "city_from.translation AS city_from, city_to.translation AS city_to " +
                    " FROM route " +
                    "JOIN `city_translator` AS city_from ON `route`.`city_id_from` = city_from.`city_id` AND city_from.`language_id` = ? " +
                    "JOIN `city_translator` AS city_to ON `route`.`city_id_to` = city_to.`city_id` AND city_to.`language_id` = ? " +
                    "%s " +
                    "ORDER BY %s %s " +
                    "LIMIT ? OFFSET ?";
    static final String SQL_FIND_ROUTE_BY_ID = "SELECT * FROM `route` WHERE `id` = ?";
    static final String SQL_FIND_NUMBER_OF_ROUTES = "SELECT COUNT(id) FROM `route` %s";
    static final String SQL_FIND_NUMBER_OF_ROUTES_BY_CITY_FROM_ID_CITY_TO_ID = "SELECT COUNT(id) FROM `route` WHERE `city_id_from` = ? AND `city_id_to` = ?";

    public RouteDaoImpl(Connection connection) {
        super(connection);
    }

    @Override
    public List<Route> findAll() throws DBException {
        throw new UnsupportedOperationException();
    }

    public List<Route> findAllWithCityNames(
            long languageId,
            String orderBy,
            String sortOrder,
            String where,
            long cityId,
            int limit,
            int offset
    ) throws DBException {
        PreparedStatement prdStatement = null;
        ResultSet resultSet = null;
        List<Route> resultRoutes = new ArrayList<>();
        String whereSQL = where.isEmpty() ? "" : "WHERE " + where + " = ?";

        try {
            prdStatement = connection
                    .prepareStatement(String
                            .format(SQL_FIND_ROUTS_WITH_CITY_NAMES, whereSQL, orderBy, sortOrder));
            int k = 1;
            prdStatement.setLong(k++, languageId);
            prdStatement.setLong(k++, languageId);

            if (!whereSQL.isEmpty()) {
                prdStatement.setLong(k++, cityId);
            }

            prdStatement.setInt(k++, limit);
            prdStatement.setInt(k, offset);
            resultSet = prdStatement.executeQuery();

            while (resultSet.next()) {
                Route route = new Route();

                route.setId(resultSet.getLong(ID));
                route.setCityFrom(new City(resultSet.getLong(CITY_ID_FROM), resultSet.getString(CITY_FROM)));
                route.setCityTo(new City(resultSet.getLong(CITY_ID_TO), resultSet.getString(CITY_TO)));
                route.setDistance(resultSet.getInt(DISTANCE));
                route.setTariff(resultSet.getDouble(TARIFF));

                resultRoutes.add(route);
            }

        } catch (SQLException e) {
            logger.error("Error while connecting to table route", e);
            throw new DBException("Failed to connect table route", e);
        } finally {
            close(resultSet);
            close(prdStatement);
        }
        return resultRoutes;
    }

    @Override
    public Route findEntityById(long id) throws DBException {
        PreparedStatement prdStatement = null;
        ResultSet resultSet = null;
        Route route = new Route();

        try {
            prdStatement = connection.prepareStatement(SQL_FIND_ROUTE_BY_ID);
            prdStatement.setLong(1, id);
            resultSet = prdStatement.executeQuery();

            while (resultSet.next()) {
                route.setId(resultSet.getLong(ID));
                route.setCityFrom(new City(resultSet.getLong(CITY_ID_FROM)));
                route.setCityTo(new City(resultSet.getLong(CITY_ID_TO)));
                route.setDistance(resultSet.getInt(DISTANCE));
                route.setTariff(resultSet.getDouble(TARIFF));
            }

        } catch (SQLException e) {
            logger.error("Error while connecting to table route", e);
            throw new DBException("Failed to connect table route", e);
        } finally {
            close(resultSet);
            close(prdStatement);
        }
        return route;
    }



    @Override
    public boolean delete(long id) throws DBException {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean delete(Route entity) throws DBException {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean create(Route route) throws DBException {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        boolean isCreated = false;

        try {
            preparedStatement = connection.prepareStatement(SQL_ADD_ROUTE, Statement.RETURN_GENERATED_KEYS);
            int i = 1;
            preparedStatement.setLong(i++, route.getCityFrom().getId());
            preparedStatement.setLong(i++, route.getCityTo().getId());
            preparedStatement.setInt(i++, route.getDistance());
            preparedStatement.setDouble(i, route.getTariff());

            if (preparedStatement.executeUpdate() > 0) {
                resultSet = preparedStatement.getGeneratedKeys();
                if (resultSet.next()) {
                    route.setId(resultSet.getLong(1));
                    isCreated = true;
                }
            }
        } catch (SQLException e) {
            logger.error("Error while connecting to table route", e);
            throw new DBException("Failed to connect table route", e);
        } finally {
            close(resultSet);
            close(preparedStatement);
        }
        return isCreated;
    }

    @Override
    public Route update(Route entity) throws DBException {
        throw new UnsupportedOperationException();
    }

    public int getNumberOfRows(String where, long cityId) throws DBException {
        PreparedStatement prdStatement = null;
        ResultSet resultSet = null;
        int numberOfRows = 0;
        String whereSQL = where.isEmpty() ? "" : "WHERE " + where + " = ?";

        try {
            prdStatement = connection.prepareStatement(String.format(SQL_FIND_NUMBER_OF_ROUTES, whereSQL));
            if (!whereSQL.isEmpty()) {
                prdStatement.setLong(1, cityId);
            }
            resultSet = prdStatement.executeQuery();

            if (resultSet.next()) {
                numberOfRows = resultSet.getInt(1);
            }

        } catch (SQLException e) {
            logger.error("Error while connecting to table route", e);
            throw new DBException("Failed to connect table route", e);
        } finally {
            close(resultSet);
            close(prdStatement);
        }
        return numberOfRows;
    }

    public int getNumberOfRowsByCityFromIdCityToId(long cityFromId, long cityToId) throws DBException {
        PreparedStatement prdStatement = null;
        ResultSet resultSet = null;
        int numberOfRows = 0;

        try {
            prdStatement = connection.prepareStatement(SQL_FIND_NUMBER_OF_ROUTES_BY_CITY_FROM_ID_CITY_TO_ID);
            prdStatement.setLong(1, cityFromId);
            prdStatement.setLong(2, cityToId);
            resultSet = prdStatement.executeQuery();

            if (resultSet.next()) {
                numberOfRows = resultSet.getInt(1);
            }

        } catch (SQLException e) {
            logger.error("Error while connecting to table route", e);
            throw new DBException("Failed to connect table route", e);
        } finally {
            close(resultSet);
            close(prdStatement);
        }
        return numberOfRows;
    }
}
