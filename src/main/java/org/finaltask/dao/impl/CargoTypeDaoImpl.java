package org.finaltask.dao.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.finaltask.dao.AbstractDao;
import org.finaltask.dao.DBException;
import org.finaltask.entity.CargoType;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CargoTypeDaoImpl extends AbstractDao<CargoType> {
    private static final Logger logger = LogManager.getLogger(CargoTypeDaoImpl.class);
    static final String ID = "id";
    static final String NAME = "name";
    static final String RATIO = "ratio";
    static final String TRANSLATION = "translation";
    static final String SQL_FIND_CARGO_TYPE_BY_NAME = "SELECT * FROM `cargo_type` WHERE `name` = ?";
    static final String SQL_FIND_CARGO_TYPE_BY_ID = "SELECT * FROM `cargo_type` WHERE `id` = ?";
    static final String SQL_FIND_CARGO_TYPE_BY_ID_WITH_TRANSLATION =
            "SELECT * FROM `cargo_type` " +
                    "JOIN `cargo_type_translator` ON `cargo_type`.`id` = `cargo_type_translator`.`cargo_type_id` " +
                    " WHERE `cargo_type`.`id` = ? AND `cargo_type_translator`.`language_id` = ?";
    static final String SQL_FIND_ALL_CARGO_TYPE = "SELECT * FROM `cargo_type`";
    static final String SQL_FIND_ALL_CARGO_TYPE_WITH_TRANSLATION =
            "SELECT * FROM `cargo_type` " +
                    "LEFT JOIN `cargo_type_translator` ON `cargo_type`.`id` = `cargo_type_translator`.`cargo_type_id` " +
                    "WHERE  `cargo_type_translator`.`language_id` = ?";

    public CargoTypeDaoImpl(Connection connection) {
        super(connection);
    }

    @Override
    public List<CargoType> findAll() throws DBException {
        Statement statement = null;
        ResultSet resultSet = null;
        List<CargoType> resultCargoTypes = new ArrayList<>();

        try {
            statement = connection.createStatement();
            resultSet = statement.executeQuery(SQL_FIND_ALL_CARGO_TYPE);

            while (resultSet.next()) {
                CargoType cargoType = new CargoType();

                cargoType.setId(resultSet.getLong(ID));
                cargoType.setName(resultSet.getString(NAME));
                cargoType.setRatio(resultSet.getDouble(RATIO));

                resultCargoTypes.add(cargoType);
            }

        } catch (SQLException e) {
            logger.error("Error while connecting to table cargo type", e);
            throw new DBException("Failed to connect table cargo type", e);
        } finally {
            close(resultSet);
            close(statement);
        }
        return resultCargoTypes;
    }

    public List<CargoType> findAllWithTranslatedNames(long languageId) throws DBException {
        PreparedStatement prdStatement = null;
        ResultSet resultSet = null;
        List<CargoType> resultCargoTypes = new ArrayList<>();

        try {
            prdStatement = connection.prepareStatement(SQL_FIND_ALL_CARGO_TYPE_WITH_TRANSLATION);
            prdStatement.setLong(1, languageId);
            resultSet = prdStatement.executeQuery();

            while (resultSet.next()) {
                CargoType cargoType = new CargoType();

                cargoType.setId(resultSet.getLong(ID));
                cargoType.setName(resultSet.getString(TRANSLATION));
                cargoType.setRatio(resultSet.getDouble(RATIO));

                resultCargoTypes.add(cargoType);
            }

        } catch (SQLException e) {
            logger.error("Error while connecting to table cargo_type", e);
            throw new DBException("Failed to connect table cargo_type", e);
        } finally {
            close(resultSet);
            close(prdStatement);
        }
        return resultCargoTypes;
    }

    @Override
    public CargoType findEntityById(long id) throws DBException {
        PreparedStatement prdStatement = null;
        ResultSet resultSet = null;
        CargoType cargoType = null;

        try {
            prdStatement = connection.prepareStatement(SQL_FIND_CARGO_TYPE_BY_ID);
            prdStatement.setLong(1, id);
            resultSet = prdStatement.executeQuery();

            if (resultSet.next()) {
                cargoType = new CargoType();
                cargoType.setId(resultSet.getLong(ID));
                cargoType.setName(resultSet.getString(NAME));
                cargoType.setRatio(resultSet.getDouble(RATIO));
            }

        } catch (SQLException e) {
            logger.error("Error while connecting to table cargo_type", e);
            throw new DBException("Failed to connect table cargo_type", e);
        } finally {
            close(resultSet);
            close(prdStatement);
        }
        return cargoType;
    }

    public CargoType findEntityByIdWithTranslatedName(long id, long languageId) throws DBException {
        PreparedStatement prdStatement = null;
        ResultSet resultSet = null;
        CargoType cargoType = null;

        try {
            prdStatement = connection.prepareStatement(SQL_FIND_CARGO_TYPE_BY_ID_WITH_TRANSLATION);
            prdStatement.setLong(1, id);
            prdStatement.setLong(2, languageId);
            resultSet = prdStatement.executeQuery();

            if (resultSet.next()) {
                cargoType = new CargoType();
                cargoType.setId(resultSet.getLong(ID));
                cargoType.setName(resultSet.getString(TRANSLATION));
                cargoType.setRatio(resultSet.getDouble(RATIO));
            }

        } catch (SQLException e) {
            logger.error("Error while connecting to table cargo_type", e);
            throw new DBException("Failed to connect table cargo_type", e);
        } finally {
            close(resultSet);
            close(prdStatement);
        }
        return cargoType;
    }
    @Override
    public boolean delete(long id) throws DBException {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean delete(CargoType entity) throws DBException {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean create(CargoType entity) throws DBException {
        throw new UnsupportedOperationException();
    }

    @Override
    public CargoType update(CargoType entity) throws DBException {
        throw new UnsupportedOperationException();
    }
}
