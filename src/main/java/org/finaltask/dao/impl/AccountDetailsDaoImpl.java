package org.finaltask.dao.impl;

import org.finaltask.dao.AbstractDao;
import org.finaltask.dao.DBException;
import org.finaltask.entity.Account;
import org.finaltask.entity.AccountDetails;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.finaltask.entity.Role;
import org.finaltask.entity.Wallet;

import java.sql.*;
import java.util.List;

public class AccountDetailsDaoImpl extends AbstractDao<AccountDetails> {
	
	private static final Logger logger = LogManager.getLogger(AccountDetailsDaoImpl.class);
    static final String ID = "id";
    public static final String NAME = "name";
    public static final String SURNAME = "surname";
    public static final String EMAIL = "email";
    public static final String PHONE = "phone";
    public static final String CITY = "city";

    static final String SQL_ADD_ACCOUNT_DETAILS =
            "INSERT `account_details`(`name`, `surname`, `email`, `phone`, `city`) VALUES (?, ?, ?, ?, ?)";
    static final String SQL_FIND_ACCOUNT_DETAILS_BY_ID =
            "SELECT * FROM `account_details` WHERE `id` = ?";
    static final String SQL_UPDATE_ACCOUNT_DETAILS_FIELD = "UPDATE `account_details` SET %s = ? WHERE `id` = ?";

    public AccountDetailsDaoImpl(Connection connection) {
        super(connection);
    }

    @Override
    public List<AccountDetails> findAll() throws DBException {
        throw new UnsupportedOperationException();
    }

    @Override
    public AccountDetails findEntityById(long id) throws DBException {
        PreparedStatement prdStatement = null;
        ResultSet resultSet = null;
        AccountDetails accountDetails = new AccountDetails();

        try {
            prdStatement = connection.prepareStatement(SQL_FIND_ACCOUNT_DETAILS_BY_ID);
            prdStatement.setLong(1, id);
            resultSet = prdStatement.executeQuery();

            while (resultSet.next()) {
                accountDetails.setId(resultSet.getLong(ID));
                accountDetails.setName(resultSet.getString(NAME));
                accountDetails.setSurname(resultSet.getString(SURNAME));
                accountDetails.setEmail(resultSet.getString(EMAIL));
                accountDetails.setPhone(resultSet.getString(PHONE));
                accountDetails.setCity(resultSet.getString(CITY));
            }
        } catch (SQLException e) {
            logger.error("Error while connect to table account_details", e);
            throw new DBException("Failed to connect table account_details", e);
        } finally {
            close(resultSet);
            close(prdStatement);
        }
        return accountDetails;
    }

    @Override
    public boolean delete(long id) throws DBException {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean delete(AccountDetails accountDetails) throws DBException {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean create(AccountDetails accountDetails) throws DBException {
        PreparedStatement prdStatement = null;
        ResultSet resultSet = null;
        boolean result = false;

        try {
            prdStatement =
                    connection.prepareStatement(SQL_ADD_ACCOUNT_DETAILS, Statement.RETURN_GENERATED_KEYS);

            int i = 1;
            prdStatement.setString(i++, accountDetails.getName());
            prdStatement.setString(i++, accountDetails.getSurname());
            prdStatement.setString(i++, accountDetails.getEmail());
            prdStatement.setString(i++, accountDetails.getPhone());
            prdStatement.setString(i, accountDetails.getCity());
            if (prdStatement.executeUpdate() > 0) {
                resultSet = prdStatement.getGeneratedKeys();
                if (resultSet.next()) {
                    accountDetails.setId(resultSet.getLong(1));
                }
                result = true;
            }

        } catch(SQLException e) {
        	logger.error("Error while connecting to tabke zccountDetails", e);
            throw new DBException("Failed to connect table accountDetails", e);
        } finally {
            close(resultSet);
            close(prdStatement);
        }

        return result;
    }

    @Override
    public AccountDetails update(AccountDetails accountDetails) throws DBException {
        throw new UnsupportedOperationException();
    }
    
    public void updateField(long id, String key, String value) throws DBException {
    	PreparedStatement prdStatement = null;

        try {
            prdStatement = connection.prepareStatement(String
                    .format(SQL_UPDATE_ACCOUNT_DETAILS_FIELD, key));
            int i = 1;
            prdStatement.setString(i++, value);
            prdStatement.setLong(i, id);
            prdStatement.executeUpdate();
        } catch (SQLException e) {
        	logger.error("Error while connecting to table account details", e);
            throw new DBException("Failed to connect table account details", e);
        } finally {
            close(prdStatement);
        }
    }
}
