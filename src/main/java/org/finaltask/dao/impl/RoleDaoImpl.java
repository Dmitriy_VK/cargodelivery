package org.finaltask.dao.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.finaltask.dao.AbstractDao;
import org.finaltask.dao.DBException;
import org.finaltask.entity.Role;
import org.finaltask.entity.RoleEnum;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class RoleDaoImpl extends AbstractDao<Role> {

    private static final Logger logger = LogManager.getLogger(RoleDaoImpl.class);

    static final String ID = "id";
    public static final String NAME = "name";
    static final String SQL_FIND_ROLE_BY_ID = "SELECT * FROM `role` WHERE `id` = ?";

    public RoleDaoImpl(Connection connection) {
        super(connection);
    }

    @Override
    public List<Role> findAll() throws DBException {
        throw new UnsupportedOperationException();
    }

    @Override
    public Role findEntityById(long id) throws DBException {
        PreparedStatement prdStatement = null;
        ResultSet resultSet = null;
        Role role = new Role();

        try {
            prdStatement = connection.prepareStatement(SQL_FIND_ROLE_BY_ID);
            prdStatement.setLong(1, id);
            resultSet = prdStatement.executeQuery();

            while (resultSet.next()) {
                role.setId(resultSet.getLong(ID));
                role.setName(RoleEnum.valueOf(resultSet.getString(NAME).toUpperCase()));
            }

        } catch (SQLException e) {
            logger.error("Error while connecting to table role", e);
            throw new DBException("Failed to connect table role", e);
        } finally {
            close(resultSet);
            close(prdStatement);
        }
        return role;
    }

    @Override
    public boolean delete(long id) throws DBException {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean delete(Role entity) throws DBException {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean create(Role entity) throws DBException {
        throw new UnsupportedOperationException();
    }

    @Override
    public Role update(Role entity) throws DBException {
        throw new UnsupportedOperationException();
    }
}
