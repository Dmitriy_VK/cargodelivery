package org.finaltask.dao.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.finaltask.dao.AbstractDao;
import org.finaltask.dao.DBException;
import org.finaltask.entity.*;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class OrderDaoImpl extends AbstractDao<Order> {

    private static final Logger logger = LogManager.getLogger(OrderDaoImpl.class);

    static final String ID = "id";
    static final String ACCOUNT_ID = "account_id";
    static final String LOGIN = "login";
    static final String ADDRESS = "address";
    static final String CARGO_TYPE_ID = "cargo_type_id";
    private static final String CARGO_TYPE_NAME = "cargo_type";
    static final String WEIGHT = "weight";
    static final String VOLUME = "volume";
    static final String STATUS = "status";
    static final String PRICE = "price";
    static final String DELIVERY_DATE = "delivery_date";
    static final String ROUTE_ID = "route_id";
    static final String CREATE_TIME = "create_time";
    static final String NAME = "name";
    static final String SURNAME = "surname";
    static final String CITY_ID_FROM = "city_id_from";
    static final String CITY_ID_TO = "city_id_to";
    static final String CITY_FROM = "city_from";
    static final String CITY_TO = "city_to";
    static final String DISTANCE = "distance";
    static final String SQL_ADD_ORDER =
            "INSERT `order` (`account_id`, `address`, `cargo_type_id`, `weight`, `volume`, " +
                    "`delivery_date`, `status`, `price`, `route_id`) " +
                    "VALUE(?, ?, ?, ?, ?, ?, ?, ?, ?)";
    static final String SQL_UPDATE_ORDER_STATUS = "UPDATE `order` SET `status` = ? WHERE id = ?";
    static final String SQL_FIND_ORDER_BY_ID = "SELECT * FROM `order` WHERE `id` = ?";
    static final String SQL_FIND_ORDERS_WITH_USER_AND_CITY_NAMES =
            "SELECT `order`.`id`, `order`.`account_id`, `order`.`address`, `order`.`cargo_type_id`, `order`.`create_time`, " +
                    "`order`.`weight`, `order`.`volume`, `order`.`status`, `order`.`price`, `order`.`delivery_date`, `order`.`route_id`, " +
                    "`cargo_type_translator`.`translation` AS cargo_type, " +
                    "`account`.`login`, " +
                    "`account_details`.`name`, `account_details`.`surname`, `account_details`.`email`, `account_details`.`phone`, " +
                    "`route`.`city_id_from`, `route`.`city_id_to`, `route`.`distance`, " +
                    "city_from.translation AS city_from, city_to.translation AS city_to " +
                    "FROM `order` " +
                    "INNER JOIN `cargo_type_translator` ON `order`.`cargo_type_id` = `cargo_type_translator`.`cargo_type_id` AND `cargo_type_translator`.`language_id` = ? " +
                    "INNER JOIN `account` ON `order`.`account_id`= `account`.`id`" +
                    "INNER JOIN `account_details` ON `account`.`account_details_id` = `account_details`.`id`" +
                    "INNER JOIN `route` ON `order`.`route_id` = `route`.`id` " +
                    "INNER JOIN `city_translator` AS city_from ON `route`.`city_id_from` = city_from.`city_id` AND city_from.`language_id` = ? " +
                    "INNER JOIN `city_translator` AS city_to ON `route`.`city_id_to` = city_to.`city_id` AND city_to.`language_id` = ? " +
                    "%s " +
                    "ORDER BY %s %s " +
                    "LIMIT ? OFFSET ?";
    static final String SQL_FIND_ORDERS_BY_CITY_LIKE_WITH_USER_AND_CITY_NAMES =
            "SELECT `order`.`id`, `order`.`account_id`, `order`.`address`, `order`.`cargo_type_id`, " +
                    "`order`.`weight`, `order`.`volume`, `order`.`status`, `order`.`price`, `order`.`delivery_date`, `order`.`route_id`, " +
                    "`cargo_type_translator`.`translation` AS cargo_type, " +
                    "`account`.`login`, " +
                    "`account_details`.`name`, `account_details`.`surname`, `account_details`.`email`, `account_details`.`phone`, " +
                    "`route`.`city_id_from`, `route`.`city_id_to`, `route`.`distance`, " +
                    "city_from.translation AS city_from, city_to.translation AS city_to " +
                    "FROM `order` " +
                    "INNER JOIN `cargo_type_translator` ON `order`.`cargo_type_id` = `cargo_type_translator`.`cargo_type_id` AND `cargo_type_translator`.`language_id` = ? " +
                    "INNER JOIN `account` ON `order`.`account_id`= `account`.`id`" +
                    "INNER JOIN `account_details` ON `account`.`account_details_id` = `account_details`.`id`" +
                    "INNER JOIN `route` ON `order`.`route_id` = `route`.`id` " +
                    "INNER JOIN `city_translator` AS city_from ON `route`.`city_id_from` = city_from.`city_id` AND city_from.`language_id` = ? " +
                    "INNER JOIN `city_translator` AS city_to ON `route`.`city_id_to` = city_to.`city_id` AND city_to.`language_id` = ? " +
                    "WHERE %s.translation LIKE ? " +
                    "ORDER BY %s %s " +
                    "LIMIT ? OFFSET ?";
    static final String SQL_FIND_ORDERS_WITH_CITY_NAMES =
            "SELECT `order`.`id`, `order`.`account_id`, `order`.`address`, `order`.`cargo_type_id`, " +
                    "`order`.`weight`, `order`.`volume`, `order`.`status`, `order`.`price`, `order`.`delivery_date`, `order`.`route_id`, " +
                    "`route`.`city_id_from`, `route`.`city_id_to`, `route`.`distance`, " +
                    "`cargo_type_translator`.`translation` AS cargo_type, " +
                    "city_from.translation AS city_from, city_to.translation AS city_to " +
                    "FROM `order` " +
                    "JOIN `cargo_type_translator` ON `order`.`cargo_type_id` = `cargo_type_translator`.`cargo_type_id` AND `cargo_type_translator`.`language_id` = ? " +
                    "JOIN `route` ON `order`.`route_id` = `route`.`id` " +
                    "JOIN `city_translator` AS city_from ON `route`.`city_id_from` = city_from.`city_id` AND city_from.`language_id` = ? " +
                    "JOIN `city_translator` AS city_to ON `route`.`city_id_to` = city_to.`city_id` AND city_to.`language_id` = ? " +
                    "WHERE `order`.`account_id` = ? %s " +
                    "ORDER BY %s %s " +
                    "LIMIT ? OFFSET ?";
    static final String SQL_FIND_NUMBER_OF_ORDERS_BY_ACCOUNT_ID_AND_STATUS = "SELECT COUNT(id) FROM `order` WHERE `account_Id` = ? %s";
    static final String SQL_FIND_NUMBER_OF_ORDERS_WITH_CONDITION = "SELECT COUNT(id) FROM `order` %s";
    static final String SQL_FIND_NUMBER_OF_ORDERS_BY_CITY_LIKE =
            "SELECT COUNT(`order`.`id`) FROM `order` " +
                    "INNER JOIN `route` ON `order`.`route_id` = `route`.`id` " +
                    "INNER JOIN `city` AS %s ON `route`.%s = %s.id " +
                    "WHERE %s.`name` LIKE ?";


    public OrderDaoImpl(Connection connection) {
        super(connection);
    }

    @Override
    public List<Order> findAll() {
        throw new UnsupportedOperationException();
    }

    @Override
    public Order findEntityById(long id) throws DBException {
        PreparedStatement prdStatement = null;
        ResultSet resultSet = null;
        Order order = new Order();
        order.setAccount(new Account());
        order.getAccount().setAccountDetails(new AccountDetails());
        order.setRoute(new Route());
        order.setCargoType(new CargoType());

        try {
            prdStatement = connection.prepareStatement(SQL_FIND_ORDER_BY_ID);
            prdStatement.setLong(1, id);
            resultSet = prdStatement.executeQuery();

            if (resultSet.next()) {
                order.setId(resultSet.getLong(ID));
                order.getAccount().setId(resultSet.getLong(ACCOUNT_ID));
                order.setAddress(resultSet.getString(ADDRESS));
                order.getCargoType().setId(resultSet.getLong(CARGO_TYPE_ID));
                order.setWeight(resultSet.getDouble(WEIGHT));
                order.setVolume(resultSet.getInt(VOLUME));
                order.setDeliveryDate(resultSet.getDate(DELIVERY_DATE));
                order.setOrderStatus(Enum.valueOf(Status.class, resultSet.getString(STATUS).toUpperCase()));
                order.setPrice(resultSet.getDouble(PRICE));
                order.getRoute().setId(resultSet.getLong(ROUTE_ID));
                order.setCreateTime(resultSet.getTimestamp(CREATE_TIME).toLocalDateTime());
            }

        } catch (SQLException e) {
            logger.error("Error while connecting to table order", e);
            throw new DBException("Failed to connect table order", e);
        } finally {
            close(resultSet);
            close(prdStatement);
        }
        return order;
    }

    @Override
    public boolean delete(long id) throws DBException {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean delete(Order entity) throws DBException {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean create(Order order) throws DBException {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        boolean isCreated = false;

        try {
            preparedStatement = connection.prepareStatement(SQL_ADD_ORDER, Statement.RETURN_GENERATED_KEYS);
            int i = 1;
            preparedStatement.setLong(i++, order.getAccount().getId());
            preparedStatement.setString(i++, order.getAddress());
            preparedStatement.setLong(i++, order.getCargoType().getId());
            preparedStatement.setDouble(i++, order.getWeight());
            preparedStatement.setInt(i++, order.getVolume());
            preparedStatement.setDate(i++, order.getDeliveryDate());
            preparedStatement.setString(i++, order.getOrderStatus().name());
            preparedStatement.setDouble(i++, order.getPrice());
            preparedStatement.setLong(i, order.getRoute().getId());

            if (preparedStatement.executeUpdate() > 0) {
                resultSet = preparedStatement.getGeneratedKeys();
                if (resultSet.next()) {
                    order.setId(resultSet.getLong(1));
                    isCreated = true;
                }
            }
        } catch (SQLException e) {
            logger.error("Error while connecting to table order", e);
            throw new DBException("Failed to connect table order", e);
        } finally {
            close(resultSet);
            close(preparedStatement);
        }
        return isCreated;
    }

    @Override
    public Order update(Order entity) throws DBException {
        throw new UnsupportedOperationException();
    }

    public List<Order> findAllWithAccountDetailsAndCityNames(
            long languageId,
            String orderBy,
            String sortOrder,
            String whereName,
            String whereValue,
            int limit,
            int offset
    ) throws DBException {
        PreparedStatement prdStatement = null;
        ResultSet resultSet = null;
        List<Order> resultOrders = new ArrayList<>();
        String whereSQL = whereName.isEmpty() ? "" : "WHERE " + whereName + " = ?";

        try {
            prdStatement = connection.prepareStatement(String
                    .format(SQL_FIND_ORDERS_WITH_USER_AND_CITY_NAMES, whereSQL, orderBy, sortOrder));
            int k = 1;
            prdStatement.setLong(k++, languageId);
            prdStatement.setLong(k++, languageId);
            prdStatement.setLong(k++, languageId);
            if (!whereSQL.isEmpty()) {
                prdStatement.setString(k++, whereValue);
            }
            prdStatement.setInt(k++, limit);
            prdStatement.setInt(k, offset);
            resultSet = prdStatement.executeQuery();

            while (resultSet.next()) {
                Order order = new Order();
                order.setAccount(new Account());
                order.getAccount().setAccountDetails(new AccountDetails());
                order.setRoute(new Route());
                order.setCargoType(new CargoType());

                order.setId(resultSet.getLong(ID));
                order.getAccount().setId(resultSet.getLong(ACCOUNT_ID));
                order.getAccount().setLogin(resultSet.getString(LOGIN));
                order.setAddress(resultSet.getString(ADDRESS));
                order.getCargoType().setId(resultSet.getLong(CARGO_TYPE_ID));
                order.getCargoType().setName(resultSet.getString(CARGO_TYPE_NAME));
                order.setWeight(resultSet.getDouble(WEIGHT));
                order.setVolume(resultSet.getInt(VOLUME));
                order.setOrderStatus(Enum.valueOf(Status.class, resultSet.getString(STATUS).toUpperCase()));
                order.setPrice(resultSet.getDouble(PRICE));
                order.setDeliveryDate(resultSet.getDate(DELIVERY_DATE));
                order.setCreateTime(resultSet.getTimestamp(CREATE_TIME).toLocalDateTime());
                order.getRoute().setId(resultSet.getLong(ROUTE_ID));
                order.getAccount().getAccountDetails().setName(resultSet.getString(NAME));
                order.getAccount().getAccountDetails().setSurname((resultSet.getString(SURNAME)));
                order.getRoute().setCityFrom(new City(resultSet.getLong(CITY_ID_FROM), resultSet.getString(CITY_FROM)));
                order.getRoute().setCityTo(new City(resultSet.getLong(CITY_ID_TO), resultSet.getString(CITY_TO)));
                order.getRoute().setDistance(resultSet.getInt(DISTANCE));

                resultOrders.add(order);
            }

        } catch (SQLException e) {
            logger.error("Error while connecting to table order", e);
            throw new DBException("Failed to connect table order", e);
        } finally {
            close(resultSet);
            close(prdStatement);
        }
        return resultOrders;
    }

    public List<Order> findAllByCityWithAccountDetailsAndCityNames(
            long languageId,
            String orderBy,
            String sortOrder,
            String whereName,
            String whereValue,
            int limit,
            int offset
    ) throws DBException {
        PreparedStatement prdStatement = null;
        ResultSet resultSet = null;
        List<Order> resultOrders = new ArrayList<>();


        try {
            prdStatement = connection.prepareStatement(String
                    .format(SQL_FIND_ORDERS_BY_CITY_LIKE_WITH_USER_AND_CITY_NAMES, whereName, orderBy, sortOrder));
            int k = 1;
            prdStatement.setLong(k++, languageId);
            prdStatement.setLong(k++, languageId);
            prdStatement.setLong(k++, languageId);
            prdStatement.setString(k++, "%" + whereValue + "%");
            prdStatement.setInt(k++, limit);
            prdStatement.setInt(k, offset);
            resultSet = prdStatement.executeQuery();

            while (resultSet.next()) {
                Order order = new Order();
                order.setAccount(new Account());
                order.getAccount().setAccountDetails(new AccountDetails());
                order.setRoute(new Route());
                order.setCargoType(new CargoType());

                order.setId(resultSet.getLong(ID));
                order.getAccount().setId(resultSet.getLong(ACCOUNT_ID));
                order.getAccount().setLogin(resultSet.getString(LOGIN));
                order.setAddress(resultSet.getString(ADDRESS));
                order.getCargoType().setId(resultSet.getLong(CARGO_TYPE_ID));
                order.getCargoType().setName(resultSet.getString(CARGO_TYPE_NAME));
                order.setWeight(resultSet.getDouble(WEIGHT));
                order.setVolume(resultSet.getInt(VOLUME));
                order.setOrderStatus(Enum.valueOf(Status.class, resultSet.getString(STATUS).toUpperCase()));
                order.setPrice(resultSet.getDouble(PRICE));
                order.setDeliveryDate(resultSet.getDate(DELIVERY_DATE));
                order.getRoute().setId(resultSet.getLong(ROUTE_ID));
                order.getAccount().getAccountDetails().setName(resultSet.getString(NAME));
                order.getAccount().getAccountDetails().setSurname((resultSet.getString(SURNAME)));
                order.getRoute().setCityFrom(new City(resultSet.getString(CITY_FROM)));
                order.getRoute().setCityTo(new City(resultSet.getString(CITY_TO)));
                order.getRoute().setDistance(resultSet.getInt(DISTANCE));
                order.setCreateTime(resultSet.getTimestamp(CREATE_TIME).toLocalDateTime());

                resultOrders.add(order);
            }

        } catch (SQLException e) {
            logger.error("Error while connecting to table order", e);
            throw new DBException("Failed to connect table order", e);
        } finally {
            close(resultSet);
            close(prdStatement);
        }
        return resultOrders;
    }

    public List<Order> findAllWithCityNamesByAccountId(
            long languageId,
            long accountId,
            String orderBy,
            String sortOrder,
            String status,
            int limit,
            int offset
    ) throws DBException {
        PreparedStatement prdStatement = null;
        ResultSet resultSet = null;
        List<Order> resultOrders = new ArrayList<>();
        String andSQL = status.isEmpty() ? "" : " AND `order`.`status` = ?";

        try {
            prdStatement = connection.prepareStatement(String
                    .format(SQL_FIND_ORDERS_WITH_CITY_NAMES, andSQL, orderBy, sortOrder));

            int k = 1;
            prdStatement.setLong(k++, languageId);
            prdStatement.setLong(k++, languageId);
            prdStatement.setLong(k++, languageId);
            prdStatement.setLong(k++, accountId);
            if (!andSQL.isEmpty()) {
                prdStatement.setString(k++, Status.valueOf(status).name());
            }
            prdStatement.setInt(k++, limit);
            prdStatement.setInt(k, offset);
            resultSet = prdStatement.executeQuery();

            while (resultSet.next()) {
                Order order = new Order();
                order.setAccount(new Account());
                order.getAccount().setAccountDetails(new AccountDetails());
                order.setRoute(new Route());
                order.setCargoType(new CargoType());

                order.setId(resultSet.getLong(ID));
                order.getAccount().setId(resultSet.getLong(ACCOUNT_ID));
                order.setAddress(resultSet.getString(ADDRESS));
                order.getCargoType().setId(resultSet.getLong(CARGO_TYPE_ID));
                order.getCargoType().setName(resultSet.getString(CARGO_TYPE_NAME));
                order.setWeight(resultSet.getDouble(WEIGHT));
                order.setVolume(resultSet.getInt(VOLUME));
                order.setOrderStatus(Enum.valueOf(Status.class, resultSet.getString(STATUS).toUpperCase()));
                order.setPrice(resultSet.getDouble(PRICE));
                order.setDeliveryDate(resultSet.getDate(DELIVERY_DATE));
                order.getRoute().setId(resultSet.getLong(ROUTE_ID));
                order.getRoute().setCityFrom(new City(resultSet.getLong(CITY_ID_FROM), resultSet.getString(CITY_FROM)));
                order.getRoute().setCityTo(new City(resultSet.getLong(CITY_ID_TO), resultSet.getString(CITY_TO)));
                order.getRoute().setDistance(resultSet.getInt(DISTANCE));
                order.setCreateTime(resultSet.getTimestamp(CREATE_TIME).toLocalDateTime());

                resultOrders.add(order);
            }

        } catch (SQLException e) {
            logger.error("Error while connecting to table order", e);
            throw new DBException("Failed to connect table order", e);
        } finally {
            close(resultSet);
            close(prdStatement);
        }
        return resultOrders;
    }

    public Order updateStatus(Order order) throws DBException {
        PreparedStatement preparedStatement = null;

        try {
            preparedStatement = connection.prepareStatement(SQL_UPDATE_ORDER_STATUS);
            int i = 1;
            preparedStatement.setString(i++, order.getOrderStatus().name());
            preparedStatement.setLong(i, order.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            logger.error("Error while connecting to table order", e);
            throw new DBException("Failed to connect table order", e);
        } finally {
            close(preparedStatement);
        }
        return order;
    }

    public int getNumberOfRowsByAccountIdAndStatus(long id, String status) throws DBException {
        PreparedStatement prdStatement = null;
        ResultSet resultSet = null;
        int numberOfRows = 0;
        String andSQL = status.isEmpty() ? "" : " AND `status` = ?";

        try {
            prdStatement = connection.prepareStatement(String.format(SQL_FIND_NUMBER_OF_ORDERS_BY_ACCOUNT_ID_AND_STATUS, andSQL));
            prdStatement.setLong(1, id);
            if (!andSQL.isEmpty()) {
                prdStatement.setString(2, Status.valueOf(status).name());
            }
            resultSet = prdStatement.executeQuery();

            if (resultSet.next()) {
                numberOfRows = resultSet.getInt(1);
            }

        } catch (SQLException e) {
            logger.error("Error while connecting to table order", e);
            throw new DBException("Failed to connect table order", e);
        } finally {
            close(resultSet);
            close(prdStatement);
        }
        return numberOfRows;
    }

    public int getNumberOfRowsWithCondition(String whereName, String whereValue) throws DBException {
        PreparedStatement prdStatement = null;
        ResultSet resultSet = null;
        int numberOfRows = 0;
        String whereSQL = whereName.isEmpty() ? "" : "WHERE " + whereName + " = ?";

        try {
            prdStatement = connection.prepareStatement(String.format(SQL_FIND_NUMBER_OF_ORDERS_WITH_CONDITION, whereSQL));

            if (!whereSQL.isEmpty()) {
                prdStatement.setString(1, whereValue);
            }
            resultSet = prdStatement.executeQuery();

            if (resultSet.next()) {
                numberOfRows = resultSet.getInt(1);
            }

        } catch (SQLException e) {
            logger.error("Error while connecting to table order", e);
            throw new DBException("Failed to connect table order", e);
        } finally {
            close(resultSet);
            close(prdStatement);
        }
        return numberOfRows;
    }

    public int getNumberOfRowsByCityLike(String whereName, String whereValue) throws DBException {
        PreparedStatement prdStatement = null;
        ResultSet resultSet = null;
        int numberOfRows = 0;

        try {
            prdStatement = connection
                    .prepareStatement(String
                            .format(SQL_FIND_NUMBER_OF_ORDERS_BY_CITY_LIKE,
                                    whereName,
                                    whereName.replace("_", "_id_"),
                                    whereName,
                                    whereName));

            prdStatement.setString(1, "%" + whereValue + "%");

            resultSet = prdStatement.executeQuery();

            if (resultSet.next()) {
                numberOfRows = resultSet.getInt(1);
            }

        } catch (SQLException e) {
            logger.error("Error while connecting to table order", e);
            throw new DBException("Failed to connect table order", e);
        } finally {
            close(resultSet);
            close(prdStatement);
        }
        return numberOfRows;
    }
}
