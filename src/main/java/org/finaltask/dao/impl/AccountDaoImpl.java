package org.finaltask.dao.impl;

import org.finaltask.dao.AbstractDao;
import org.finaltask.dao.DBException;
import org.finaltask.entity.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class AccountDaoImpl extends AbstractDao<Account> {
    private static final Logger logger = LogManager.getLogger(AccountDaoImpl.class);
    static final String ID = "id";
    public static final String LOGIN = "login";
    public static final String PASSWORD = "password";
    static final String ROLE_ID = "role_id";
    static final String WALLET_ID = "wallet_id";
    static final String ROLE_NAME = "role";
    static final String ACCOUNT_DETAILS_ID = "account_details_id";
    static final String SQL_ADD_ACCOUNT =
            "INSERT `account`(`login`, `password`, `role_id`, `wallet_id`, `account_details_id`) VALUES " +
                    "(?, ?, (SELECT `id` FROM `role` WHERE `name`= ?), ?, ?)";
    static final String SQL_FIND_ACCOUNT_BY_ID = "SELECT * FROM `account` WHERE `id` = ?";
    static final String SQL_FIND_ACCOUNT_BY_LOGIN = "SELECT * FROM `account` WHERE `login` = ?";
    static final String SQL_UPDATE_ACCOUNT_FIELD = "UPDATE `account` SET %s = ? WHERE `id` = ?";
    static final String SQL_FIND_ACCOUNT_BY_ROLE =
            "SELECT * FROM `account` WHERE `role_id` = (SELECT id FROM `role` WHERE `name` = ?)";
    static final String SQL_FIND_ACCOUNTS_WITH_ACCOUNT_DETAILS =
            "SELECT `account`.`id`, `account`.`login`, `account`.`role_id`, `account`.`wallet_id`, `account`.`account_details_id`, " +
                    "`role`.`name` as role, " +
                    "`wallet`.`value`, " +
                    "`account_details`.`name`, `account_details`.`surname`, `account_details`.`email`, " +
                    "`account_details`.`phone`, `account_details`.`city` " +
                    "FROM `account` " +
                    "INNER JOIN `account_details` ON `account`.`account_details_id` = `account_details`.`id`" +
                    "INNER JOIN `role` ON `account`.`role_id` = `role`.`id`" +
                    "INNER JOIN `wallet` ON `account`.`wallet_id` = `wallet`.`id`" +
                    "WHERE `role`.`name` = ? " +
                    "ORDER BY %s %s " +
                    "LIMIT ? OFFSET ?";
    static final String SQL_FIND_NUMBER_OF_USERS_BY_ROLE =
            "SELECT COUNT(id) FROM `account` WHERE `role_id` = (SELECT id FROM `role` WHERE `name` = ? )";

    public AccountDaoImpl(Connection connection) {
        super(connection);
    }

    @Override
    public List<Account> findAll() throws DBException {
        throw new UnsupportedOperationException();
    }

    @Override
    public Account findEntityById(long id) throws DBException {
        PreparedStatement prdStatement = null;
        ResultSet resultSet = null;
        Account account = null;

        try {
            prdStatement = connection.prepareStatement(SQL_FIND_ACCOUNT_BY_ID);
            prdStatement.setLong(1, id);
            resultSet = prdStatement.executeQuery();

            if (resultSet.next()) {
                account = new Account();
                account.setRole(new Role());
                account.setWallet(new Wallet());

                account.setAccountDetails(new AccountDetails());
                account.setId(resultSet.getLong(ID));
                account.setLogin(resultSet.getString(LOGIN));
                account.setPassword(resultSet.getString(PASSWORD));
                account.getRole().setId(resultSet.getLong(ROLE_ID));
                account.getWallet().setId(resultSet.getLong(WALLET_ID));
                account.getAccountDetails().setId(resultSet.getLong(ACCOUNT_DETAILS_ID));
            }
        } catch (SQLException e) {
            logger.error("Error while connect to table account", e);
            throw new DBException("Failed to connect table account", e);
        } finally {
            close(resultSet);
            close(prdStatement);
        }
        return account;
    }

    @Override
    public boolean delete(long id) throws DBException {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean delete(Account entity) throws DBException {
        throw new UnsupportedOperationException();
    }

    @Override
    public Account update(Account entity) throws DBException {
        throw new UnsupportedOperationException();
    }

    public void updateField(long id, String key, String value) throws DBException {
        PreparedStatement prdStatement = null;

        try {
            prdStatement = connection.prepareStatement(String.format(SQL_UPDATE_ACCOUNT_FIELD, key));
            int i = 1;
            prdStatement.setString(i++, value);
            prdStatement.setLong(i, id);
            prdStatement.executeUpdate();
        } catch (SQLException e) {
            logger.error("Error while connecting to table account", e);
            throw new DBException("Failed to connect table account", e);
        } finally {
            close(prdStatement);
        }
    }

    @Override
    public boolean create(Account account) throws DBException {

        PreparedStatement prdStatement = null;
        ResultSet resultSet = null;
        boolean result = false;

        try {
            prdStatement = connection.prepareStatement(SQL_ADD_ACCOUNT, Statement.RETURN_GENERATED_KEYS);

            int i = 1;
            prdStatement.setString(i++, account.getLogin());
            prdStatement.setString(i++, account.getPassword());
            prdStatement.setString(i++, account.getRole().getName().toString());
            prdStatement.setLong(i++, account.getWallet().getId());
            prdStatement.setLong(i, account.getAccountDetails().getId());

            if (prdStatement.executeUpdate() > 0) {
                resultSet = prdStatement.getGeneratedKeys();
                if (resultSet.next()) {
                    account.setId(resultSet.getLong(1));
                }
                result = true;
            }

        } catch (SQLException e) {
            logger.error("Error while connect to table account", e);
            throw new DBException("Failed to connect table account");
        } finally {
            close(resultSet);
            close(prdStatement);
        }

        return result;
    }

    public Account findEntityByLogin(String login) throws DBException {
        PreparedStatement prdStatement = null;
        ResultSet resultSet = null;
        Account account = null;

        try {
            prdStatement = connection.prepareStatement(SQL_FIND_ACCOUNT_BY_LOGIN);
            prdStatement.setString(1, login);
            resultSet = prdStatement.executeQuery();

            while (resultSet.next()) {
                account = new Account();
                account.setRole(new Role());
                account.setWallet(new Wallet());
                account.setAccountDetails(new AccountDetails());

                account.setId(resultSet.getLong(ID));
                account.setLogin(resultSet.getString(LOGIN));
                account.setPassword(resultSet.getString(PASSWORD));
                account.getRole().setId(resultSet.getLong(ROLE_ID));
                account.getWallet().setId(resultSet.getLong(WALLET_ID));
                account.getAccountDetails().setId(resultSet.getLong(ACCOUNT_DETAILS_ID));
            }
        } catch (SQLException e) {
            logger.error("Error while connect to table account", e);
            throw new DBException("Failed to connect table account", e);
        } finally {
            close(resultSet);
            close(prdStatement);
        }
        return account;
    }

    public Account findEntityByRole(RoleEnum role) throws DBException {
        PreparedStatement prdStatement = null;
        ResultSet resultSet = null;
        Account account = null;

        try {
            prdStatement = connection.prepareStatement(SQL_FIND_ACCOUNT_BY_ROLE);
            prdStatement.setString(1, role.name());
            resultSet = prdStatement.executeQuery();

            if (resultSet.next()) {
                account = new Account();
                account.setRole(new Role());
                account.setWallet(new Wallet());
                account.setAccountDetails(new AccountDetails());

                account.setId(resultSet.getLong(ID));
                account.setLogin(resultSet.getString(LOGIN));
                account.setPassword(resultSet.getString(PASSWORD));
                account.getRole().setId(resultSet.getLong(ROLE_ID));
                account.getWallet().setId(resultSet.getLong(WALLET_ID));
                account.getAccountDetails().setId(resultSet.getLong(ACCOUNT_DETAILS_ID));
            }
        } catch (SQLException e) {
            logger.error("Error while connect to table account", e);
            throw new DBException("Failed to connect table account", e);
        } finally {
            close(resultSet);
            close(prdStatement);
        }
        return account;
    }

    public List<Account> findAllWithAccountDetails(
            RoleEnum role,
            String orderBy,
            String sortOrder,
            int limit,
            int offset
    ) throws DBException {
        PreparedStatement prdStatement = null;
        ResultSet resultSet = null;
        List<Account> resultAccounts = new ArrayList<>();

        try {
            prdStatement = connection.prepareStatement(String
                    .format(SQL_FIND_ACCOUNTS_WITH_ACCOUNT_DETAILS, orderBy, sortOrder));
            int k = 1;
            prdStatement.setString(k++, role.name());
            prdStatement.setInt(k++, limit);
            prdStatement.setInt(k, offset);
            resultSet = prdStatement.executeQuery();

            while (resultSet.next()) {
                Account account = new Account();
                account.setRole(new Role());
                account.setWallet(new Wallet());
                account.setAccountDetails(new AccountDetails());

                account.setId(resultSet.getLong(ID));
                account.setLogin(resultSet.getString(LOGIN));
                account.getRole().setId(resultSet.getLong(ROLE_ID));
                account.getRole().setName(RoleEnum.valueOf(resultSet.getString(ROLE_NAME).toUpperCase()));
                account.getWallet().setId(resultSet.getLong(WALLET_ID));
                account.getWallet().setValue(resultSet.getDouble(WalletDaoImpl.VALUE));
                account.getAccountDetails().setId(resultSet.getLong(ACCOUNT_DETAILS_ID));
                account.getAccountDetails().setName(resultSet.getString(AccountDetailsDaoImpl.NAME));
                account.getAccountDetails().setSurname(resultSet.getString(AccountDetailsDaoImpl.SURNAME));
                account.getAccountDetails().setEmail(resultSet.getString(AccountDetailsDaoImpl.EMAIL));
                account.getAccountDetails().setPhone(resultSet.getString(AccountDetailsDaoImpl.PHONE));
                account.getAccountDetails().setCity(resultSet.getString(AccountDetailsDaoImpl.CITY));

                resultAccounts.add(account);
            }

        } catch (SQLException e) {
            logger.error("Error while connecting to table account and account details", e);
            throw new DBException("Failed to connect table account and account details", e);
        } finally {
            close(resultSet);
            close(prdStatement);
        }
        return resultAccounts;
    }

    public int getNumberOfRows(RoleEnum role) throws DBException {
        PreparedStatement prdStatement = null;
        ResultSet resultSet = null;
        int numberOfRows = 0;

        try {
            prdStatement = connection.prepareStatement(SQL_FIND_NUMBER_OF_USERS_BY_ROLE);

            prdStatement.setString(1, role.name());

            resultSet = prdStatement.executeQuery();

            if (resultSet.next()) {
                numberOfRows = resultSet.getInt(1);
            }

        } catch (SQLException e) {
            logger.error("Error while connecting to table account", e);
            throw new DBException("Failed to connect table account", e);
        } finally {
            close(resultSet);
            close(prdStatement);
        }
        return numberOfRows;
    }
}
