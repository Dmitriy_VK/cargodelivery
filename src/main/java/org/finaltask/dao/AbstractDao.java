package org.finaltask.dao;

import org.finaltask.connectionpool.ConnectionPool;
import org.finaltask.entity.Entity;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 * The class AbstractDao contains simple basic methods for working with DB
 */
public abstract class AbstractDao<T extends Entity> {
    protected Connection connection;

    public AbstractDao(Connection connection) {
        this.connection = connection;
    }

    public abstract List<T> findAll() throws DBException;
    public abstract T findEntityById(long id) throws DBException;
    public abstract boolean delete(long id) throws DBException;
    public abstract boolean delete(T entity) throws DBException;
    public abstract boolean create(T entity) throws DBException;
    public abstract T update(T entity) throws DBException;

    protected void close(AutoCloseable autoCloseable){
        if (autoCloseable != null) {
            try {
                autoCloseable.close();
            } catch (Exception e) {
                e.printStackTrace();
                throw new IllegalStateException("Cannot close " + autoCloseable);
            }
        }
    }
}