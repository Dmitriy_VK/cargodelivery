package org.finaltask.service;

import org.finaltask.entity.Route;

import java.util.List;

public interface RouteService {
    List<Route> findAllWithCityNames(
            String locale,
            String orderBy,
            String sortOrder,
            String where,
            long cityId,
            int limit,
            int offset
    ) throws ServiceException;
    Route findEntityById(int id) throws ServiceException;
    int getNumberOfRows(String where, long cityId) throws ServiceException;
    boolean isExistEntityWithCityFromIdCityToId(long cityFromId, long cityToId) throws ServiceException;
    boolean create(Route route) throws ServiceException;
}
