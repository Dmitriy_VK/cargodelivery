package org.finaltask.service;

import org.finaltask.entity.AccountDetails;

public interface AccountDetailsService {
    AccountDetails findEntityById(long id) throws ServiceException;
}
