package org.finaltask.service;

import org.finaltask.entity.Account;
import org.finaltask.entity.RoleEnum;

import java.util.List;
import java.util.Map;

public interface AccountService {
    boolean createAccountWithDetailsAndWallet(Account account) throws ServiceException;

    Account findEntityByLogin(String login) throws ServiceException;

    Account findEntityByRole(RoleEnum role) throws ServiceException;

    Account findAccountProfileById(long id) throws ServiceException;

    List<Account> findAllByRoleWithAccountDetails(
            RoleEnum role,
            String orderBy,
            String sortOrder,
            int limit,
            int offset
    ) throws ServiceException;

    void updateFields(Map<String, String> fieldsForUpdate, long id) throws ServiceException;
    int getNumberOfRowsByRole(RoleEnum role) throws ServiceException;
}
