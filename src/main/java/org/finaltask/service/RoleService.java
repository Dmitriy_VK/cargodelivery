package org.finaltask.service;

import org.finaltask.entity.Role;

public interface RoleService {
	Role findEntityById(long id) throws ServiceException;
}
