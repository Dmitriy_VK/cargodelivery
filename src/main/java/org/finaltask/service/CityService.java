package org.finaltask.service;

import org.finaltask.entity.City;

import java.util.List;

public interface CityService {
    int findIdByName(String name) throws ServiceException;
    List<City> findAll() throws ServiceException;
}
