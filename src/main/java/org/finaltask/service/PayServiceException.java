package org.finaltask.service;

public class PayServiceException extends Exception {
    public PayServiceException() {
    }

    public PayServiceException(String message) {
        super(message);
    }
    
    public PayServiceException(String message, Throwable cause) {
        super(message, cause);
    }

}
