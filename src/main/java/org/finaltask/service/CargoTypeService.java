package org.finaltask.service;

import org.finaltask.entity.CargoType;

import java.util.List;

public interface CargoTypeService {
    List<CargoType> findAll() throws ServiceException;
    List<CargoType> findAllWithTranslatedNames(String languageCode) throws ServiceException;
    CargoType findEntityByIdWithTranslatedName(long id, String languageCode) throws ServiceException;
}
