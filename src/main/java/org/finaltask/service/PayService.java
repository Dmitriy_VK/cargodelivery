package org.finaltask.service;

import org.finaltask.entity.Account;
import org.finaltask.entity.Order;

public interface PayService {
    void payForOrder(long orderId) throws ServiceException, PayServiceException;
}
