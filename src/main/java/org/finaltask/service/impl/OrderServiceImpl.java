package org.finaltask.service.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.finaltask.connectionpool.ConnectionPool;
import org.finaltask.dao.DBException;
import org.finaltask.dao.impl.*;
import org.finaltask.entity.Language;
import org.finaltask.entity.Order;
import org.finaltask.entity.Status;
import org.finaltask.service.OrderService;
import org.finaltask.service.ServiceException;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public class OrderServiceImpl implements OrderService {

    private static final Logger logger = LogManager.getLogger(OrderServiceImpl.class);

    @Override
    public boolean create(Order order) throws ServiceException {
        Connection connection = null;

        try {
            connection = ConnectionPool.INSTANCE.getConnection();// add exception
            OrderDaoImpl orderDao = new OrderDaoImpl(connection);
            return orderDao.create(order);
        } catch (DBException e) {
            logger.error("Error while creating order in DB", e);
            throw new ServiceException("Failed to create order", e);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    logger.error("Error while closing connection", e);
                    throw new ServiceException("Failed to close connection", e);
                }
            }
        }
    }

    @Override
    public Order findEntityById(long id) throws ServiceException {
        Connection connection = null;

        try {
            connection = ConnectionPool.INSTANCE.getConnection();// add exception
            OrderDaoImpl orderDao = new OrderDaoImpl(connection);

            return orderDao.findEntityById(id);
        } catch (DBException e) {
            logger.error("Error while finding order in DB", e);
            throw new ServiceException("Failed to read order from database", e);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    logger.error("Error while closing connection", e);
                    throw new ServiceException("Failed to close connection", e);
                }
            }
        }
    }

    @Override
    public Order findEntityByIdWithRouteAndAccount(long id) throws ServiceException {
        Connection connection = null;
        Order order;

        try {
            connection = ConnectionPool.INSTANCE.getConnection();// add exception
            RouteDaoImpl routeDao = new RouteDaoImpl(connection);
            AccountDaoImpl accountDao = new AccountDaoImpl(connection);
            AccountDetailsDaoImpl accountDetailsDao = new AccountDetailsDaoImpl(connection);
            CityDaoImpl cityDao = new CityDaoImpl(connection);

            order = findEntityById(id);
            if (order != null) {
                order.setRoute(routeDao.findEntityById(order.getRoute().getId()));
                order.getRoute().getCityTo().setName(cityDao.findEntityById(order.getRoute().getCityTo().getId()).getName());
                order.getRoute().getCityFrom().setName(cityDao.findEntityById(order.getRoute().getCityFrom().getId()).getName());
                order.setAccount(accountDao.findEntityById(order.getAccount().getId()));
                order.getAccount().setAccountDetails(accountDetailsDao.findEntityById(order.getAccount().getAccountDetails().getId()));
            }
            return order;
        } catch (DBException e) {
            logger.error("Error while finding order in DB", e);
            throw new ServiceException("Failed to read order from database", e);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    logger.error("Error while closing connection", e);
                    throw new ServiceException("Failed to close connection", e);
                }
            }
        }
    }

    @Override
    public void updateStatus(long orderId, Status newStatus) throws ServiceException {
        Connection connection = null;

        try {
            connection = ConnectionPool.INSTANCE.getConnection();// add exception
            connection.setAutoCommit(false);

            OrderDaoImpl orderDao = new OrderDaoImpl(connection);

            Order order = orderDao.findEntityById(orderId);
            order.setOrderStatus(newStatus);
            orderDao.updateStatus(order);

            connection.commit();
        } catch (DBException | SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException ex) {
                logger.error("Error cannot rollback transaction in OrderService", ex);
                throw new ServiceException("failed to update status in order", ex);
            }
            logger.error("Error while updating order's status in DB", e);
            throw new ServiceException("Failed to update order's status", e);
        } finally {
            if (connection != null) {
                try {
                    connection.setAutoCommit(true);
                    connection.close();
                } catch (SQLException e) {
                    logger.error("Error while closing connection", e);
                    throw new ServiceException("Failed to close connection", e);
                }
            }
        }
    }

    @Override
    public List<Order> findAllWithAccountDetailsAndCityNames(
            String locale,
            String orderBy,
            String sortOrder,
            String whereName,
            String whereValue,
            int limit,
            int offset
    ) throws ServiceException {
        Connection connection = null;

        try {
            connection = ConnectionPool.INSTANCE.getConnection();// add exception
            OrderDaoImpl orderDao = new OrderDaoImpl(connection);
            LanguageDaoImpl languageDao = new LanguageDaoImpl(connection);
            Language language = languageDao.findEntityByLanguageCode(locale);

            if(language == null) {
                throw new DBException("Nothing was found in languages");
            }
            return orderDao.findAllWithAccountDetailsAndCityNames(language.getId(), orderBy, sortOrder, whereName, whereValue, limit, offset);
        } catch (DBException e) {
            logger.error("Error while finding order in DB", e);
            throw new ServiceException("Failed to find orders", e);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    logger.error("Error while closing connection", e);
                    throw new ServiceException("Failed to close connection");
                }
            }
        }
    }

    @Override
    public List<Order> findAllWithCityNamesByAccountId(
            String locale,
            long accountId,
            String orderBy,
            String sortOrder,
            String status,
            int limit,
            int offset
    ) throws ServiceException {
        Connection connection = null;

        try {
            connection = ConnectionPool.INSTANCE.getConnection();// add exception
            OrderDaoImpl orderDao = new OrderDaoImpl(connection);
            LanguageDaoImpl languageDao = new LanguageDaoImpl(connection);
            Language language = languageDao.findEntityByLanguageCode(locale);

            if(language == null) {
                throw new DBException("Nothing was found in languages");
            }

            return orderDao.findAllWithCityNamesByAccountId(
                    language.getId(),
                    accountId,
                    orderBy,
                    sortOrder,
                    status,
                    limit,
                    offset
            );
        } catch (DBException e) {
            logger.error("Error while finding order in DB", e);
            throw new ServiceException("Failed to find orders", e);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    logger.error("Error while closing connection", e);
                    throw new ServiceException("Failed to close connection");
                }
            }
        }
    }

    @Override
    public List<Order> findAllByCityWithAccountDetailsAndCityNames(
            String locale,
            String orderBy,
            String sortOrder,
            String whereName,
            String whereValue,
            int limit,
            int offset
    ) throws ServiceException {
        Connection connection = null;

        try {
            connection = ConnectionPool.INSTANCE.getConnection();// add exception
            OrderDaoImpl orderDao = new OrderDaoImpl(connection);
            LanguageDaoImpl languageDao = new LanguageDaoImpl(connection);
            Language language = languageDao.findEntityByLanguageCode(locale);

            if(language == null) {
                throw new DBException("Nothing was found in languages");
            }

            return orderDao.findAllByCityWithAccountDetailsAndCityNames(language.getId(), orderBy, sortOrder, whereName, whereValue, limit, offset);
        } catch (DBException e) {
            logger.error("Error while finding order in DB", e);
            throw new ServiceException("Failed to find orders", e);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    logger.error("Error while closing connection", e);
                    throw new ServiceException("Failed to close connection");
                }
            }
        }
    }

    @Override
    public int getNumberOfRowsByAccountIdAndStatus(long id, String status) throws ServiceException {
        Connection connection = null;

        try {
            connection = ConnectionPool.INSTANCE.getConnection();// add exception
            OrderDaoImpl orderDao = new OrderDaoImpl(connection);

            return orderDao.getNumberOfRowsByAccountIdAndStatus(id, status);
        } catch (DBException e) {
            logger.error("Error while finding number of orders in DB", e);
            throw new ServiceException("Failed to read order from database", e);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    logger.error("Error while closing connection", e);
                    throw new ServiceException("Failed to close connection", e);
                }
            }
        }
    }

    @Override
    public int getNumberOfRowsWithCondition(String whereName, String whereValue) throws ServiceException {
        Connection connection = null;

        try {
            connection = ConnectionPool.INSTANCE.getConnection();// add exception
            OrderDaoImpl orderDao = new OrderDaoImpl(connection);

            return orderDao.getNumberOfRowsWithCondition(whereName, whereValue);
        } catch (DBException e) {
            logger.error("Error while finding number of orders in DB", e);
            throw new ServiceException("Failed to read order from database", e);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    logger.error("Error while closing connection", e);
                    throw new ServiceException("Failed to close connection", e);
                }
            }
        }
    }

    @Override
    public int getNumberOfRowsByCityLike(String whereName, String whereValue) throws ServiceException {
        Connection connection = null;

        try {
            connection = ConnectionPool.INSTANCE.getConnection();// add exception
            OrderDaoImpl orderDao = new OrderDaoImpl(connection);

            return orderDao.getNumberOfRowsByCityLike(whereName, whereValue);
        } catch (DBException e) {
            logger.error("Error while finding number of orders in DB", e);
            throw new ServiceException("Failed to read order from database", e);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    logger.error("Error while closing connection", e);
                    throw new ServiceException("Failed to close connection", e);
                }
            }
        }
    }
}
