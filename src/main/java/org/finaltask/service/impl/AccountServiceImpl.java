package org.finaltask.service.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.finaltask.connectionpool.ConnectionPool;
import org.finaltask.dao.DBException;
import org.finaltask.dao.impl.AccountDaoImpl;
import org.finaltask.dao.impl.AccountDetailsDaoImpl;
import org.finaltask.dao.impl.RoleDaoImpl;
import org.finaltask.dao.impl.WalletDaoImpl;
import org.finaltask.entity.Account;
import org.finaltask.entity.RoleEnum;
import org.finaltask.service.AccountService;
import org.finaltask.service.ServiceException;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class AccountServiceImpl implements AccountService {

    private static final Logger logger = LogManager.getLogger(AccountServiceImpl.class);

    @Override
    public boolean createAccountWithDetailsAndWallet(Account account) throws ServiceException {

        Connection connection = null;
        boolean result;

        try {
            connection = ConnectionPool.INSTANCE.getConnection();// add exception
            connection.setAutoCommit(false);

            AccountDaoImpl accountDao = new AccountDaoImpl(connection);
            AccountDetailsDaoImpl accountDetailsDao = new AccountDetailsDaoImpl(connection);
            WalletDaoImpl walletDao = new WalletDaoImpl(connection);

            if (account.getRole().getName() == RoleEnum.MANAGER) {

                Account manager = accountDao.findEntityByRole(RoleEnum.MANAGER);
                if (manager == null) {
                    logger.error("No manager account in DB");
                    throw new ServiceException("failed to create account with wallet and details");
                }
                account.getWallet().setId(manager.getWallet().getId());
                account.getWallet().setValue(manager.getWallet().getValue());

                result = accountDetailsDao.create(account.getAccountDetails()) &
                        accountDao.create(account);
            } else {
                result = walletDao.create(account.getWallet()) &
                        accountDetailsDao.create(account.getAccountDetails()) &
                        accountDao.create(account);
            }

            connection.commit();

        } catch (DBException | SQLException e) {

            try {
                connection.rollback();
            } catch (SQLException ex) {
                logger.error("Error cannot rollback transaction while creating account", ex);
                throw new ServiceException("failed to create account with wallet and details");
            }

            logger.error("Error while creating account in DB", e);
            throw new ServiceException("failed to create account with wallet and details", e);
        } finally {
            if (connection != null) {
                try {
                    connection.setAutoCommit(true);
                    connection.close();
                } catch (SQLException e) {
                    logger.error("Error while closing connection", e);
                    throw new ServiceException("Failed to close connection", e);
                }
            }
        }
        return result;
    }

    @Override
    public Account findAccountProfileById(long id) throws ServiceException {
        Connection connection = null;
        Account account;

        try {
            connection = ConnectionPool.INSTANCE.getConnection();// add exception

            AccountDaoImpl accountDao = new AccountDaoImpl(connection);
            RoleDaoImpl roleDao = new RoleDaoImpl(connection);
            WalletDaoImpl walletDao = new WalletDaoImpl(connection);
            AccountDetailsDaoImpl accountDetailsDao = new AccountDetailsDaoImpl(connection);

            account = accountDao.findEntityById(id);

            if (account != null) {
                account.setRole(roleDao.findEntityById(account.getRole().getId()));
                account.setWallet(walletDao.findEntityById(account.getWallet().getId()));
                account.setAccountDetails(accountDetailsDao.findEntityById(account.getAccountDetails().getId()));
            }

            return account;

        } catch (DBException e) {
            logger.error("Error while reading account from DB", e);
            throw new ServiceException("failed to read account with role, wallet, account details", e);
        } finally {
            if (connection != null) {
                try {

                    connection.close();
                } catch (SQLException e) {
                    logger.error("Error while closing connection", e);
                    throw new ServiceException("Failed to close connection", e);
                }
            }
        }
    }

    @Override
    public Account findEntityByLogin(String login) throws ServiceException {
        Connection connection = null;
        Account account;

        try {
            connection = ConnectionPool.INSTANCE.getConnection();// add exception            

            AccountDaoImpl accountDao = new AccountDaoImpl(connection);
            RoleDaoImpl roleDao = new RoleDaoImpl(connection);
            WalletDaoImpl walletDao = new WalletDaoImpl(connection);

            account = accountDao.findEntityByLogin(login);

            if (account != null) {
                account.setRole(roleDao.findEntityById(account.getRole().getId()));
                account.setWallet(walletDao.findEntityById(account.getWallet().getId()));
            }

            return account;

        } catch (DBException e) {
            logger.error("Error while reading account from DB", e);
            throw new ServiceException("failed to read account with role", e);
        } finally {
            if (connection != null) {
                try {

                    connection.close();
                } catch (SQLException e) {
                    logger.error("Error while closing connection", e);
                    throw new ServiceException("Failed to close connection", e);
                }
            }
        }

    }

    @Override
    public Account findEntityByRole(RoleEnum role) throws ServiceException {
        Connection connection = null;
        Account account;

        try {
            connection = ConnectionPool.INSTANCE.getConnection();// add exception            

            AccountDaoImpl accountDao = new AccountDaoImpl(connection);
            RoleDaoImpl roleDao = new RoleDaoImpl(connection);
            WalletDaoImpl walletDao = new WalletDaoImpl(connection);

            account = accountDao.findEntityByRole(role);

            if (account != null) {
                account.setRole(roleDao.findEntityById(account.getRole().getId()));
                account.setWallet(walletDao.findEntityById(account.getWallet().getId()));
            }

            return account;

        } catch (DBException e) {
            logger.error("Error while reading account from DB", e);
            throw new ServiceException("failed to read account with role", e);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    logger.error("Error while closing connection", e);
                    throw new ServiceException("Failed to close connection", e);
                }
            }
        }
    }

    @Override
    public List<Account> findAllByRoleWithAccountDetails(RoleEnum role, String orderBy, String sortOrder, int limit, int offset) throws ServiceException {
        Connection connection = null;
        List<Account> account;

        try {
            connection = ConnectionPool.INSTANCE.getConnection();// add exception

            AccountDaoImpl accountDao = new AccountDaoImpl(connection);

            account = accountDao.findAllWithAccountDetails(role, orderBy, sortOrder, limit, offset);

            return account;

        } catch (DBException e) {
            logger.error("Error while reading account from DB", e);
            throw new ServiceException("failed to read account with role", e);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    logger.error("Error while closing connection", e);
                    throw new ServiceException("Failed to close connection", e);
                }
            }
        }
    }

    @Override
    public int getNumberOfRowsByRole(RoleEnum role) throws ServiceException {
        Connection connection = null;

        try {
            connection = ConnectionPool.INSTANCE.getConnection();// add exception
            AccountDaoImpl accountDao = new AccountDaoImpl(connection);

            return accountDao.getNumberOfRows(role);
        } catch (DBException e) {
            logger.error("Error while finding number of accounts in DB", e);
            throw new ServiceException("Failed to read order from database", e);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    logger.error("Error while closing connection", e);
                    throw new ServiceException("Failed to close connection", e);
                }
            }
        }
    }

    @Override
    public void updateFields(Map<String, String> fieldsForUpdate, long id) throws ServiceException {
        Connection connection = null;
        Set<String> accountFields = Set.of(AccountDaoImpl.LOGIN, AccountDaoImpl.PASSWORD);
        Set<String> accountDetailsFields = Set.of(
                AccountDetailsDaoImpl.NAME,
                AccountDetailsDaoImpl.SURNAME,
                AccountDetailsDaoImpl.EMAIL,
                AccountDetailsDaoImpl.PHONE,
                AccountDetailsDaoImpl.CITY);

        try {
            connection = ConnectionPool.INSTANCE.getConnection();// add exception
            connection.setAutoCommit(false);

            AccountDaoImpl accountDao = new AccountDaoImpl(connection);
            AccountDetailsDaoImpl accountDetailsDao = new AccountDetailsDaoImpl(connection);

            for (Map.Entry<String, String> entry : fieldsForUpdate.entrySet()) {

                if (accountFields.contains(entry.getKey())) {
                    accountDao.updateField(id, entry.getKey(), entry.getValue());
                }

                if (accountDetailsFields.contains(entry.getKey())) {
                    accountDetailsDao.updateField(id, entry.getKey(), entry.getValue());
                }
            }

            connection.commit();

        } catch (DBException | SQLException e) {

            try {
                connection.rollback();
            } catch (SQLException ex) {
                logger.error("Error cannot rollback transaction while creating account", ex);
                throw new ServiceException("failed to create account with wallet and details");
            }

            logger.error("Error while creating account in DB", e);
            throw new ServiceException("failed to create account with wallet and details", e);
        } finally {
            if (connection != null) {
                try {
                    connection.setAutoCommit(true);
                    connection.close();
                } catch (SQLException e) {
                    logger.error("Error while closing connection", e);
                    throw new ServiceException("Failed to close connection", e);
                }
            }
        }
    }
}
