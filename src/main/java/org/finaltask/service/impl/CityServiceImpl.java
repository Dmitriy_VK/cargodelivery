package org.finaltask.service.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.finaltask.connectionpool.ConnectionPool;
import org.finaltask.dao.DBException;
import org.finaltask.dao.impl.CityDaoImpl;
import org.finaltask.entity.City;
import org.finaltask.service.CityService;
import org.finaltask.service.ServiceException;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public class CityServiceImpl implements CityService {
    private static final Logger logger = LogManager.getLogger(CityServiceImpl.class);
    @Override
    public int findIdByName(String name) throws ServiceException {
        Connection connection = null;

        try {
            connection = ConnectionPool.INSTANCE.getConnection();// add exception
            CityDaoImpl cityDao = new CityDaoImpl(connection);

            return cityDao.findIdByName(name);
        } catch (DBException e) {
            logger.error("Error while finding city id in DB", e);
            throw new ServiceException("Failed to read city from database", e);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    logger.error("Error while closing connection", e);
                    throw new ServiceException("Failed to close connection", e);
                }
            }
        }
    }

    @Override
    public List<City> findAll() throws ServiceException {
        Connection connection = null;

        try {
            connection = ConnectionPool.INSTANCE.getConnection();// add exception
            CityDaoImpl cityDao = new CityDaoImpl(connection);

            return cityDao.findAll();
        } catch (DBException e) {
            logger.error("Error while finding city in DB", e);
            throw new ServiceException("Failed to read city from database", e);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    logger.error("Error while closing connection", e);
                    throw new ServiceException("Failed to close connection", e);
                }
            }
        }
    }
}
