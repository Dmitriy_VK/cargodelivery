package org.finaltask.service.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.finaltask.connectionpool.ConnectionPool;
import org.finaltask.dao.DBException;
import org.finaltask.dao.impl.CityDaoImpl;
import org.finaltask.dao.impl.LanguageDaoImpl;
import org.finaltask.dao.impl.OrderDaoImpl;
import org.finaltask.dao.impl.RouteDaoImpl;
import org.finaltask.entity.Language;
import org.finaltask.entity.Route;
import org.finaltask.service.RouteService;
import org.finaltask.service.ServiceException;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public class RouteServiceImpl implements RouteService {
	
	private static final Logger logger = LogManager.getLogger(RouteServiceImpl.class);
	
    @Override
    public List<Route> findAllWithCityNames(
            String locale,
            String orderBy,
            String sortOrder,
            String where,
            long cityId,
            int limit,
            int offset
    ) throws ServiceException {
        Connection connection = null;

        try {
            connection = ConnectionPool.INSTANCE.getConnection();// add exception
            RouteDaoImpl routeDao = new RouteDaoImpl(connection);
            LanguageDaoImpl languageDao = new LanguageDaoImpl(connection);
            Language language = languageDao.findEntityByLanguageCode(locale);

            if(language == null) {
                throw new DBException("Nothing was found in languages");
            }

            return routeDao.findAllWithCityNames(language.getId(), orderBy, sortOrder, where, cityId, limit, offset);
        } catch (DBException e) {
        	logger.error("Error while finding route in DB", e);
            throw new ServiceException("Failed to read routs from database", e);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                	logger.error("Error while closing connection", e);
                    throw new ServiceException("Failed to close connection");
                }
            }
        }
    }

    @Override
    public Route findEntityById(int id) throws ServiceException {
        Connection connection = null;
        Route route = null;

        try {
            connection = ConnectionPool.INSTANCE.getConnection();// add exception
            RouteDaoImpl routeDao = new RouteDaoImpl(connection);
            CityDaoImpl cityDao = new CityDaoImpl(connection);

            route = routeDao.findEntityById(id);
            route.getCityFrom().setName(cityDao.findEntityById(route.getCityFrom().getId()).getName());
            route.getCityTo().setName(cityDao.findEntityById(route.getCityTo().getId()).getName());
            return route;
        } catch (DBException e) {
        	logger.error("Error while finding route in DB", e);
            throw new ServiceException("Failed to read rout from database", e);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                	logger.error("Error while closing connection", e);
                    throw new ServiceException("Failed to close connection", e);
                }
            }
        }
    }

    @Override
    public boolean create(Route route) throws ServiceException {
        Connection connection = null;

        try {
            connection = ConnectionPool.INSTANCE.getConnection();// add exception
            RouteDaoImpl routeDao = new RouteDaoImpl(connection);
            return routeDao.create(route);
        } catch (DBException e) {
            logger.error("Error while creating route in DB", e);
            throw new ServiceException("Failed to create route", e);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    logger.error("Error while closing connection", e);
                    throw new ServiceException("Failed to close connection", e);
                }
            }
        }
    }

    @Override
    public int getNumberOfRows(String where, long cityId) throws ServiceException {
    	Connection connection = null;

        try {
            connection = ConnectionPool.INSTANCE.getConnection();// add exception
            RouteDaoImpl routeDao = new RouteDaoImpl(connection);

            return routeDao.getNumberOfRows(where, cityId);
        } catch (DBException e) {
        	logger.error("Error while finding number of routes in DB", e);
            throw new ServiceException("Failed to read rout from database", e);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                	logger.error("Error while closing connection", e);
                    throw new ServiceException("Failed to close connection", e);
                }
            }
        }
    }

    @Override
    public boolean isExistEntityWithCityFromIdCityToId(long cityFromId, long cityToId) throws ServiceException {
        Connection connection = null;

        try {
            connection = ConnectionPool.INSTANCE.getConnection();// add exception
            RouteDaoImpl routeDao = new RouteDaoImpl(connection);

            if(routeDao.getNumberOfRowsByCityFromIdCityToId(cityFromId, cityToId) > 0) {
                return true;
            }

            return false;
        } catch (DBException e) {
            logger.error("Error while finding number of routes in DB", e);
            throw new ServiceException("Failed to read rout from database", e);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    logger.error("Error while closing connection", e);
                    throw new ServiceException("Failed to close connection", e);
                }
            }
        }
    }
}
