package org.finaltask.service.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.finaltask.connectionpool.ConnectionPool;
import org.finaltask.dao.DBException;
import org.finaltask.dao.impl.CargoTypeDaoImpl;
import org.finaltask.dao.impl.LanguageDaoImpl;
import org.finaltask.entity.CargoType;
import org.finaltask.entity.Language;
import org.finaltask.service.CargoTypeService;
import org.finaltask.service.ServiceException;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public class CargoTypeServiceImpl implements CargoTypeService {
    private static final Logger logger = LogManager.getLogger(CargoTypeServiceImpl.class);

    @Override
    public List<CargoType> findAll() throws ServiceException {
        Connection connection = null;

        try {
            connection = ConnectionPool.INSTANCE.getConnection();// add exception
            CargoTypeDaoImpl cargoTypeDao = new CargoTypeDaoImpl(connection);

            return cargoTypeDao.findAll();
        } catch (DBException e) {
            logger.error("Error while finding cargo type in DB", e);
            throw new ServiceException("Failed to read cargo type from database", e);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    logger.error("Error while closing connection", e);
                    throw new ServiceException("Failed to close connection", e);
                }
            }
        }
    }

    @Override
    public List<CargoType> findAllWithTranslatedNames(String languageCode) throws ServiceException {
        Connection connection = null;

        try {
            connection = ConnectionPool.INSTANCE.getConnection();// add exception
            CargoTypeDaoImpl cargoTypeDao = new CargoTypeDaoImpl(connection);
            LanguageDaoImpl languageDao = new LanguageDaoImpl(connection);
            Language language = languageDao.findEntityByLanguageCode(languageCode);

            if(language == null) {
                throw new DBException("Nothing was found in languages");
            }

            return cargoTypeDao.findAllWithTranslatedNames(language.getId());
        } catch (DBException e) {
            logger.error("Error while finding cargo type in DB", e);
            throw new ServiceException("Failed to read cargo type from database", e);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    logger.error("Error while closing connection", e);
                    throw new ServiceException("Failed to close connection", e);
                }
            }
        }
    }

    @Override
    public CargoType findEntityByIdWithTranslatedName(long id, String languageCode) throws ServiceException {
        Connection connection = null;

        try {
            connection = ConnectionPool.INSTANCE.getConnection();// add exception
            CargoTypeDaoImpl cargoTypeDao = new CargoTypeDaoImpl(connection);
            LanguageDaoImpl languageDao = new LanguageDaoImpl(connection);
            Language language = languageDao.findEntityByLanguageCode(languageCode);

            if(language == null) {
                throw new DBException("Nothing was found in languages");
            }

            return cargoTypeDao.findEntityByIdWithTranslatedName(id, language.getId());
        } catch (DBException e) {
            logger.error("Error while finding cargo type in DB", e);
            throw new ServiceException("Failed to read cargo type from database", e);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    logger.error("Error while closing connection", e);
                    throw new ServiceException("Failed to close connection", e);
                }
            }
        }
    }
}
