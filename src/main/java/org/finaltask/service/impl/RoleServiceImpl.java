package org.finaltask.service.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.finaltask.connectionpool.ConnectionPool;
import org.finaltask.dao.DBException;
import org.finaltask.dao.impl.RoleDaoImpl;
import org.finaltask.entity.Role;
import org.finaltask.service.RoleService;
import org.finaltask.service.ServiceException;

import java.sql.Connection;
import java.sql.SQLException;

public class RoleServiceImpl implements RoleService {
    private static final Logger logger = LogManager.getLogger(RoleServiceImpl.class);
    @Override
    public Role findEntityById(long id) throws ServiceException {
        Connection connection = null;

        try {
            connection = ConnectionPool.INSTANCE.getConnection();// add exception
            RoleDaoImpl roleDao = new RoleDaoImpl(connection);

            return roleDao.findEntityById(id);
        } catch (DBException e) {
            logger.error("Error while finding role in DB", e);
            throw new ServiceException("Failed to read role from database", e);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    logger.error("Error while closing connection", e);
                    throw new ServiceException("Failed to close connection", e);
                }
            }
        }
    }
}
