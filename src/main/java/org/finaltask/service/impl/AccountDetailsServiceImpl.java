package org.finaltask.service.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.finaltask.connectionpool.ConnectionPool;
import org.finaltask.dao.DBException;
import org.finaltask.dao.impl.AccountDaoImpl;
import org.finaltask.dao.impl.AccountDetailsDaoImpl;
import org.finaltask.dao.impl.RoleDaoImpl;
import org.finaltask.dao.impl.WalletDaoImpl;
import org.finaltask.entity.Account;
import org.finaltask.entity.AccountDetails;
import org.finaltask.service.AccountDetailsService;
import org.finaltask.service.AccountService;
import org.finaltask.service.ServiceException;

import java.sql.Connection;
import java.sql.SQLException;

public class AccountDetailsServiceImpl implements AccountDetailsService {

    private static final Logger logger = LogManager.getLogger(AccountDetailsServiceImpl.class);

    @Override
    public AccountDetails findEntityById(long id) throws ServiceException {
        Connection connection = null;
        AccountDetails accountDetails = null;

        try {
            connection = ConnectionPool.INSTANCE.getConnection();// add exception

            AccountDetailsDaoImpl accountDetailsDao = new AccountDetailsDaoImpl(connection);

            accountDetails = accountDetailsDao.findEntityById(id);

            return accountDetails;

        } catch (DBException e) {
            logger.error("Error while reading account details from DB", e);
            throw new ServiceException("failed to read account details", e);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    logger.error("Error while closing connection", e);
                    throw new ServiceException("Failed to close connection", e);
                }
            }
        }
    }
}
