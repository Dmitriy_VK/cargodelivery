package org.finaltask.service.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.finaltask.connectionpool.ConnectionPool;
import org.finaltask.dao.DBException;
import org.finaltask.dao.impl.WalletDaoImpl;
import org.finaltask.entity.Wallet;
import org.finaltask.service.ServiceException;
import org.finaltask.service.WalletService;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.sql.Connection;
import java.sql.SQLException;

public class WalletServiceImpl implements WalletService {

    private static final Logger logger = LogManager.getLogger(WalletServiceImpl.class);

    @Override
    public void update(Wallet wallet) throws ServiceException {
        Connection connection = null;

        try {
            connection = ConnectionPool.INSTANCE.getConnection();// add exception
            WalletDaoImpl walletDao = new WalletDaoImpl(connection);
            walletDao.update(wallet);
        } catch (DBException e) {
        	logger.error("Error while updating wallet in DB", e);
            throw new ServiceException("Failed to read from database", e);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                	logger.error("Error while closing connection", e);
                    throw new ServiceException("Failed to close connection", e);
                }
            }
        }
    }
    
    @Override
    public Wallet findEntityById(long id) throws ServiceException {
        Connection connection = null;

        try {
            connection = ConnectionPool.INSTANCE.getConnection();// add exception
            WalletDaoImpl walletDao = new WalletDaoImpl(connection);
            return walletDao.findEntityById(id);
        } catch (DBException e) {
        	logger.error("Error while finding wallet in DB", e);
            throw new ServiceException("Failed to read from database", e);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                	logger.error("Error while closing connection", e);
                    throw new ServiceException("Failed to close connection", e);
                }
            }
        }
    }

    @Override
    public void fillUpWallet(long id, BigDecimal amount) throws ServiceException {
    	Connection connection = null;
    	
    	try {
    		connection = ConnectionPool.INSTANCE.getConnection();// add exception
    		connection.setAutoCommit(false);
            WalletDaoImpl walletDao = new WalletDaoImpl(connection);
            
            Wallet wallet = walletDao.findEntityById(id);
            BigDecimal walletValue = BigDecimal.valueOf(wallet.getValue()).setScale(2, RoundingMode.CEILING);
            BigDecimal newWalletValue = walletValue.add(amount);

            System.out.println("amount: " + amount + " walletValue: " + walletValue + " newWalletValue: " + newWalletValue);

            wallet.setValue(newWalletValue.doubleValue());
            walletDao.update(wallet);
            connection.commit();            
    	} catch (DBException | SQLException e) {

            try {
                connection.rollback();
            } catch (SQLException ex) {
                logger.error("Error cannot rollback transaction in WalletService", ex);
                throw new ServiceException("failed to update wallet in DB", ex);
            }

            logger.error("Error while executing update wallet in WalletService", e);
            throw new ServiceException("failed to update wallet in DB", e);
        } finally {
            if (connection != null) {
                try {
                    connection.setAutoCommit(true);
                    connection.close();
                } catch (SQLException e) {
                    logger.error("Error while closing connection", e);
                    throw new ServiceException("Failed to close connection", e);
                }
            }
        }
    }
}
