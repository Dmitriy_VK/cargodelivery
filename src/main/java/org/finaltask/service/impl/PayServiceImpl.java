package org.finaltask.service.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.finaltask.connectionpool.ConnectionPool;
import org.finaltask.dao.DBException;
import org.finaltask.dao.impl.AccountDaoImpl;
import org.finaltask.dao.impl.OrderDaoImpl;
import org.finaltask.dao.impl.WalletDaoImpl;
import org.finaltask.entity.*;
import org.finaltask.service.PayService;
import org.finaltask.service.PayServiceException;
import org.finaltask.service.ServiceException;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Connection;
import java.sql.SQLException;

public class PayServiceImpl implements PayService {
    private static final Logger logger = LogManager.getLogger(PayServiceImpl.class);
    @Override
    public void payForOrder(long orderId) throws ServiceException, PayServiceException {
        Connection connection = null;
        BigDecimal userValue;
        BigDecimal companyValue;
        BigDecimal orderValue;
        BigDecimal resultValueUser;
        BigDecimal resultValueCompany;
        
        try {
            connection = ConnectionPool.INSTANCE.getConnection();// add exception
            connection.setAutoCommit(false);

            OrderDaoImpl orderDao = new OrderDaoImpl(connection);
            AccountDaoImpl accountDao = new AccountDaoImpl(connection);
            WalletDaoImpl walletDao = new WalletDaoImpl(connection);

            Order currentOrder = orderDao.findEntityById(orderId);
            Account userAccount = accountDao.findEntityById(currentOrder.getAccount().getId());
            Account managerAccount = accountDao.findEntityByRole(RoleEnum.MANAGER);
            Wallet userWallet = walletDao.findEntityById(userAccount.getWallet().getId());
            Wallet companyWallet = walletDao.findEntityById(managerAccount.getWallet().getId());

            userValue = new BigDecimal(userWallet.getValue()).setScale(2, RoundingMode.CEILING);
            companyValue = new BigDecimal(companyWallet.getValue()).setScale(2, RoundingMode.CEILING);
            orderValue = new BigDecimal(currentOrder.getPrice()).setScale(2, RoundingMode.CEILING);

            if (userValue.compareTo(orderValue) >= 0) {
                resultValueUser = userValue.subtract(orderValue);
                resultValueCompany = companyValue.add(orderValue);
            } else {
                logger.info("Invalid balance");
                throw new PayServiceException("Invalid balance");
            }

            userWallet.setValue(resultValueUser.doubleValue());
            companyWallet.setValue(resultValueCompany.doubleValue());
            currentOrder.setOrderStatus(Status.PAID);

            walletDao.update(userWallet);
            walletDao.update(companyWallet);
            orderDao.updateStatus(currentOrder);

            connection.commit();

        } catch (DBException | SQLException e) {

            try {
                connection.rollback();
            } catch (SQLException ex) {
                logger.error("Error cannot rollback transaction in PayService", ex);
                throw new ServiceException("failed to create account with wallet and details", ex);
            }

            logger.error("Error while executing PayService", e);
            throw new ServiceException("failed to create account with wallet and details", e);
        } finally {
            if (connection != null) {
                try {
                    connection.setAutoCommit(true);
                    connection.close();
                } catch (SQLException e) {
                    logger.error("Error while closing connection", e);
                    throw new ServiceException("Failed to close connection", e);
                }
            }
        }
    }
}
