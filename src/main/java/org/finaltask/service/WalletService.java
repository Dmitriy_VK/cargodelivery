package org.finaltask.service;

import org.finaltask.entity.Wallet;

import java.math.BigDecimal;

public interface WalletService {
    void update(Wallet wallet) throws ServiceException;
    Wallet findEntityById(long id) throws ServiceException;
    void fillUpWallet(long id, BigDecimal amount) throws ServiceException;
}
