package org.finaltask.service;

import org.finaltask.entity.Order;
import org.finaltask.entity.Status;

import java.util.List;

public interface OrderService {
    boolean create(Order order) throws ServiceException;
    Order findEntityById(long id) throws ServiceException;
    Order findEntityByIdWithRouteAndAccount(long id) throws ServiceException;

    void updateStatus(long orderId, Status newStatus) throws ServiceException;

    List<Order> findAllWithAccountDetailsAndCityNames(
            String locale,
            String orderBy,
            String sortOrder,
            String whereName,
            String whereValue,
            int limit,
            int offset
    ) throws ServiceException;
    int getNumberOfRowsByAccountIdAndStatus(long id, String status) throws ServiceException;
    int getNumberOfRowsWithCondition(String whereName, String whereValue) throws ServiceException;
    int getNumberOfRowsByCityLike(String whereName, String whereValue) throws ServiceException;

    List<Order> findAllWithCityNamesByAccountId(
            String locale,
            long accountId,
            String orderBy,
            String sortOrder,
            String status,
            int limit,
            int offset
    ) throws ServiceException;
    List<Order> findAllByCityWithAccountDetailsAndCityNames(
            String locale,
            String orderBy,
            String sortOrder,
            String whereName,
            String whereValue,
            int limit,
            int offset
    ) throws ServiceException;
}
