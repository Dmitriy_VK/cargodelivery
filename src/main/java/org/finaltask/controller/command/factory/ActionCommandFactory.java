package org.finaltask.controller.command.factory;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.finaltask.controller.command.ActionCommand;
import org.finaltask.controller.command.ActionCommandEnum;

import javax.servlet.http.HttpServletRequest;

public final class ActionCommandFactory {

    private static final Logger logger = LogManager.getLogger(ActionCommandFactory.class);

    private ActionCommandFactory() {

    }

    public static ActionCommand getActionCommand(HttpServletRequest req) {
        String commandName = req.getServletPath();
        ActionCommand actionCommand = null;
        logger.info("Servlet path: " + commandName);

        if (commandName != null) {
            try {
                actionCommand = ActionCommandEnum
                        .valueOf(commandName.replace("/", "").toUpperCase())
                        .getActionCommand();
            } catch (IllegalArgumentException e) {
                logger.error("Error while determining the command", e);
                req.setAttribute("error", "Cannot determine the command");
                actionCommand = ActionCommandEnum.ERRORPAGE.getActionCommand();
            }
        } else {
            logger.error("Error while determining servlet path");
            req.setAttribute("error", "Cannot determine servlet path");
            actionCommand = ActionCommandEnum.ERRORPAGE.getActionCommand();
        }
        return actionCommand;
    }

}
