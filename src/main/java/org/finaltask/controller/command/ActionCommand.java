package org.finaltask.controller.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface ActionCommand {
    ActionCommandResult execute(HttpServletRequest req, HttpServletResponse resp);
}
