package org.finaltask.controller.command;

public class ActionCommandResult {
    private String path;
    private boolean isRedirect;

    public ActionCommandResult() {
    }

    public ActionCommandResult(String path) {
        this.path = path;
        this.isRedirect = false;
    }

    public ActionCommandResult(String path, boolean isRedirect) {
        this.path = path;
        this.isRedirect = isRedirect;
    }

    public String getPath() {
        return path;
    }

    public boolean isRedirect() {
        return isRedirect;
    }
}
