package org.finaltask.controller.command;

import org.finaltask.controller.command.impl.*;
import org.finaltask.controller.command.impl.common.*;
import org.finaltask.controller.command.impl.manager.*;
import org.finaltask.controller.command.impl.user.*;

public enum ActionCommandEnum {
    ADDROUTE(new AddRouteFormActionCommand()),
    ADDROUTEPROCESS(new AddRouteProcessActionCommand()),
    ADDROUTERESULT(new AddRouteResultActionCommand()),
    HOME(new HomePageActionCommand()),
    CALCULATEFORM(new CalculateFormPageActionCommand()),
    CALCULATE(new CalculatePageActionCommand()),
    CALCULATERESULT(new CalculateResultActionCommand()),
    CHANGESTATUS(new ChangeStatusActionCommand()),
    LOGIN(new LoginPageActionCommand()),
    REGISTRATION(new RegistrationPageActionCommand()),
    LOGOUT(new LogoutActionCommand()),
    DISPLAYACCOUNT(new DisplayAccountActionCommand()),
    MAKEORDER(new MakeOrderActionCommand()),
    MAKEORDERRESULT(new MakeOrderResultActionCommand()),
    PROCESSORDER(new ProcessOrderActionCommand()),
    VIEWORDERS(new ViewOrdersActionCommand()),
    VIEWPROFILE(new ViewProfileActionCommand()),
    VIEWACCOUNTS(new ViewAccountsActionCommand()),
    EDITPROFILE(new EditProfileActionCommand()),
    EDITPROFILERESULT(new EditProfileResultActionCommand()),
    PAYPAGE(new PayPageActionCommand()),
    PAYORDER(new PayOrderActionCommand()),
    PAYORDERRESULT(new PayOrderResultActionCommand()),
    FILLUPWALLETPAGE(new FillUpWalletPageActionCommand()),
    FILLUPWALLET(new FillUpWalletActionCommand()),
    FILLUPWALLETRESULT(new FillUpWalletResultActionCommand()),
    VIEWCITYREPORT(new ViewCityReportActionCommand()),
    VIEWDATEREPORT(new ViewDateReportActionCommand()),
    ERRORPAGE(new ErrorPageActionCommand());

    private ActionCommand command;

    ActionCommandEnum(ActionCommand command) {
        this.command = command;
    }

    public ActionCommand getActionCommand() {
        return command;
    }
}
