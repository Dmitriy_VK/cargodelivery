package org.finaltask.controller.command.impl.manager;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.finaltask.controller.command.ActionCommand;
import org.finaltask.controller.command.ActionCommandResult;
import org.finaltask.entity.City;
import org.finaltask.entity.Route;
import org.finaltask.service.CityService;
import org.finaltask.service.RouteService;
import org.finaltask.service.ServiceException;
import org.finaltask.service.impl.CityServiceImpl;
import org.finaltask.service.impl.RouteServiceImpl;
import org.finaltask.utils.Validation;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class AddRouteProcessActionCommand implements ActionCommand {
    private static final Logger logger = LogManager.getLogger(AddRouteProcessActionCommand.class);
    private final RouteService routeService = new RouteServiceImpl();
    private final CityService cityService = new CityServiceImpl();

    @Override
    public ActionCommandResult execute(HttpServletRequest req, HttpServletResponse resp) {
        boolean errorInputData = false;

        String formData = req.getParameter("formData");
        if (!"formData".equals(formData)) {
            logger.info("Clean order form form");
            return new ActionCommandResult("addRouteForm");
        }

        int cityFromId = 0;
        String value = req.getParameter("cityFromId");
        try {
            cityFromId = Integer.parseInt(value);
        } catch (NullPointerException | NumberFormatException e) {
            logger.error("Invalid city id was received (value = " + value + ")");
            req.setAttribute("error", "Internal error. Please try again");
            return new ActionCommandResult("error");
        }

        int cityToId = 0;
        value = req.getParameter("cityToId");
        try {
            cityToId = Integer.parseInt(value);
        } catch (NullPointerException | NumberFormatException e) {
            logger.error("Invalid city id was received (value = " + value + ")");
            req.setAttribute("error", "Internal error. Please try again");
            return new ActionCommandResult("error");
        }

        if (cityFromId == cityToId) {
            logger.info("The same city id was received");
            req.setAttribute("error_same_city", true);
            errorInputData = true;
        }

        int distance = 0;
        value = req.getParameter("distance");

        if (value == null || !Validation.validateVolume(value)) {
            logger.info("Invalid distance was received");
            req.setAttribute("error_distance", true);
            errorInputData = true;
        } else {

            try {
                distance = Integer.parseInt(value);
            } catch (NumberFormatException e) {
                logger.error("Distance value not a number (value = " + value + ")");
                req.setAttribute("error_distance", true);
                errorInputData = true;
            }
        }

        double tariff = 0;
        value = req.getParameter("tariff");

        if (value == null || !Validation.validateWeight(value)) {
            logger.info("Invalid tariff was received");
            req.setAttribute("error_tariff", true);
            errorInputData = true;
        } else {

            try {
                tariff = Double.parseDouble(value);
            } catch (NumberFormatException e) {
                logger.error("Tariff value not a number (value = " + value + ")");
                req.setAttribute("error_tariff", true);
                errorInputData = true;
            }
        }

        try {
            if (routeService.isExistEntityWithCityFromIdCityToId(cityFromId, cityToId)) {
                logger.info("The route is exist in DB");
                req.setAttribute("error_route_exist", true);
                errorInputData = true;
            }
        } catch (ServiceException e) {
            logger.error("Error while getting route from DB", e);
            req.setAttribute("error", "Cannot connect to DB");
            return new ActionCommandResult("error");
        }

        if (errorInputData) {

            try {
                List<City> cityList = cityService.findAll();
                req.setAttribute("cityList", cityList);
            } catch (ServiceException e) {
                logger.error("Cannot get all cities from DB", e);
                req.setAttribute("error", "DB error. Cannot find data in DB");
                return new ActionCommandResult("error");
            }

            return new ActionCommandResult("addRouteForm");
        }

        boolean result;

        try {

                Route route = new Route();

                route.setCityFrom(new City(cityFromId));
                route.setCityTo(new City(cityToId));
                route.setDistance(distance);
                route.setTariff(tariff);

                result = routeService.create(route);

        } catch (ServiceException e) {
            logger.error("Error while creating route in DB", e);
            req.setAttribute("error", "Cannot connect to DB");
            return new ActionCommandResult("error");
        }

        return new ActionCommandResult("addRouteResult" + "?result=" + result, true);
    }
}
