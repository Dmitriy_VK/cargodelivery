package org.finaltask.controller.command.impl.user;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.finaltask.controller.command.ActionCommand;
import org.finaltask.controller.command.ActionCommandResult;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class FillUpWalletPageActionCommand implements ActionCommand {
    private static final Logger logger = LogManager.getLogger(FillUpWalletPageActionCommand.class);
    @Override
    public ActionCommandResult execute(HttpServletRequest req, HttpServletResponse resp) {
        return new ActionCommandResult("fillUpWallet");
    }
}
