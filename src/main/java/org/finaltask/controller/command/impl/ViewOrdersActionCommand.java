package org.finaltask.controller.command.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.finaltask.controller.command.ActionCommand;
import org.finaltask.controller.command.ActionCommandResult;
import org.finaltask.entity.Account;
import org.finaltask.entity.Order;
import org.finaltask.entity.RoleEnum;
import org.finaltask.service.OrderService;
import org.finaltask.service.ServiceException;
import org.finaltask.service.impl.OrderServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

public class ViewOrdersActionCommand implements ActionCommand {
	private static final Logger logger = LogManager.getLogger(ViewOrdersActionCommand.class);

	@Override
	public ActionCommandResult execute(HttpServletRequest req, HttpServletResponse resp) {

		HttpSession session = req.getSession(false);
		Account account = null;

		if (session != null && session.getAttribute("account") != null) {
			account = (Account) session.getAttribute("account");

		} else {
			logger.info("No session or authorized user");
			req.setAttribute("error", "Please login");
			return new ActionCommandResult("error");
		}

		if (account.getRole().getName() == RoleEnum.USER) {
			return viewOrdersUser(req, session, account);
		} else if (account.getRole().getName() == RoleEnum.MANAGER) {
			return viewOrdersManager(req, session);
		} else {
			logger.info("Cannot define account role");
			req.setAttribute("error", "Please login");
			return new ActionCommandResult("error");
		}
	}

	private ActionCommandResult viewOrdersUser(HttpServletRequest req, HttpSession session, Account account) {
		OrderService orderService = new OrderServiceImpl();
		String status = req.getParameter("status");
		status = status != null ? status : "";

		String orderBy = req.getParameter("orderBy");
		orderBy = orderBy != null ? orderBy : "id";

		String sortOrder = req.getParameter("sortOrder");
		sortOrder = sortOrder != null ? sortOrder : "ASC";

		int recordsPerPage = 0;
		String value = req.getParameter("recordsPerPage");
		try {
			recordsPerPage = value != null ? Integer.parseInt(value) : 5;
		} catch (NumberFormatException e) {
			logger.error("Limit value not a number (value = " + value + ")", e);
			recordsPerPage = 5;
		}

		int currentPage = 0;
		value = req.getParameter("currentPage");
		try {
			currentPage = value != null ? Integer.parseInt(value) : 1;
		} catch (NumberFormatException e) {
			logger.error("Offset value not a number (value = " + value + ")", e);
			currentPage = 1;
		}

		int offset = currentPage * recordsPerPage - recordsPerPage;

		try {

			int rows = orderService.getNumberOfRowsByAccountIdAndStatus(account.getId(), status);

			int numOfPages = rows != recordsPerPage ? rows / recordsPerPage : rows / recordsPerPage - 1;

			if (numOfPages % recordsPerPage > 0) {
				numOfPages++;
			}

			String locale = (String) session.getAttribute("locale");
			locale = locale != null ? locale : "en";

			List<Order> orders = orderService.findAllWithCityNamesByAccountId(
					locale,
					account.getId(),
					orderBy,
					sortOrder,
					status,
					recordsPerPage,
					offset);

			req.setAttribute("orders", orders);
			req.setAttribute("numOfPages", numOfPages);
			req.setAttribute("currentPage", currentPage);
			req.setAttribute("recordsPerPage", recordsPerPage);
			req.setAttribute("orderBy", orderBy);
			req.setAttribute("sortOrder", sortOrder);
			req.setAttribute("status", status);

		} catch (ServiceException e) {
			logger.error("Error while getting orders from DB", e);
			req.setAttribute("error", "Cannot connect to DB");
			return new ActionCommandResult("error");
		}

		return new ActionCommandResult("viewOrdersUser");
	}

	private ActionCommandResult viewOrdersManager(HttpServletRequest req, HttpSession session) {
		OrderService orderService = new OrderServiceImpl();

		String whereValue = req.getParameter("whereValue");
		whereValue = whereValue != null ? whereValue : "";

		String whereName;
		if (whereValue.isEmpty() || "all".equals(whereValue)) {
			whereName = "";
		} else {
			whereName = req.getParameter("whereName");
			whereName = whereName != null ? whereName : "";
		}

		String orderBy = req.getParameter("orderBy");
		orderBy = orderBy != null ? orderBy : "id";

		String sortOrder = req.getParameter("sortOrder");
		sortOrder = sortOrder != null ? sortOrder : "ASC";

		int recordsPerPage = 0;
		String value = req.getParameter("recordsPerPage");
		try {
			recordsPerPage = value != null ? Integer.parseInt(value) : 5;
		} catch (NumberFormatException e) {
			logger.error("Limit value not a number (value = " + value + ")", e);
			recordsPerPage = 5;
		}

		int currentPage = 0;
		value = req.getParameter("currentPage");
		try {
			currentPage = value != null ? Integer.parseInt(value) : 1;
		} catch (NumberFormatException e) {
			logger.error("Offset value not a number (value = " + value + ")", e);
			currentPage = 1;
		}

		int offset = currentPage * recordsPerPage - recordsPerPage;

		try {

			int rows = orderService.getNumberOfRowsWithCondition(whereName, whereValue);

			int numOfPages = rows != recordsPerPage ? rows / recordsPerPage : rows / recordsPerPage - 1;

			if (numOfPages % recordsPerPage > 0) {
				numOfPages++;
			}

			String locale = (String) session.getAttribute("locale");
			locale = locale != null ? locale : "en";

			List<Order> orders = orderService.findAllWithAccountDetailsAndCityNames(locale, orderBy, sortOrder, whereName,
					whereValue, recordsPerPage, offset);

			req.setAttribute("orders", orders);
			req.setAttribute("numOfPages", numOfPages);
			req.setAttribute("currentPage", currentPage);
			req.setAttribute("recordsPerPage", recordsPerPage);
			req.setAttribute("orderBy", orderBy);
			req.setAttribute("sortOrder", sortOrder);
			req.setAttribute("whereValue", whereValue);
			req.setAttribute("whereName", whereName);

		} catch (ServiceException e) {
			logger.error("Error while getting orders from DB", e);
			req.setAttribute("error", "Cannot connect to DB");
			return new ActionCommandResult("error");
		}

		return new ActionCommandResult("viewOrdersManager");
	}
}
