package org.finaltask.controller.command.impl.manager;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.finaltask.controller.command.ActionCommand;
import org.finaltask.controller.command.ActionCommandResult;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class AddRouteResultActionCommand implements ActionCommand {
    private static final Logger logger = LogManager.getLogger(AddRouteResultActionCommand.class);
    @Override
    public ActionCommandResult execute(HttpServletRequest req, HttpServletResponse resp) {
        
        String result = req.getParameter("result");
        if (result == null) {
        	logger.error("Error while creating route");
            req.setAttribute("error", "Invalid data");
            return new ActionCommandResult("error");
        }
        
        req.setAttribute("result", result);
        
        return new ActionCommandResult("addRouteResult");
    }
}
