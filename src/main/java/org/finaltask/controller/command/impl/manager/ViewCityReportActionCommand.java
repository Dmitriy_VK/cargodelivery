package org.finaltask.controller.command.impl.manager;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.finaltask.controller.command.ActionCommand;
import org.finaltask.controller.command.ActionCommandResult;
import org.finaltask.entity.Order;
import org.finaltask.service.OrderService;
import org.finaltask.service.ServiceException;
import org.finaltask.service.impl.OrderServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;
import java.util.List;

public class ViewCityReportActionCommand implements ActionCommand {
    private static final Logger logger = LogManager.getLogger(ViewCityReportActionCommand.class);

    @Override
    public ActionCommandResult execute(HttpServletRequest req, HttpServletResponse resp) {
    	OrderService orderService = new OrderServiceImpl();

        String whereName = req.getParameter("whereName");
        whereName = whereName != null ? whereName : "";

        String whereValue = req.getParameter("whereValue");
        whereValue = whereValue != null ? whereValue : "";

        String orderBy = req.getParameter("orderBy");
        orderBy = orderBy != null ? orderBy : "id";

        String sortOrder = req.getParameter("sortOrder");
        sortOrder = sortOrder != null ? sortOrder : "ASC";

        int recordsPerPage = 0;
        String value = req.getParameter("recordsPerPage");
        try {
            recordsPerPage = value != null ? Integer.parseInt(value) : 5;
        } catch (NumberFormatException e) {
            logger.error("Limit value not a number (value = " + value + ")", e);
            recordsPerPage = 5;
        }

        int currentPage = 0;
        value = req.getParameter("currentPage");
        try {
            currentPage = value != null ? Integer.parseInt(value) : 1;
        } catch (NumberFormatException e) {
            logger.error("Offset value not a number (value = " + value + ")", e);
            currentPage = 1;
        }

        int offset = currentPage * recordsPerPage - recordsPerPage;

        try {

            int rows = orderService.getNumberOfRowsByCityLike(whereName, whereValue);

            int numOfPages = rows != recordsPerPage ? rows / recordsPerPage : rows / recordsPerPage - 1;

            if (numOfPages % recordsPerPage > 0) {
                numOfPages++;
            }

            String locale = (String) req.getSession().getAttribute("locale");
            locale = locale != null ? locale : "en";

            List<Order> orders = orderService.findAllByCityWithAccountDetailsAndCityNames(
                    locale,
                    orderBy,
                    sortOrder,
                    whereName,
                    whereValue,
                    recordsPerPage,
                    offset
            );

            req.setAttribute("orders", orders);
            req.setAttribute("numOfPages", numOfPages);
            req.setAttribute("currentPage", currentPage);
            req.setAttribute("recordsPerPage", recordsPerPage);
            req.setAttribute("orderBy", orderBy);
            req.setAttribute("sortOrder", sortOrder);
            req.setAttribute("whereName", whereName);
            req.setAttribute("whereValue", whereValue);

        } catch (ServiceException e) {
            logger.error("Error while getting orders from DB", e);
            req.setAttribute("error", "Cannot connect to DB");
            return new ActionCommandResult("error");
        }

        return new ActionCommandResult("viewCityReport");
    }
}
