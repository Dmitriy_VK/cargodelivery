package org.finaltask.controller.command.impl.manager;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.finaltask.controller.command.ActionCommand;
import org.finaltask.controller.command.ActionCommandResult;
import org.finaltask.entity.Order;
import org.finaltask.service.OrderService;
import org.finaltask.service.ServiceException;
import org.finaltask.service.impl.OrderServiceImpl;
import org.finaltask.utils.Validation;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

public class ViewDateReportActionCommand implements ActionCommand {
    private static final Logger logger = LogManager.getLogger(ViewDateReportActionCommand.class);

    @Override
    public ActionCommandResult execute(HttpServletRequest req, HttpServletResponse resp) {
    	OrderService orderService = new OrderServiceImpl();
        Date deliveryDateSQL;

        String deliveryDate = req.getParameter("deliveryDate");
        if (deliveryDate == null || !Validation.validateDate(deliveryDate)) {
            logger.info("wrong delivery date was received");
            req.setAttribute("error_deliveryDate", true);
            return new ActionCommandResult("viewDateReport");
        } else {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");

            try {
                deliveryDateSQL = new Date(simpleDateFormat.parse(deliveryDate).getTime());
            } catch (ParseException e) {
                logger.error("Error while parsing delivery date", e);
                req.setAttribute("error", "Cannot parse date");
                return new ActionCommandResult("error");
            }
        }
        
        String orderBy = req.getParameter("orderBy");
        orderBy = orderBy != null ? orderBy : "id";

        String sortOrder = req.getParameter("sortOrder");
        sortOrder = sortOrder != null ? sortOrder : "ASC";

        int recordsPerPage = 0;
        String value = req.getParameter("recordsPerPage");
        try {
            recordsPerPage = value != null ? Integer.parseInt(value) : 5;
        } catch (NumberFormatException e) {
            logger.error("Limit value not a number (value = " + value + ")", e);
            recordsPerPage = 5;
        }


        int currentPage = 0;
        value = req.getParameter("currentPage");
        try {
            currentPage = value != null ? Integer.parseInt(value) : 1;
        } catch (NumberFormatException e) {
            logger.error("Offset value not a number (value = " + value + ")", e);
            currentPage = 1;
        }

        int offset = currentPage * recordsPerPage - recordsPerPage;

        try {

            int rows = orderService.getNumberOfRowsWithCondition("delivery_date", deliveryDateSQL.toString());

            int numOfPages = rows != recordsPerPage ? rows / recordsPerPage : rows / recordsPerPage - 1;

            if (numOfPages % recordsPerPage > 0) {
                numOfPages++;
            }

            String locale = (String) req.getSession().getAttribute("locale");
            locale = locale != null ? locale : "en";

            List<Order> orders = orderService.findAllWithAccountDetailsAndCityNames(
                    locale,
                    orderBy,
                    sortOrder,
                    "delivery_date",
                    deliveryDateSQL.toString(),
                    recordsPerPage,
                    offset
            );

            req.setAttribute("orders", orders);
            req.setAttribute("numOfPages", numOfPages);
            req.setAttribute("currentPage", currentPage);
            req.setAttribute("recordsPerPage", recordsPerPage);
            req.setAttribute("orderBy", orderBy);
            req.setAttribute("sortOrder", sortOrder);
            req.setAttribute("deliveryDate", deliveryDate);

        } catch (ServiceException e) {
            logger.error("Error while getting orders from DB", e);
            req.setAttribute("error", "Cannot connect to DB");
            return new ActionCommandResult("error");
        }

        return new ActionCommandResult("viewDateReport");
    }
}
