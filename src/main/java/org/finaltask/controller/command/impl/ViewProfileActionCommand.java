package org.finaltask.controller.command.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.finaltask.controller.command.ActionCommand;
import org.finaltask.controller.command.ActionCommandResult;
import org.finaltask.entity.Account;
import org.finaltask.service.AccountService;
import org.finaltask.service.ServiceException;
import org.finaltask.service.impl.AccountServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class ViewProfileActionCommand implements ActionCommand {
	private static final Logger logger = LogManager.getLogger(ViewProfileActionCommand.class);
	private final AccountService accountService = new AccountServiceImpl();

	@Override
	public ActionCommandResult execute(HttpServletRequest req, HttpServletResponse resp) {
		String path = null;

		String action = req.getParameter("action");
		if ("edit".equals(action)) {
			path = "editProfile";
		} else if ("view".equals(action)) {
			path = "viewProfile";
		} else {
			req.setAttribute("error", "Invalid data");
			return new ActionCommandResult("error");
		}
		
		HttpSession session = req.getSession();
		
		if (session != null && session.getAttribute("account") != null) {
			Account accountInSession = (Account) session.getAttribute("account");
			try {
				Account accountProfile = accountService.findAccountProfileById(accountInSession.getId());
				req.setAttribute("accountProfile", accountProfile);
			} catch (ServiceException e) {
				logger.error("Error while getting account with details and wallet from DB", e);
				req.setAttribute("error", "Cannot connect to DB");
				return new ActionCommandResult("error");
			}
		} else {
			logger.error("No session or account in session");
			req.setAttribute("error", "Please login");
			return new ActionCommandResult("error");
		}

		return new ActionCommandResult(path);
	}
}
