package org.finaltask.controller.command.impl.common;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.finaltask.controller.command.ActionCommand;
import org.finaltask.controller.command.ActionCommandResult;
import org.finaltask.entity.CargoType;
import org.finaltask.entity.Route;
import org.finaltask.service.CargoTypeService;
import org.finaltask.service.ServiceException;
import org.finaltask.service.impl.CargoTypeServiceImpl;
import org.finaltask.utils.Cost;
import org.finaltask.utils.Validation;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

public class CalculatePageActionCommand implements ActionCommand {
    private static final Logger logger = LogManager.getLogger(CalculatePageActionCommand.class);
    private final CargoTypeService cargoTypeService = new CargoTypeServiceImpl();

    @Override
    public ActionCommandResult execute(HttpServletRequest req, HttpServletResponse resp) {
        boolean errorInputData = false;
        HttpSession session = req.getSession();
        CargoType cargoType;
        CalcResult calcResult;

        String formData = req.getParameter("formData");
        if (!"formData".equals(formData)) {
            logger.info("No form data for calculation");

            session.removeAttribute("cargoTypeList");

            String locale = (String) session.getAttribute("locale");
            locale = locale != null ? locale : "en";

            try {
                List<CargoType> cargoTypeList = cargoTypeService.findAllWithTranslatedNames(locale);
                session.setAttribute("cargoTypeList", cargoTypeList);
            } catch (ServiceException e) {
                logger.error("Error while getting data from DB", e);
                req.setAttribute("error", "Cannot connect to DB");
                return new ActionCommandResult("error");
            }
            return new ActionCommandResult("calcForm");
        }

        int cargoTypeId = 0;
        String value = req.getParameter("cargoTypeId");
        if (value == null) {
            logger.info("Invalid cargoTypeId was received");
            req.setAttribute("error_cargoType", true);
            errorInputData = true;
        } else {

            try {
                cargoTypeId = Integer.parseInt(value);
            } catch (NumberFormatException e) {
                logger.error("CargoTypeId value not a number (value = " + value + ")");
                req.setAttribute("error_cargoType", true);
                errorInputData = true;
            }
        }

        double weight = 0;
        value = req.getParameter("weight");

        if (value == null || !Validation.validateWeight(value)) {
            logger.info("wrong weight was received");
            req.setAttribute("error_weight", true);
            errorInputData = true;
        } else {
            try {
                weight = Double.parseDouble(value);
            } catch (NumberFormatException e) {
                logger.error("Weight value not a number (value = " + value + ")");
                req.setAttribute("error_weight", true);
                errorInputData = true;
            }
        }

        int volume = 0;
        value = req.getParameter("volume");

        if (value == null || !Validation.validateVolume(value)) {
            logger.info("wrong volume was received");
            req.setAttribute("error_volume", true);
            errorInputData = true;
        } else {

            try {
                volume = Integer.parseInt(value);
            } catch (NumberFormatException e) {
                logger.error("Volume value not a number (value = " + value + ")");
                req.setAttribute("error_volume", true);
                errorInputData = true;
            }
        }

        if (errorInputData) {
            return new ActionCommandResult("calcForm");
        }

        Route route = (Route) session.getAttribute("routeCalcPage");

        if (route == null) {
            logger.error("No any route in session");
            req.setAttribute("error", "WRONG DATA");
            return new ActionCommandResult("error");
        }


        try {
            List<CargoType> cargoTypeList = (List<CargoType>) session.getAttribute("cargoTypeList");
            final int cti = cargoTypeId;
            cargoType = cargoTypeList.stream().filter(ct -> ct.getId() == cti).findFirst().get();
        } catch (Exception e) {
            logger.error("Error while processing data", e);
            req.setAttribute("error", "Internal error. Please try again");
            return new ActionCommandResult("error");
        }

        calcResult = new CalcResult();
        calcResult.setLanguageCode((String) session.getAttribute("locale"));
        calcResult.setCargoType(new CargoType());
        calcResult.getCargoType().setId(cargoType.getId());
        calcResult.getCargoType().setName(cargoType.getName());
        calcResult.setWeight(weight);
        calcResult.setVolume(volume);
        calcResult.setDepartureCity(route.getCityFrom().getName());
        calcResult.setDestinationCity(route.getCityTo().getName());
        calcResult.setDistance(route.getDistance());
        calcResult.setCostOfDelivery(Cost.calculate(weight, volume, route, cargoType.getRatio()));

        session.setAttribute("calcResult", calcResult);
        session.removeAttribute("routeCalcPage");
        session.removeAttribute("cargoTypeList");

        return new ActionCommandResult("calculateResult", true);
    }

    public static class CalcResult {
        private String languageCode;
        private CargoType cargoType;
        private double weight;
        private int volume;
        private String departureCity;
        private String destinationCity;
        private int distance;
        private double costOfDelivery;

        public String getLanguageCode() {
            return languageCode;
        }

        public void setLanguageCode(String languageCode) {
            this.languageCode = languageCode;
        }

        public CargoType getCargoType() {
            return cargoType;
        }

        public void setCargoType(CargoType cargoType) {
            this.cargoType = cargoType;
        }

        public double getWeight() {
            return weight;
        }

        public void setWeight(double weight) {
            this.weight = weight;
        }

        public int getVolume() {
            return volume;
        }

        public void setVolume(int volume) {
            this.volume = volume;
        }

        public String getDepartureCity() {
            return departureCity;
        }

        public void setDepartureCity(String departureCity) {
            this.departureCity = departureCity;
        }

        public String getDestinationCity() {
            return destinationCity;
        }

        public void setDestinationCity(String destinationCity) {
            this.destinationCity = destinationCity;
        }

        public int getDistance() {
            return distance;
        }

        public void setDistance(int distance) {
            this.distance = distance;
        }

        public double getCostOfDelivery() {
            return costOfDelivery;
        }

        public void setCostOfDelivery(double costOfDelivery) {
            this.costOfDelivery = costOfDelivery;
        }
    }
}
