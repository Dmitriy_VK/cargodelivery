package org.finaltask.controller.command.impl.common;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.finaltask.controller.command.ActionCommand;
import org.finaltask.controller.command.ActionCommandResult;
import org.finaltask.entity.CargoType;
import org.finaltask.service.CargoTypeService;
import org.finaltask.service.ServiceException;
import org.finaltask.service.impl.CargoTypeServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class CalculateResultActionCommand implements ActionCommand {
    private static final Logger logger = LogManager.getLogger(CalculateResultActionCommand.class);
    private final CargoTypeService cargoTypeService = new CargoTypeServiceImpl();

    @Override
    public ActionCommandResult execute(HttpServletRequest req, HttpServletResponse resp) {
        HttpSession session = req.getSession();
        CargoType cargoType = null;
        CalculatePageActionCommand.CalcResult calcResult;

        calcResult = (CalculatePageActionCommand.CalcResult) session.getAttribute("calcResult");

        if (calcResult != null) {
            String languageCode = (String) session.getAttribute("locale");
            languageCode = languageCode != null ? languageCode : "en";
            if (!languageCode.equals(calcResult.getLanguageCode())) {
                try {
                    calcResult
                            .getCargoType()
                            .setName(cargoTypeService
                                    .findEntityByIdWithTranslatedName(calcResult
                                            .getCargoType()
                                            .getId(), languageCode)
                                    .getName());
                    calcResult.setLanguageCode(languageCode);
                } catch (ServiceException e) {
                    logger.error("Cannot change language in calculation result");
                    req.setAttribute("error", "Internal error. Please try again");
                    return new ActionCommandResult("error");
                }
            }
        }

        return new ActionCommandResult("calcResult");
    }
}
