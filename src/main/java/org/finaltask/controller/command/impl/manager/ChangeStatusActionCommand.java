package org.finaltask.controller.command.impl.manager;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.finaltask.controller.command.ActionCommand;
import org.finaltask.controller.command.ActionCommandResult;
import org.finaltask.entity.Status;
import org.finaltask.service.OrderService;
import org.finaltask.service.ServiceException;
import org.finaltask.service.impl.OrderServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ChangeStatusActionCommand implements ActionCommand {
    private static final Logger logger = LogManager.getLogger(ChangeStatusActionCommand.class);
    private final OrderService orderService = new OrderServiceImpl();

    @Override
    public ActionCommandResult execute(HttpServletRequest req, HttpServletResponse resp) {

        String whereName = req.getParameter("whereName");
        whereName = whereName != null ? whereName : "";

        String whereValue = req.getParameter("whereValue");
        whereValue = whereValue != null ? whereValue : "";

        String orderBy = req.getParameter("orderBy");
        orderBy = orderBy != null ? orderBy : "id";

        String sortOrder = req.getParameter("sortOrder");
        sortOrder = sortOrder != null ? sortOrder : "ASC";

        int recordsPerPage = 0;
        String value = req.getParameter("recordsPerPage");
        try {
            recordsPerPage = value != null ? Integer.parseInt(value) : 5;
        } catch (NumberFormatException e) {
            logger.error("Request parameter recordsPerPage not a number (value = " + value + ")", e);
            recordsPerPage = 5;
        }


        int currentPage = 0;
        value = req.getParameter("currentPage");
        try {
            currentPage = value != null ? Integer.parseInt(value) : 1;
        } catch (NumberFormatException e) {
            logger.error("Request parameter currentPage not a number (value = " + value + ")", e);
            currentPage = 1;
        }

        int orderId;
        value = req.getParameter("orderId");
        try {
            orderId = Integer.parseInt(value);
        } catch (NumberFormatException e) {
            logger.error("Request parameter orderId not a number (value = " + value + ")", e);
            req.setAttribute("error", "Wrong request parametrs");
            return new ActionCommandResult("error");
        }


        Status status;
        try {
            status = Status.valueOf(req.getParameter("status"));
        } catch (NullPointerException | IllegalArgumentException e) {
            logger.error("Request parameter status not a part of enum values.", e);
            req.setAttribute("error", "Wrong request parameters");
            return new ActionCommandResult("error");
        }


        try {
            orderService.updateStatus(orderId, status);
        } catch (ServiceException e) {
            logger.error("Cannot update order status", e);
            req.setAttribute("error", "DB error. Cannot update order status");
            return new ActionCommandResult("error");
        }

        return new ActionCommandResult("viewOrders"
                + "?whereName=" + whereName
                + "&whereValue=" + whereValue
                + "&orderBy=" + orderBy
                + "&sortOrder=" + sortOrder
                + "&recordsPerPage=" + recordsPerPage
                + "&currentPage=" + currentPage, true);
    }
}
