package org.finaltask.controller.command.impl.user;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.finaltask.controller.command.ActionCommand;
import org.finaltask.controller.command.ActionCommandResult;
import org.finaltask.entity.Account;
import org.finaltask.service.PayService;
import org.finaltask.service.PayServiceException;
import org.finaltask.service.ServiceException;
import org.finaltask.service.WalletService;
import org.finaltask.service.impl.PayServiceImpl;
import org.finaltask.service.impl.WalletServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class PayOrderActionCommand implements ActionCommand {
    private static final Logger logger = LogManager.getLogger(PayOrderActionCommand.class);
    @Override
    public ActionCommandResult execute(HttpServletRequest req, HttpServletResponse resp) {
        PayService payService = new PayServiceImpl();
        WalletService walletService = new WalletServiceImpl();
       
        int orderId;

        String value = req.getParameter("orderId");
        if (value == null) {
            logger.error("Error. Cannot get order id from request");
            req.setAttribute("error", "Not correct data");
            return new ActionCommandResult("error");
        }

        try {
            orderId = Integer.parseInt(value);
        } catch (NumberFormatException e) {
            logger.error("Order id value not a number (value = " + value + ")");
            req.setAttribute("error", "Payment was declined");
            return new ActionCommandResult("error");
        }

        try {            
            
            payService.payForOrder(orderId);
            
            HttpSession session = req.getSession(false);
                        
            if(session != null && session.getAttribute("account") != null) {
            	Account account = (Account)session.getAttribute("account");
            	account.setWallet(walletService.findEntityById(account.getWallet().getId()));
            }
                                   
            
        } catch (ServiceException e) {
            logger.error("Error while during payment", e);
            req.setAttribute("error", "Cannot connect to DB");
            return new ActionCommandResult("error");
        } catch (PayServiceException e) {
            logger.error("Error while during payment", e);
            req.setAttribute("error", e.getMessage());
            return new ActionCommandResult("error");
        } 
        
        return new ActionCommandResult("payOrderResult" + "?success=true", true);
    }
}
