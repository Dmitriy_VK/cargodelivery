package org.finaltask.controller.command.impl.common;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.finaltask.controller.command.ActionCommand;
import org.finaltask.controller.command.ActionCommandResult;
import org.finaltask.entity.Account;
import org.finaltask.service.AccountService;
import org.finaltask.service.ServiceException;
import org.finaltask.service.impl.AccountServiceImpl;
import org.finaltask.utils.PasswordSecure;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class LoginPageActionCommand implements ActionCommand {
    private static final Logger logger = LogManager.getLogger(LoginPageActionCommand.class);
    @Override
    public ActionCommandResult execute(HttpServletRequest req, HttpServletResponse resp) {
        AccountService accountService = new AccountServiceImpl();
        boolean errorInputData = false;

        String formData = req.getParameter("formData");
        if (formData == null || !formData.equals("formData")) {
            logger.info("Clean login form");
            return new ActionCommandResult("login");
        }
        
        String login = req.getParameter("userName");
        if (login == null) {
            logger.info("null login was received");
            req.setAttribute("error_userName", true);
            errorInputData = true;
        }
        
        String password = req.getParameter("password");
        if (password == null) {
            logger.info("null password was received");
            req.setAttribute("error_password", true);
            errorInputData = true;
        }
        
        if (errorInputData) {
            return new ActionCommandResult("login");
        }
        
        HttpSession session = req.getSession(false);
        Account account = null;
        
        if(session != null && session.getAttribute("account") != null) {
        	account = (Account)session.getAttribute("account");
        	logger.info("user is already authorized with login: " + account.getLogin());
            req.setAttribute("user_exist", true);
            return new ActionCommandResult("login");
        } else {
        	session = req.getSession(true);

        	try {
        		account = accountService.findEntityByLogin(login);

        		if (account != null) {
        			if (account.getPassword().equals(PasswordSecure.hashPassword(password))) {
        				session.setAttribute("account", account);
        			} else {
                        logger.info("user entered wrong password");
                        req.setAttribute("error_password", true);
                        return new ActionCommandResult("login");
                    }
        		} else{
                    logger.info("user entered wrong login");
                    req.setAttribute("error_userName", true);
                    return new ActionCommandResult("login");
                }
        		
            } catch (ServiceException e) {
                logger.error("Error while getting account from DB", e);
                req.setAttribute("error", "Cannot connect to DB");
                return new ActionCommandResult("error");
            }
        }
        
        return new ActionCommandResult("home", true);
    }
}
