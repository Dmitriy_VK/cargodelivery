package org.finaltask.controller.command.impl.common;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.finaltask.controller.command.ActionCommand;
import org.finaltask.controller.command.ActionCommandResult;
import org.finaltask.entity.Account;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class LogoutActionCommand implements ActionCommand {
    private static final Logger logger = LogManager.getLogger(LogoutActionCommand.class);
    @Override
    public ActionCommandResult execute(HttpServletRequest req, HttpServletResponse resp) {

        HttpSession session = req.getSession(false);

        if(session != null && session.getAttribute("account") != null) {
            Account account = (Account)session.getAttribute("account");
            String login = account.getLogin();
        	session.removeAttribute("account");
        	logger.info("user quite from app with login: " + login);
        }
        
        return new ActionCommandResult("home", true);
    }
}
