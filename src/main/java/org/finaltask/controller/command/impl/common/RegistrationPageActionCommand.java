package org.finaltask.controller.command.impl.common;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.finaltask.controller.command.ActionCommand;
import org.finaltask.controller.command.ActionCommandResult;
import org.finaltask.entity.*;
import org.finaltask.service.AccountService;
import org.finaltask.service.ServiceException;
import org.finaltask.service.impl.AccountServiceImpl;
import org.finaltask.utils.PasswordSecure;
import org.finaltask.utils.Validation;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class RegistrationPageActionCommand implements ActionCommand {
    private static final Logger logger = LogManager.getLogger(RegistrationPageActionCommand.class);
    private AccountService accountService = new AccountServiceImpl();

    @Override
    public ActionCommandResult execute(HttpServletRequest req, HttpServletResponse resp) {
        boolean errorInputData = false;

        String formData = req.getParameter("formData");
        if (formData == null || !formData.equals("formData")) {
            logger.info("Clean register form");
            return new ActionCommandResult("registration");
        }

        String firstName = req.getParameter("firstName");
        if (firstName == null || !Validation.validateName(firstName)) {
            logger.info("Invalid first name was received");
            req.setAttribute("error_firstName", true);
            errorInputData = true;
        }

        String lastName = req.getParameter("lastName");
        if (firstName == null || !Validation.validateName(lastName)) {
            logger.info("Invalid last name was received");
            req.setAttribute("error_lastName", true);
            errorInputData = true;
        }

        String userName = req.getParameter("username");
        if (userName == null || !Validation.validateLogin(userName)) {
            logger.info("Invalid user name was received");
            req.setAttribute("error_userName", true);
            errorInputData = true;
        } else {

            try {

                Account accountByLogin = accountService.findEntityByLogin(userName);

                if (accountByLogin != null) {
                    logger.info("There is a user with this name in the database");
                    req.setAttribute("error_userExist", true);
                    errorInputData = true;
                }

            } catch (ServiceException e) {
                logger.error("Error while getting account from DB", e);
                req.setAttribute("error", "Cannot connect to DB");
                return new ActionCommandResult("error");
            }
        }

        String password = req.getParameter("password");
        if (password == null || !Validation.validatePassword(password)) {
            logger.info("Invalid password was received");
            req.setAttribute("error_password", true);
            errorInputData = true;
        }

        String re_password = req.getParameter("re_pass");
        if (re_password == null || !re_password.equals(password)) {
            logger.info("Repeated password does not match");
            req.setAttribute("error_re_password", true);
            errorInputData = true;
        }

        String email = req.getParameter("email");
        if (email == null || !Validation.validateEmail(email)) {
            logger.info("Invalid email was received");
            req.setAttribute("error_email", true);
            errorInputData = true;
        }

        String phone = req.getParameter("phone");
        if (phone == null || !Validation.validatePhone(phone)) {
            logger.info("Invalid phone was received");
            req.setAttribute("error_phone", true);
            errorInputData = true;
        }

        String city = req.getParameter("city");
        if (city == null || !Validation.validateName(city)) {
            logger.info("Invalid city name was received");
            req.setAttribute("error_cityName", true);
            errorInputData = true;
        }

        RoleEnum role;
        try {
            role = RoleEnum.valueOf(req.getParameter("role").toUpperCase());
        } catch (NullPointerException | IllegalArgumentException e) {
            logger.info("Invalid role parameter", e);
            req.setAttribute("error", "Unexpected error");
            return new ActionCommandResult("error");
        }

        if (errorInputData) {
            return new ActionCommandResult("registration");
        }

        Account account = new Account();
        account.setRole(new Role());
        account.setWallet(new Wallet());
        account.setAccountDetails(new AccountDetails());


        account.setLogin(userName);
        account.setPassword(PasswordSecure.hashPassword(password));
        account.getRole().setName(role);
        account.getWallet().setValue(0d);
        account.getAccountDetails().setName(firstName);
        account.getAccountDetails().setSurname(lastName);
        account.getAccountDetails().setEmail(email);
        account.getAccountDetails().setPhone(phone);
        account.getAccountDetails().setCity(city);

        boolean regResult;

        try {

            regResult = accountService.createAccountWithDetailsAndWallet(account);

        } catch (ServiceException e) {
            logger.error("Error while creating account with details and wallet", e);
            req.setAttribute("error", "Cannot connect to DB");
            return new ActionCommandResult("error");
        }

        return new ActionCommandResult("displayAccount" + "?regResult=" + regResult, true);
    }
}
