package org.finaltask.controller.command.impl.user;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.finaltask.controller.command.ActionCommand;
import org.finaltask.controller.command.ActionCommandResult;
import org.finaltask.entity.*;
import org.finaltask.service.CargoTypeService;
import org.finaltask.service.OrderService;
import org.finaltask.service.ServiceException;
import org.finaltask.service.impl.CargoTypeServiceImpl;
import org.finaltask.service.impl.OrderServiceImpl;
import org.finaltask.utils.Cost;
import org.finaltask.utils.Validation;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.UnsupportedEncodingException;
import java.sql.Date;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.List;
import java.util.NoSuchElementException;

public class ProcessOrderActionCommand implements ActionCommand {
    private static final Logger logger = LogManager.getLogger(ProcessOrderActionCommand.class);
    private final OrderService orderService = new OrderServiceImpl();
    private final CargoTypeService cargoTypeService = new CargoTypeServiceImpl();

    @Override
    public ActionCommandResult execute(HttpServletRequest req, HttpServletResponse resp) {
        boolean errorInputData = false;

        String formData = req.getParameter("formData");
        if (!"formData".equals(formData)) {
            logger.info("Clean order form form");
            return new ActionCommandResult("makeOrderForm");
        }

        int cargoTypeId = 0;
        String value = req.getParameter("cargoTypeId");
        if (value == null) {
            logger.info("Invalid cargoTypeId was received");
            req.setAttribute("error_cargoType", true);
            errorInputData = true;
        } else {

            try {
                cargoTypeId = Integer.parseInt(value);
            } catch (NumberFormatException e) {
                logger.error("CargoTypeId value not a number (value = " + value + ")");
                req.setAttribute("error_cargoType", true);
                errorInputData = true;
            }
        }

        double weight = 0d;
        value = req.getParameter("weight");

        if (value == null || !Validation.validateWeight(value)) {
            logger.info("Invalid weight was received");
            req.setAttribute("error_weight", true);
            errorInputData = true;
        } else {

            try {
                weight = Double.parseDouble(value);
            } catch (NumberFormatException e) {
                logger.error("Weight value not a number (value = " + value + ")");
                req.setAttribute("error_weight", true);
                errorInputData = true;
            }
        }

        int volume = 0;
        value = req.getParameter("volume");

        if (value == null || !Validation.validateVolume(value)) {
            logger.info("Invalid volume was received");
            req.setAttribute("error_volume", true);
            errorInputData = true;
        } else {

            try {
                volume = Integer.parseInt(value);
            } catch (NumberFormatException e) {
                logger.error("Volume value not a number (value = " + value + ")");
                req.setAttribute("error_volume", true);
                errorInputData = true;
            }
        }

        String address = req.getParameter("address");
        if (address == null) {
            logger.info("null address was received");
            req.setAttribute("error_address", true);
            errorInputData = true;
        }

        String deliveryDate = req.getParameter("deliveryDate");
        LocalDate dateOfDelivery = null;
        if (deliveryDate == null || !Validation.validateDate(deliveryDate)) {
            logger.info("Invalid deliveryDate was received");
            req.setAttribute("error_deliveryDate", true);
            errorInputData = true;
        } else {
            try {
                DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd.MM.uuuu");
                dateOfDelivery = LocalDate.parse(deliveryDate, dateTimeFormatter);
            } catch (DateTimeParseException e) {
                logger.error("Error while parsing date", e);
                req.setAttribute("error", "Invalid data");
                return new ActionCommandResult("error");
            }

            if (dateOfDelivery.compareTo(LocalDate.now()) <= 0) {
                logger.info("Delivery date is before or equals today date");
                req.setAttribute("error_compareDate", true);
                errorInputData = true;
            }
        }

        if (errorInputData) {
            return new ActionCommandResult("makeOrderForm");
        }

        HttpSession session = req.getSession();
        Account account = (Account) session.getAttribute("account");
        Route route = (Route) session.getAttribute("route");


        if (account == null) {
            logger.error("No any account in session");
            req.setAttribute("error", "Please login");
            return new ActionCommandResult("error");
        }

        if (route == null) {
            logger.error("No any route in session");
            req.setAttribute("error", "WRONG DATA");
            return new ActionCommandResult("error");
        }

        CargoType cargoTypeClass;

        try {
            List<CargoType> cargoTypeList = (List<CargoType>) session.getAttribute("cargoTypes");
            final int cti = cargoTypeId;
            cargoTypeClass = cargoTypeList.stream().filter(ct -> ct.getId() == cti).findFirst().get();
        } catch (Exception e) {
            logger.error("Error while processing data", e);
            req.setAttribute("error", "Internal error. Please try again");
            return new ActionCommandResult("error");
        }

        Order order = new Order();
        order.setAccount(account);
        order.setAddress(address);
        order.setCargoType(cargoTypeClass);
        order.setWeight(weight);
        order.setVolume(volume);
        order.setDeliveryDate(Date.valueOf(dateOfDelivery));
        order.setOrderStatus(Status.NEW);
        order.setRoute(route);
        order.setPrice(Cost.calculate(weight, volume, route, cargoTypeClass.getRatio()));
        order.setAccount(account);

        boolean makeOrderResult;

        try {
            makeOrderResult = orderService.create(order);
        } catch (ServiceException e) {
            logger.error("Error while creating order", e);
            req.setAttribute("error", "Cannot connect to DB");
            return new ActionCommandResult("error");
        }

        return new ActionCommandResult("makeOrderResult" + "?orderResult=" + makeOrderResult, true);
    }
}
