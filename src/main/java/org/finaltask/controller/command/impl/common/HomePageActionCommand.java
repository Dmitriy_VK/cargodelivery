package org.finaltask.controller.command.impl.common;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.finaltask.controller.command.ActionCommand;
import org.finaltask.controller.command.ActionCommandResult;
import org.finaltask.entity.Route;
import org.finaltask.service.RouteService;
import org.finaltask.service.ServiceException;
import org.finaltask.service.impl.RouteServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class HomePageActionCommand implements ActionCommand {

    private static final Logger logger = LogManager.getLogger(HomePageActionCommand.class);
    private RouteService routeService = new RouteServiceImpl();

    @Override
    public ActionCommandResult execute(HttpServletRequest req, HttpServletResponse resp) {

        String orderBy = req.getParameter("orderBy");
        orderBy = orderBy != null ? orderBy : "city_from";

        String sortOrder = req.getParameter("sortOrder");
        sortOrder = sortOrder != null ? sortOrder : "ASC";

        String where = req.getParameter("where");
        where = where != null ? where : "";

        long cityId = 0;
        String value = req.getParameter("cityId");
        if (value != null) {
            try {
                cityId = Long.parseLong(value);
            } catch (NumberFormatException e) {
                logger.error("cityId value not a number (value = " + value + ")", e);
            }
        }

        int recordsPerPage;
        value = req.getParameter("recordsPerPage");
        try {
            recordsPerPage = value != null ? Integer.parseInt(value) : 5;
        } catch (NumberFormatException e) {
            logger.error("Limit value not a number (value = " + value + ")", e);
            recordsPerPage = 5;
        }

        int page;
        value = req.getParameter("page");
        try {
            page = value != null ? Integer.parseInt(value) : 1;
        } catch (NumberFormatException e) {
            logger.error("Offset value not a number (value = " + value + ")", e);
            page = 1;
        }

        int offset = page * recordsPerPage - recordsPerPage;

        try {

            int rows = routeService.getNumberOfRows(where, cityId);

            int numOfPages = rows != recordsPerPage ? rows / recordsPerPage : rows / recordsPerPage - 1;

            if (numOfPages % recordsPerPage > 0) {
                numOfPages++;
            }

            String locale = (String) req.getSession().getAttribute("locale");
            locale = locale != null ? locale : "en";

            List<Route> routes = routeService.findAllWithCityNames(locale, orderBy, sortOrder, where, cityId, recordsPerPage, offset);
            req.setAttribute("routes", routes);

            req.setAttribute("numOfPages", numOfPages);
            req.setAttribute("currentPage", page);
            req.setAttribute("recordsPerPage", recordsPerPage);
            req.setAttribute("orderBy", orderBy);
            req.setAttribute("sortOrder", sortOrder);
            req.setAttribute("where", where);
            req.setAttribute("cityId", cityId);

        } catch (ServiceException e) {
            logger.error("Error while getting routes from DB", e);
            req.setAttribute("error", "Cannot connect to DB");
            return new ActionCommandResult("error");
        }
        return new ActionCommandResult("home");
    }
}
