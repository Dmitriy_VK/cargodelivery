package org.finaltask.controller.command.impl;

import org.finaltask.controller.command.ActionCommand;
import org.finaltask.controller.command.ActionCommandResult;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ErrorPageActionCommand implements ActionCommand {
    @Override
    public ActionCommandResult execute(HttpServletRequest req, HttpServletResponse resp) {
        req.setAttribute("error", "Cannot connect to DB");
        return new ActionCommandResult("error");
    }
}
