package org.finaltask.controller.command.impl.user;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.finaltask.controller.command.ActionCommand;
import org.finaltask.controller.command.ActionCommandResult;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class PayOrderResultActionCommand implements ActionCommand {
    private static final Logger logger = LogManager.getLogger(PayOrderResultActionCommand.class);
    @Override
    public ActionCommandResult execute(HttpServletRequest req, HttpServletResponse resp) {
        
        String payOrderResult = req.getParameter("success");
        if (!"true".equals(payOrderResult)) {
        	logger.error("Error while paying order");
            req.setAttribute("error", "Invalid data");
            return new ActionCommandResult("error");
        }
        
        req.setAttribute("successfulPayment", payOrderResult);
        
        return new ActionCommandResult("payPage");
    }
}
