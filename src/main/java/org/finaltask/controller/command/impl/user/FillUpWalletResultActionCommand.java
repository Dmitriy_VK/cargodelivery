package org.finaltask.controller.command.impl.user;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.finaltask.controller.command.ActionCommand;
import org.finaltask.controller.command.ActionCommandResult;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class FillUpWalletResultActionCommand implements ActionCommand {
    private static final Logger logger = LogManager.getLogger( FillUpWalletResultActionCommand.class);
    @Override
    public ActionCommandResult execute(HttpServletRequest req, HttpServletResponse resp) {
        
        String fillUpWalletResult = req.getParameter("success");
        if (!"true".equals(fillUpWalletResult)) {
        	logger.error("Error while filling up user wallet");
            req.setAttribute("error", "Invalid data");
            return new ActionCommandResult("error");
        }

        String amountOfMoney = req.getParameter("amountOfMoney");
        if (amountOfMoney == null) {
            logger.error("Error. Cannot get amount of money from request");
            req.setAttribute("error_amountOfMoney", true);
            return new ActionCommandResult("error");
        }
        
        req.setAttribute("fillUpWalletResult", fillUpWalletResult);
        req.setAttribute("amountOfMoney", amountOfMoney);

        return new ActionCommandResult("fillUpWallet");
    }
}
