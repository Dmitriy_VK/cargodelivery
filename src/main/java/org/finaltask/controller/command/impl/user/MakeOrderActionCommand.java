package org.finaltask.controller.command.impl.user;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.finaltask.controller.command.ActionCommand;
import org.finaltask.controller.command.ActionCommandResult;
import org.finaltask.entity.CargoType;
import org.finaltask.entity.Route;
import org.finaltask.service.CargoTypeService;
import org.finaltask.service.RouteService;
import org.finaltask.service.ServiceException;
import org.finaltask.service.impl.CargoTypeServiceImpl;
import org.finaltask.service.impl.RouteServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

public class MakeOrderActionCommand implements ActionCommand {
    private static final Logger logger = LogManager.getLogger(MakeOrderActionCommand.class);
    private final RouteService routeService = new RouteServiceImpl();
    private final CargoTypeService cargoTypeService = new CargoTypeServiceImpl();

    @Override
    public ActionCommandResult execute(HttpServletRequest req, HttpServletResponse resp) {
        int routeId;

        String value = req.getParameter("routeId");
        if (value == null) {
            logger.error("Error. Cannot get route id from request");
            req.setAttribute("error", "Not correct data");
            return new ActionCommandResult("error");
        }

        try {
            routeId = Integer.parseInt(value);
        } catch (NumberFormatException e) {
            logger.error("Route id value not a number (value = " + value + ")");
            req.setAttribute("error", "Not correct data");
            return new ActionCommandResult("error");
        }

        HttpSession session = req.getSession(false);
        if (session == null) {
            logger.error("No session");
            req.setAttribute("error", "No account was founded. Please Sing in");
            return new ActionCommandResult("error");
        }

        List<CargoType> cargoTypes;
        try {
            String languageCode = (String) session.getAttribute("locale");
            languageCode = languageCode != null ? languageCode : "en";
            cargoTypes = cargoTypeService.findAllWithTranslatedNames(languageCode);
        } catch (ServiceException e) {
            logger.error("Error while getting route from DB", e);
            req.setAttribute("error", "Cannot connect to DB");
            return new ActionCommandResult("error");
        }

        Route route;
        try {
            route = routeService.findEntityById(routeId);
        } catch (ServiceException e) {
            logger.error("Error while getting route from DB", e);
            req.setAttribute("error", "Cannot connect to DB");
            return new ActionCommandResult("error");
        }

        session.setAttribute("route", route);
        session.setAttribute("cargoTypes", cargoTypes);

        return new ActionCommandResult("makeOrderForm");
    }
}
