package org.finaltask.controller.command.impl.user;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.finaltask.controller.command.ActionCommand;
import org.finaltask.controller.command.ActionCommandResult;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class MakeOrderResultActionCommand implements ActionCommand {
    private static final Logger logger = LogManager.getLogger( MakeOrderResultActionCommand.class);
    @Override
    public ActionCommandResult execute(HttpServletRequest req, HttpServletResponse resp) {
        

        String orderResult = req.getParameter("orderResult");
        if (orderResult == null) {
        	logger.error("Error while creating order");
            req.setAttribute("error", "Invalid data");
            return new ActionCommandResult("error");
        }
        
        req.setAttribute("orderResult", orderResult);
        req.getSession().removeAttribute("route");
        req.getSession().removeAttribute("cargoTypes");

        return new ActionCommandResult("makeOrderForm");
    }
}
