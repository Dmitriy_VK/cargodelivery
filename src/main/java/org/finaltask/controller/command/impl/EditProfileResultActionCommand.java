package org.finaltask.controller.command.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.finaltask.controller.command.ActionCommand;
import org.finaltask.controller.command.ActionCommandResult;
import org.finaltask.entity.Account;
import org.finaltask.service.AccountService;
import org.finaltask.service.ServiceException;
import org.finaltask.service.impl.AccountServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class EditProfileResultActionCommand implements ActionCommand {
    private static final Logger logger = LogManager.getLogger(EditProfileResultActionCommand.class);
    private final AccountService accountService = new AccountServiceImpl();
    @Override
    public ActionCommandResult execute(HttpServletRequest req, HttpServletResponse resp) {
        
    	HttpSession session = req.getSession();
		Account accountInSession;
		
		if (session != null && session.getAttribute("account") != null) {
			accountInSession = (Account) session.getAttribute("account");
			try {
				Account account = accountService.findAccountProfileById(accountInSession.getId());
				req.setAttribute("accountProfile", account);
				session.setAttribute("account", account);
			} catch (ServiceException e) {
				logger.error("Error while getting account with details and wallet from DB", e);
				req.setAttribute("error", "Cannot connect to DB");
				return new ActionCommandResult("error");
			}
		} else {
			logger.error("No session or account in session");
			req.setAttribute("error", "Please login");
			return new ActionCommandResult("error");
		}
        
        return new ActionCommandResult("viewProfile");
    }
}
