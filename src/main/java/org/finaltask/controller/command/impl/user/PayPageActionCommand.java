package org.finaltask.controller.command.impl.user;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.finaltask.controller.command.ActionCommand;
import org.finaltask.controller.command.ActionCommandResult;
import org.finaltask.entity.Order;
import org.finaltask.service.OrderService;
import org.finaltask.service.ServiceException;
import org.finaltask.service.impl.OrderServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class PayPageActionCommand implements ActionCommand {
    private static final Logger logger = LogManager.getLogger(PayPageActionCommand.class);
    @Override
    public ActionCommandResult execute(HttpServletRequest req, HttpServletResponse resp) {
        OrderService orderService = new OrderServiceImpl();
        int orderId;

        String value = req.getParameter("orderId");
        if (value == null) {
            logger.error("Error. Cannot get order id from request");
            req.setAttribute("error", "Not correct data");
            return new ActionCommandResult("error");
        }

        try {
            orderId = Integer.parseInt(value);
        } catch (NumberFormatException e) {
            logger.error("Order id value not a number (value = " + value + ")");
            req.setAttribute("error", "Not correct data");
            return new ActionCommandResult("error");
        }

        try {
            Order order = orderService.findEntityByIdWithRouteAndAccount(orderId);
            req.setAttribute("order", order);
        } catch (ServiceException e) {
            logger.error("Error while getting order from DB", e);
            req.setAttribute("error", "Cannot connect to DB");
            return new ActionCommandResult("error");
        }
        return new ActionCommandResult("payPage");
    }
}
