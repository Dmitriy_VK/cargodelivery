package org.finaltask.controller.command.impl.manager;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.finaltask.controller.command.ActionCommand;
import org.finaltask.controller.command.ActionCommandResult;
import org.finaltask.entity.City;
import org.finaltask.service.CityService;
import org.finaltask.service.ServiceException;
import org.finaltask.service.impl.CityServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class AddRouteFormActionCommand implements ActionCommand {
    private static final Logger logger = LogManager.getLogger(AddRouteFormActionCommand.class);
    private final CityService cityService = new CityServiceImpl();

    @Override
    public ActionCommandResult execute(HttpServletRequest req, HttpServletResponse resp) {

        try {
            List<City> cityList = cityService.findAll();
            req.setAttribute("cityList", cityList);
        } catch (ServiceException e) {
            logger.error("Cannot get all cities from DB", e);
            req.setAttribute("error", "DB error. Cannot find data in DB");
            return new ActionCommandResult("error");
        }

        return new ActionCommandResult("addRouteForm");
    }
}
