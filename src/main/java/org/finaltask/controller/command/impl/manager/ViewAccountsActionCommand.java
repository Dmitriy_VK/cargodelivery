package org.finaltask.controller.command.impl.manager;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.finaltask.controller.command.ActionCommand;
import org.finaltask.controller.command.ActionCommandResult;
import org.finaltask.entity.Account;
import org.finaltask.entity.RoleEnum;
import org.finaltask.service.AccountService;
import org.finaltask.service.ServiceException;
import org.finaltask.service.impl.AccountServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;
import java.util.List;

public class ViewAccountsActionCommand implements ActionCommand {
    private static final Logger logger = LogManager.getLogger(ViewAccountsActionCommand.class);
    private final AccountService accountService = new AccountServiceImpl();

    @Override
    public ActionCommandResult execute(HttpServletRequest req, HttpServletResponse resp) {

        RoleEnum role;
        try {
            role = RoleEnum.valueOf(req.getParameter("role").toUpperCase());
        } catch (NullPointerException | IllegalArgumentException e){
            logger.error("Invalid request parameter role", e);
            req.setAttribute("error", "Internal error. try again");
            return new ActionCommandResult("error");
        }

        String orderBy = req.getParameter("orderBy");
        orderBy = orderBy != null ? orderBy : "id";

        String sortOrder = req.getParameter("sortOrder");
        sortOrder = sortOrder != null ? sortOrder : "ASC";

        int recordsPerPage = 0;
        String value = req.getParameter("recordsPerPage");
        try {
            recordsPerPage = value != null ? Integer.parseInt(value) : 5;
        } catch (NumberFormatException e) {
            logger.error("Limit value not a number (value = " + value + ")", e);
            recordsPerPage = 5;
        }

        int currentPage = 0;
        value = req.getParameter("currentPage");
        try {
            currentPage = value != null ? Integer.parseInt(value) : 1;
        } catch (NumberFormatException e) {
            logger.error("Offset value not a number (value = " + value + ")", e);
            currentPage = 1;
        }

        int offset = currentPage * recordsPerPage - recordsPerPage;

        try {

            int rows = accountService.getNumberOfRowsByRole(role);

            int numOfPages = rows != recordsPerPage ? rows / recordsPerPage : rows / recordsPerPage - 1;

            if (numOfPages % recordsPerPage > 0) {
                numOfPages++;
            }

            List<Account> accountList = accountService.findAllByRoleWithAccountDetails(
                    role,
                    orderBy,
                    sortOrder,
                    recordsPerPage,
                    offset
            );

            System.out.println("rows: " + rows);////////

            req.setAttribute("accountList", accountList);
            req.setAttribute("numOfPages", numOfPages);
            req.setAttribute("currentPage", currentPage);
            req.setAttribute("recordsPerPage", recordsPerPage);
            req.setAttribute("orderBy", orderBy);
            req.setAttribute("sortOrder", sortOrder);
            req.setAttribute("role", role.name());

        } catch (ServiceException e) {
            logger.error("Error while getting orders from DB", e);
            req.setAttribute("error", "Cannot connect to DB");
            return new ActionCommandResult("error");
        }

        return new ActionCommandResult("viewAccounts");
    }
}
