package org.finaltask.controller.command.impl.common;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.finaltask.controller.command.ActionCommand;
import org.finaltask.controller.command.ActionCommandResult;
import org.finaltask.entity.CargoType;
import org.finaltask.entity.Route;
import org.finaltask.service.CargoTypeService;
import org.finaltask.service.RouteService;
import org.finaltask.service.ServiceException;
import org.finaltask.service.impl.CargoTypeServiceImpl;
import org.finaltask.service.impl.RouteServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

public class CalculateFormPageActionCommand implements ActionCommand {
    private static final Logger logger = LogManager.getLogger(CalculateFormPageActionCommand.class);
    private final RouteService routeService = new RouteServiceImpl();
    private final CargoTypeService cargoTypeService = new CargoTypeServiceImpl();

    @Override
    public ActionCommandResult execute(HttpServletRequest req, HttpServletResponse resp) {
        int routeId = 0;
        String value = req.getParameter("id");

        if (value == null) {
            logger.error("Error. Cannot get route id from request");
            req.setAttribute("error", "Not correct data");
            return new ActionCommandResult("error");
        }

        try {
            routeId = Integer.parseInt(value);
        } catch (NumberFormatException e) {
            logger.error("Route id value not a number (value = " + value + ")");
            req.setAttribute("error", "Not correct data");
            return new ActionCommandResult("error");
        }

        try {
            HttpSession session = req.getSession();
            String locale = (String) session.getAttribute("locale");
            locale = locale != null ? locale : "en";
            List<CargoType> cargoTypeList = cargoTypeService.findAllWithTranslatedNames(locale);
            Route route = routeService.findEntityById(routeId);

            session.removeAttribute("calcResult");
            session.setAttribute("routeCalcPage", route);
            session.setAttribute("cargoTypeList", cargoTypeList);

        } catch (ServiceException e) {
            logger.error("Error while getting data from DB", e);
            req.setAttribute("error", "Cannot connect to DB");
            return new ActionCommandResult("error");
        }
        return new ActionCommandResult("calcForm");
    }
}
