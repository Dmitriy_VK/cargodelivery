package org.finaltask.controller.command.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.finaltask.controller.command.ActionCommand;
import org.finaltask.controller.command.ActionCommandResult;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class DisplayAccountActionCommand implements ActionCommand {
    private static final Logger logger = LogManager.getLogger(DisplayAccountActionCommand.class);
    @Override
    public ActionCommandResult execute(HttpServletRequest req, HttpServletResponse resp) {
        
        String regResult = req.getParameter("regResult");
        if (regResult == null) {
        	logger.error("Error while creating account with details and wallet");
            req.setAttribute("error", "Invalid data");
            return new ActionCommandResult("error");
        }
        
        req.setAttribute("regResult", regResult);
        
        return new ActionCommandResult("registration");
    }
}
