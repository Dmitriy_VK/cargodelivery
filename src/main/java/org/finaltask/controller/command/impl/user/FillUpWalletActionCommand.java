package org.finaltask.controller.command.impl.user;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.finaltask.controller.command.ActionCommand;
import org.finaltask.controller.command.ActionCommandResult;
import org.finaltask.entity.Account;
import org.finaltask.service.ServiceException;
import org.finaltask.service.WalletService;
import org.finaltask.service.impl.WalletServiceImpl;
import org.finaltask.utils.Validation;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.math.BigDecimal;
import java.math.RoundingMode;

public class FillUpWalletActionCommand implements ActionCommand {
    private static final Logger logger = LogManager.getLogger(FillUpWalletActionCommand.class);
    private final WalletService walletService = new WalletServiceImpl();

    @Override
    public ActionCommandResult execute(HttpServletRequest req, HttpServletResponse resp) {
        boolean errorInputData = false;
        BigDecimal amountOfMoney = null;

        String formData = req.getParameter("formData");
        if (formData == null || !"formData".equals(formData)) {
            logger.info("Clean fill up wallet form form");
            return new ActionCommandResult("fillUpWallet");
        }

        String value = req.getParameter("amount");
        if (value == null || !Validation.validateMoney(value)) {
            logger.error("Error. Cannot get amount of money from request");
            req.setAttribute("error_amountOfMoney", true);
            errorInputData = true;
        } else {

            try {
                amountOfMoney = new BigDecimal(value).setScale(2, RoundingMode.CEILING);
            } catch (NumberFormatException e) {
                logger.error("Order id value not a number (value = " + value + ")");
                req.setAttribute("error_amountOfMoney", true);
                errorInputData = true;
            }
        }

        if (errorInputData) {
            return new ActionCommandResult("fillUpWallet");
        }

        HttpSession session = req.getSession(false);

        if (session != null && session.getAttribute("account") != null) {
            Account account = (Account) session.getAttribute("account");
            try {
                walletService.fillUpWallet(account.getWallet().getId(), amountOfMoney);
                account.setWallet(walletService.findEntityById(account.getWallet().getId()));
            } catch (ServiceException e) {
                logger.error("Error while filing up wallet", e);
                req.setAttribute("error", "Cannot connect to DB");
                return new ActionCommandResult("error");
            }
        } else {
            logger.info("User is not logged in");
            req.setAttribute("error", "User is not logged in");
            return new ActionCommandResult("error");
        }

        return new ActionCommandResult("fillUpWalletResult" + "?success=true&amountOfMoney=" + amountOfMoney, true);
    }
}
