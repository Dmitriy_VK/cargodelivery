package org.finaltask.controller.command.impl.common;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.finaltask.controller.command.ActionCommand;
import org.finaltask.controller.command.ActionCommandResult;
import org.finaltask.dao.impl.AccountDaoImpl;
import org.finaltask.dao.impl.AccountDetailsDaoImpl;
import org.finaltask.entity.*;
import org.finaltask.service.AccountService;
import org.finaltask.service.ServiceException;
import org.finaltask.service.impl.AccountServiceImpl;
import org.finaltask.utils.PasswordSecure;
import org.finaltask.utils.Validation;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;

public class EditProfileActionCommand implements ActionCommand {
    private static final Logger logger = LogManager.getLogger(EditProfileActionCommand.class);
    private final AccountService accountService = new AccountServiceImpl();

    @Override
    public ActionCommandResult execute(HttpServletRequest req, HttpServletResponse resp) {
        boolean errorInputData = false;

        String formData = req.getParameter("formData");
        if (formData == null || !formData.equals("formData")) {
            logger.info("Clean register form");
            return new ActionCommandResult("registration");
        }

        String firstName = req.getParameter("firstName");
        if (firstName == null || !Validation.validateName(firstName)) {
            logger.info("Invalid first name was received");
            req.setAttribute("error_firstName", true);
            errorInputData = true;
        }

        String lastName = req.getParameter("lastName");
        if (firstName == null || !Validation.validateName(lastName)) {
            logger.info("Invalid last name was received");
            req.setAttribute("error_lastName", true);
            errorInputData = true;
        }

        String userName = req.getParameter("username");
        if (userName == null || !Validation.validateLogin(userName)) {
            logger.info("Invalid user name was received");
            req.setAttribute("error_userName", true);
            errorInputData = true;
        }

        String password = req.getParameter("password");
        if (password != null) {
            if (!password.isEmpty()) {
                if (!Validation.validatePassword(password)) {
                    logger.info("Invalid password was received");
                    req.setAttribute("error_password", true);
                    errorInputData = true;
                }
            }
        } else {
            logger.info("Invalid password was received");
            req.setAttribute("error_password", true);
            errorInputData = true;
        }

        String re_password = req.getParameter("re_pass");
        if (re_password == null || !re_password.equals(password)) {
            logger.info("Repeated password does not match");
            req.setAttribute("error_re_password", true);
            errorInputData = true;
        }

        String email = req.getParameter("email");
        if (email == null || !Validation.validateEmail(email)) {
            logger.info("Invalid email was received");
            req.setAttribute("error_email", true);
            errorInputData = true;
        }

        String phone = req.getParameter("phone");
        if (email == null || !Validation.validatePhone(phone)) {
            logger.info("Invalid phone was received");
            req.setAttribute("error_phone", true);
            errorInputData = true;
        }

        String city = req.getParameter("city");
        if (city == null || !Validation.validateName(city)) {
            logger.info("Invalid city name was received");
            req.setAttribute("error_cityName", true);
            errorInputData = true;
        }

//		String role = req.getParameter("role");
//		if (role == null) {
//			logger.info("Role parameter is null");
//			req.setAttribute("error", "Unexpected error");
//			return new ActionCommandResult("error");
//		}

        if (errorInputData) {
        	HttpSession session = req.getSession();
    		
    		if (session != null && session.getAttribute("account") != null) {
    			Account accountInSession = (Account) session.getAttribute("account");
    			try {
    				Account accountProfile = accountService.findAccountProfileById(accountInSession.getId());
    				req.setAttribute("accountProfile", accountProfile);
    			} catch (ServiceException e) {
    				logger.error("Error while getting account with details and wallet from DB", e);
    				req.setAttribute("error", "Cannot connect to DB");
    				return new ActionCommandResult("error");
    			}
    		} else {
    			logger.error("No session or account in session");
    			req.setAttribute("error", "Please login");
    			return new ActionCommandResult("error");
    		}
            return new ActionCommandResult("editProfile");
        }

        Account editAccount = new Account();
        editAccount.setRole(new Role());
        editAccount.setWallet(new Wallet());
        editAccount.setAccountDetails(new AccountDetails());

        editAccount.setLogin(userName);
        if (!password.isEmpty()) {
            editAccount.setPassword(PasswordSecure.hashPassword(password));
        }
        //editAccount.getRole().setName(RoleEnum.valueOf(role.toUpperCase()));
        // editAccount.getWallet().setValue(0d);
        editAccount.getAccountDetails().setName(firstName);
        editAccount.getAccountDetails().setSurname(lastName);
        editAccount.getAccountDetails().setEmail(email);
        editAccount.getAccountDetails().setPhone(phone);
        editAccount.getAccountDetails().setCity(city);

        HttpSession session = req.getSession();

        if (session != null && session.getAttribute("account") != null) {
            Account accountInSession = (Account) session.getAttribute("account");
            try {
                Account accountDB = accountService.findAccountProfileById(accountInSession.getId());
                Map<String, String> fieldsForUpdate = compareAccounts(editAccount, accountDB);
                if (!fieldsForUpdate.isEmpty()) {
                    accountService.updateFields(fieldsForUpdate, accountDB.getId());
                    return new ActionCommandResult("editProfileResult", true);
                }
            } catch (ServiceException e) {
                logger.error("Error while getting account with details and wallet from DB", e);
                req.setAttribute("error", "Cannot connect to DB");
                return new ActionCommandResult("error", true);
            }
        } else {
            logger.error("No session or account in session");
            req.setAttribute("error", "Please login");
            return new ActionCommandResult("error", true);
        }

        return new ActionCommandResult("editProfileResult", true);
    }

    private Map<String, String> compareAccounts(Account editAccount, Account accountDB) {
        Map<String, String> fieldsForUpdate = new HashMap<>();

        if (!accountDB.getLogin().equals(editAccount.getLogin())) {
            fieldsForUpdate.put(AccountDaoImpl.LOGIN, editAccount.getLogin());
        }

        if (editAccount.getPassword() != null && !accountDB.getPassword().equals(editAccount.getPassword())) {
            fieldsForUpdate.put(AccountDaoImpl.PASSWORD, editAccount.getPassword());
        }

        if (!accountDB.getAccountDetails().getName().equals(editAccount.getAccountDetails().getName())) {
            fieldsForUpdate.put(AccountDetailsDaoImpl.NAME, editAccount.getAccountDetails().getName());
        }

        if (!accountDB.getAccountDetails().getSurname().equals(editAccount.getAccountDetails().getSurname())) {
            fieldsForUpdate.put(AccountDetailsDaoImpl.SURNAME, editAccount.getAccountDetails().getSurname());
        }

        if (!accountDB.getAccountDetails().getEmail().equals(editAccount.getAccountDetails().getEmail())) {
            fieldsForUpdate.put(AccountDetailsDaoImpl.EMAIL, editAccount.getAccountDetails().getEmail());
        }

        if (!accountDB.getAccountDetails().getPhone().equals(editAccount.getAccountDetails().getPhone())) {
            fieldsForUpdate.put(AccountDetailsDaoImpl.PHONE, editAccount.getAccountDetails().getPhone());
        }

        if (!accountDB.getAccountDetails().getCity().equals(editAccount.getAccountDetails().getCity())) {
            fieldsForUpdate.put(AccountDetailsDaoImpl.CITY, editAccount.getAccountDetails().getCity());
        }

        return fieldsForUpdate;
    }

}
