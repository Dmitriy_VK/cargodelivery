package org.finaltask.controller.filter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.finaltask.entity.Account;
import org.finaltask.entity.RoleEnum;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@WebFilter("/*")
public class AuthorizationFilter implements Filter {
    private static final Logger logger = LogManager.getLogger(AuthorizationFilter.class);
    private Map<RoleEnum, List<String>> rolePaths;
    private List<String> noControlPaths;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        Filter.super.init(filterConfig);

        rolePaths = new HashMap<>();
        rolePaths.put(RoleEnum.USER, Arrays.asList(
                "/viewProfile",
                "/editProfile",
                "/editProfileResult",
                "/makeOrder",
                "/processOrder",
                "/makeOrderResult",
                "/viewOrders",
                "/payPage",
                "/payOrder",
                "/payOrderResult",
                "/fillUpWalletPage",
                "/fillUpWallet",
                "/fillUpWalletResult")
        );
        rolePaths.put(RoleEnum.MANAGER, Arrays.asList(
                "/viewProfile",
                "/editProfile",
                "/editProfileResult",
                "/viewOrders",
                "/changeStatus",
                "/viewDateReport",
                "/viewCityReport",
                "/viewAccounts",
                "/addRoute",
                "/addRouteProcess",
                "/addRouteResult")
        );

        logger.info("AuthorizationFilter init");
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;

        if (isAccessed(req)) {
            chain.doFilter(request, response);
        } else {
            request.setAttribute("errorAccess", true);

            logger.info("User does not have permission to access the requested resource");

            request.getRequestDispatcher("WEB-INF/jsp/error.jsp").forward(request, response);
        }
    }

    @Override
    public void destroy() {
        Filter.super.destroy();

        logger.info("AuthorizationFilter destroy");
    }

    private boolean isAccessed(HttpServletRequest req) {
        String servletPath = req.getServletPath();
        HttpSession session = req.getSession();

        if (rolePaths.get(RoleEnum.MANAGER).contains(servletPath)) {
            if(session != null && session.getAttribute("account") != null) {
                Account account = (Account) session.getAttribute("account");
                if (account.getRole().getName() == RoleEnum.MANAGER) {
                    return true;
                }
            }
            return false;
        }

        if (rolePaths.get(RoleEnum.USER).contains(servletPath)) {
            if(session != null && session.getAttribute("account") != null) {
                Account account = (Account) session.getAttribute("account");
                if (account.getRole().getName() == RoleEnum.USER) {
                    return true;
                }
            }
            return false;
        }

        return true;
    }
}
