package org.finaltask.controller.listener;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.finaltask.connectionpool.ConnectionPool;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import java.util.Properties;
import java.util.ResourceBundle;

@WebListener
public class ContextListener implements ServletContextListener {

    private static final Logger logger = LogManager.getLogger(ContextListener.class);
    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        ResourceBundle dbProperties = ResourceBundle.getBundle("db.database");

        Properties properties = new Properties();
        properties.put("user", dbProperties.getString("user"));
        properties.put("password", dbProperties.getString("password"));

        String dbDriver = dbProperties.getString("driver");
        String dbURL = dbProperties.getString("URL");

        ConnectionPool.INSTANCE.init(dbDriver, dbURL, properties);
        logger.info("Init listener");
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        ConnectionPool.INSTANCE.destroyPool();
        logger.info("Destroy listener");
    }
}
