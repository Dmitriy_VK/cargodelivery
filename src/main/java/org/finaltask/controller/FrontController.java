package org.finaltask.controller;


import org.finaltask.controller.command.ActionCommand;
import org.finaltask.controller.command.ActionCommandResult;
import org.finaltask.controller.command.factory.ActionCommandFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = {
        "/home",
        "/login",
        "/logout",
        "/registration",
        "/calculateForm",
        "/calculate",
        "/calculateResult",
        "/displayAccount",
        "/viewProfile",
        "/viewAccounts",
        "/editProfile",
        "/editProfileResult",
        "/makeOrder",
        "/processOrder",
        "/makeOrderResult",
        "/viewOrders",
        "/payPage",
        "/payOrder",
        "/payOrderResult",
        "/fillUpWalletPage",
        "/fillUpWallet",
        "/fillUpWalletResult",
        "/changeStatus",
        "/viewDateReport",
        "/viewCityReport",
        "/addRoute",
        "/addRouteProcess",
        "/addRouteResult"
})
public class FrontController extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    private void processRequest(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ActionCommand command = ActionCommandFactory.getActionCommand(req);

        ActionCommandResult result = command.execute(req, resp);

        if (result.getPath() != null) {
            if (result.isRedirect()) {
                resp.sendRedirect(req.getContextPath() + "/" + result.getPath());
            } else {
                req.getRequestDispatcher("WEB-INF/jsp/" + result.getPath() + ".jsp").forward(req, resp);
            }
        } else {
            resp.sendRedirect(req.getContextPath());
        }
    }
}
