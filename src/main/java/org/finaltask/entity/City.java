package org.finaltask.entity;

public class City extends Entity {
	private String name;

	public City() {
	}

	public City(String name) {
		this.name = name;
	}
	public City(long id) {
		super(id);
	}

	public City(long id, String name) {
		super(id);
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
