package org.finaltask.entity;

public class Wallet extends Entity {
	private double value;

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}
}
