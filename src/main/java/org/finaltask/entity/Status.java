package org.finaltask.entity;

public enum Status {
    NEW, PROCESSED, PAID, DELIVERED;
}
