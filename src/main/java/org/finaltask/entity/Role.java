package org.finaltask.entity;

public class Role extends Entity {
	private RoleEnum name;

	public Role() {
	}

	public Role(RoleEnum name) {
		this.name = name;
	}

	public RoleEnum getName() {
		return name;
	}

	public void setName(RoleEnum name) {
		this.name = name;
	}
}
