package org.finaltask.utils;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class ValidationTest {

	@ParameterizedTest
	@ValueSource(strings = {"rtz.com", "@fr.yu","fgt@rth","ong()*@gmail.com","mong@gmail.com.1a"})
    void testValidateEmailFalse(String email) {
		assertFalse(Validation.validateEmail(email));
    }
	
	@ParameterizedTest
	@ValueSource(strings = {"d_fg@rtz.com", "gh-j123@fr.yu","1fg3TT@rth.ua"})
    void testValidateEmailTrue(String email) {
		assertTrue(Validation.validateEmail(email));
    }

	@ParameterizedTest
	@ValueSource(strings = {"12", "hjgfp7","_moreThenTwentyCharacters"})
    void testValidatePasswordFalse(String password) {
		assertFalse(Validation.validatePassword(password));
    }
	
	@ParameterizedTest
	@ValueSource(strings = {"12345678", "hjgfLp7O","_kjkb5LL"})
    void testValidatePasswordTrue(String password) {
		assertTrue(Validation.validatePassword(password));
    }

	@ParameterizedTest
	@ValueSource(strings = {"k", "7","_k","moreThenTwentyCharacters"})
    void testValidateNameFalse(String name) {
		assertFalse(Validation.validateName(name));
    }
	
	@ParameterizedTest
	@ValueSource(strings = {"kp", "hPk","kfd","Дніпро"})
    void testValidateNameTrue(String name) {
		assertTrue(Validation.validateName(name));
    }

	@ParameterizedTest
	@ValueSource(strings = {"?hj", "","_k","more+more"})
    void testValidateLoginFalse(String login) {
		assertFalse(Validation.validateLogin(login));
    }
	
	@ParameterizedTest
	@ValueSource(strings = {"Login", "Login12","Log-in","log_ji"})
    void testValidateLoginTrue(String login) {
		assertTrue(Validation.validateLogin(login));
    }

	@ParameterizedTest
	@ValueSource(strings = {"9", "+3","+380502","0502235623123",""})
    void testValidatePhoneFalse(String phone) {
		assertFalse(Validation.validatePhone(phone));
    }
	
	@ParameterizedTest
	@ValueSource(strings = {"+380502235642","380502235642"})
    void testValidatePhoneTrue(String phone) {
		assertTrue(Validation.validatePhone(phone));
    }

	@ParameterizedTest
	@ValueSource(strings = {"-9", "+3","3.256",".225"})
    void testValidateMoneyFalse(String money) {
		assertFalse(Validation.validateMoney(money));
    }
	
	@ParameterizedTest
	@ValueSource(strings = {"9.23", "3","3256.25","0.2",".36"})
    void testValidateMoneyTrue(String money) {
		assertTrue(Validation.validateMoney(money));
    }

	@ParameterizedTest
	@ValueSource(strings = {"1.02.2023","01.2.2023","01.02.20", "01:02:2023","42.11.2023","12.15.2023"})
    void testValidateDateFalse(String date) {
		assertFalse(Validation.validateDate(date));
    }
	
	@ParameterizedTest
	@ValueSource(strings = {"01.02.2023"})
    void testValidateDateTrue(String date) {
		assertTrue(Validation.validateDate(date));
    }

	@ParameterizedTest
	@ValueSource(strings = {"-9", "+3","0"})
    void testValidateWeightFalse(String weight) {
		assertFalse(Validation.validateWeight(weight));
    }
	
	@ParameterizedTest
	@ValueSource(strings = {"9", "3256","0.2","0.2256"})
    void testValidateWeightTrue(String weight) {
		assertTrue(Validation.validateWeight(weight));
    }

	@ParameterizedTest
	@ValueSource(strings = {"-9", "+3","0",".2","0.25"})
    void testValidateVolumeFalse(String volume) {
		assertFalse(Validation.validateVolume(volume));
    }
	
	@ParameterizedTest
	@ValueSource(strings = {"9", "23"})
    void testValidateVolumeTrue(String volume) {
		assertTrue(Validation.validateVolume(volume));
    }
}