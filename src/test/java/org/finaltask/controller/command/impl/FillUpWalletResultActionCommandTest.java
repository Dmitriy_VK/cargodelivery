package org.finaltask.controller.command.impl;

import org.finaltask.controller.command.ActionCommandResult;
import org.finaltask.controller.command.impl.user.FillUpWalletResultActionCommand;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class FillUpWalletResultActionCommandTest  {
    HttpServletResponse response;
    HttpServletRequest request;
    FillUpWalletResultActionCommand fillUpWalletResultActionCommand;

    @BeforeEach
    void init() {
        response = mock(HttpServletResponse.class);
        request = mock(HttpServletRequest.class);
        fillUpWalletResultActionCommand = new FillUpWalletResultActionCommand();
    }

    @Test
    void testFillUpWalletResultActionCommandRightParameters() {
        when(request.getParameter("success")).thenReturn("true");
        when(request.getParameter("amountOfMoney")).thenReturn("500");

        ActionCommandResult result = fillUpWalletResultActionCommand.execute(request, response);

        assertEquals("fillUpWallet", result.getPath());
        assertFalse(result.isRedirect());
    }
    
    @Test
    void testFillUpWalletResultActionCommandNullParameterSuccess() {
        when(request.getParameter("amountOfMoney")).thenReturn("500");

        ActionCommandResult result = fillUpWalletResultActionCommand.execute(request, response);

        assertEquals("error", result.getPath());
        assertFalse(result.isRedirect());
    }
    
    @Test
    void testFillUpWalletResultActionCommandWrongParameterSuccess() {
    	when(request.getParameter("success")).thenReturn("tthf");
        when(request.getParameter("amountOfMoney")).thenReturn("500");

        ActionCommandResult result = fillUpWalletResultActionCommand.execute(request, response);

        assertEquals("error", result.getPath());
        assertFalse(result.isRedirect());
    }
    
    @Test
    void testFillUpWalletResultActionCommandNullParameterAmountOfMoney() {
        when(request.getParameter("success")).thenReturn("true");

        ActionCommandResult result = fillUpWalletResultActionCommand.execute(request, response);

        assertEquals("error", result.getPath());
        assertFalse(result.isRedirect());
    }

    @AfterEach
    void tearDown() {
        response = null;
        request = null;        
        fillUpWalletResultActionCommand = null;
    }
}