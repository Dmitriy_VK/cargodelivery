package org.finaltask.controller.command.impl;

import org.finaltask.controller.command.ActionCommandResult;
import org.finaltask.controller.command.impl.manager.ChangeStatusActionCommand;
import org.finaltask.entity.Status;
import org.finaltask.service.OrderService;
import org.finaltask.service.ServiceException;
import org.finaltask.service.impl.OrderServiceImpl;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.powermock.reflect.Whitebox;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.mockito.Mockito.*;

class ChangeStatusActionCommandTest {
    HttpServletResponse response;
    HttpServletRequest request;
    ChangeStatusActionCommand changeStatusActionCommand;
    OrderService orderService;

    @BeforeEach
    void init() {
        response = mock(HttpServletResponse.class);
        request = mock(HttpServletRequest.class);
        changeStatusActionCommand = new ChangeStatusActionCommand();
        orderService = mock(OrderServiceImpl.class);
        Whitebox.setInternalState(changeStatusActionCommand, "orderService", orderService);
    }

    @Test
    void testChangeStatusActionCommandRightParameters() {
    	String whereName = "city_to";
        String whereValue = "Дніпро";
        String orderBy = "id";
        String sortOrder = "ASC";
        String recordsPerPage = "10";
        String currentPage = "1";
        
        when(request.getParameter("whereName")).thenReturn(whereName);
        when(request.getParameter("whereValue")).thenReturn(whereValue);
        when(request.getParameter("orderBy")).thenReturn(orderBy);
        when(request.getParameter("sortOrder")).thenReturn(sortOrder);
        when(request.getParameter("recordsPerPage")).thenReturn(recordsPerPage);
        when(request.getParameter("currentPage")).thenReturn(currentPage);
        
        when(request.getParameter("orderId")).thenReturn("12");
        when(request.getParameter("status")).thenReturn("NEW");

        ActionCommandResult result = changeStatusActionCommand.execute(request, response);

        assertEquals("viewOrders" 
        		+ "?whereName=" + whereName
                + "&whereValue=" + whereValue
                + "&orderBy=" + orderBy
                + "&sortOrder=" + sortOrder
                + "&recordsPerPage=" + recordsPerPage
                + "&currentPage=" + currentPage,
                	result.getPath());
        Assertions.assertTrue(result.isRedirect());
    }
    
    @Test
    void testChangeStatusActionCommandNullParameterRecordsPerPage() {
    	String whereName = "city_to";
        String whereValue = "Дніпро";
        String orderBy = "id";
        String sortOrder = "ASC";
        String recordsPerPage = "5";
        String currentPage = "1";
        
        when(request.getParameter("whereName")).thenReturn(whereName);
        when(request.getParameter("whereValue")).thenReturn(whereValue);
        when(request.getParameter("orderBy")).thenReturn(orderBy);
        when(request.getParameter("sortOrder")).thenReturn(sortOrder);
        when(request.getParameter("currentPage")).thenReturn(currentPage);
        
        when(request.getParameter("orderId")).thenReturn("12");
        when(request.getParameter("status")).thenReturn("NEW");

        ActionCommandResult result = changeStatusActionCommand.execute(request, response);

        assertEquals("viewOrders" 
        		+ "?whereName=" + whereName
                + "&whereValue=" + whereValue
                + "&orderBy=" + orderBy
                + "&sortOrder=" + sortOrder
                + "&recordsPerPage=" + recordsPerPage
                + "&currentPage=" + currentPage,
                	result.getPath());
        Assertions.assertTrue(result.isRedirect());
    }

    @Test
    void testChangeStatusActionCommandWrongParameterRecordsPerPage() {
    	String whereName = "city_to";
        String whereValue = "Дніпро";
        String orderBy = "id";
        String sortOrder = "ASC";
        String recordsPerPage = "5";
        String currentPage = "1";
        
        when(request.getParameter("whereName")).thenReturn(whereName);
        when(request.getParameter("whereValue")).thenReturn(whereValue);
        when(request.getParameter("orderBy")).thenReturn(orderBy);
        when(request.getParameter("sortOrder")).thenReturn(sortOrder);
        when(request.getParameter("recordsPerPage")).thenReturn("6.0");
        when(request.getParameter("currentPage")).thenReturn(currentPage);
        
        when(request.getParameter("orderId")).thenReturn("12");
        when(request.getParameter("status")).thenReturn("NEW");

        ActionCommandResult result = changeStatusActionCommand.execute(request, response);

        assertEquals("viewOrders" 
        		+ "?whereName=" + whereName
                + "&whereValue=" + whereValue
                + "&orderBy=" + orderBy
                + "&sortOrder=" + sortOrder
                + "&recordsPerPage=" + recordsPerPage
                + "&currentPage=" + currentPage,
                	result.getPath());
        Assertions.assertTrue(result.isRedirect());
    }
    
    @Test
    void testChangeStatusActionCommandNullParameterCurrentPage() {
    	String whereName = "city_to";
        String whereValue = "Дніпро";
        String orderBy = "id";
        String sortOrder = "ASC";
        String recordsPerPage = "10";
        String currentPage = "1";
        
        when(request.getParameter("whereName")).thenReturn(whereName);
        when(request.getParameter("whereValue")).thenReturn(whereValue);
        when(request.getParameter("orderBy")).thenReturn(orderBy);
        when(request.getParameter("sortOrder")).thenReturn(sortOrder);
        when(request.getParameter("recordsPerPage")).thenReturn(recordsPerPage);
        
        when(request.getParameter("orderId")).thenReturn("12");
        when(request.getParameter("status")).thenReturn("NEW");

        ActionCommandResult result = changeStatusActionCommand.execute(request, response);

        assertEquals("viewOrders" 
        		+ "?whereName=" + whereName
                + "&whereValue=" + whereValue
                + "&orderBy=" + orderBy
                + "&sortOrder=" + sortOrder
                + "&recordsPerPage=" + recordsPerPage
                + "&currentPage=" + currentPage,
                	result.getPath());
        Assertions.assertTrue(result.isRedirect());
    }
    
    @Test
    void testChangeStatusActionCommandWrongParameterCurrentPage() {
    	String whereName = "city_to";
        String whereValue = "Дніпро";
        String orderBy = "id";
        String sortOrder = "ASC";
        String recordsPerPage = "10";
        String currentPage = "1";
        
        when(request.getParameter("whereName")).thenReturn(whereName);
        when(request.getParameter("whereValue")).thenReturn(whereValue);
        when(request.getParameter("orderBy")).thenReturn(orderBy);
        when(request.getParameter("sortOrder")).thenReturn(sortOrder);
        when(request.getParameter("recordsPerPage")).thenReturn(recordsPerPage);
        when(request.getParameter("currentPage")).thenReturn("5.0");
        
        when(request.getParameter("orderId")).thenReturn("12");
        when(request.getParameter("status")).thenReturn("NEW");

        ActionCommandResult result = changeStatusActionCommand.execute(request, response);

        assertEquals("viewOrders" 
        		+ "?whereName=" + whereName
                + "&whereValue=" + whereValue
                + "&orderBy=" + orderBy
                + "&sortOrder=" + sortOrder
                + "&recordsPerPage=" + recordsPerPage
                + "&currentPage=" + currentPage,
                	result.getPath());
        Assertions.assertTrue(result.isRedirect());
    }
    
    @Test
    void testChangeStatusActionCommandNullParameterOrderId() {
    	String whereName = "city_to";
        String whereValue = "Дніпро";
        String orderBy = "id";
        String sortOrder = "ASC";
        String recordsPerPage = "10";
        String currentPage = "1";
        
        when(request.getParameter("whereName")).thenReturn(whereName);
        when(request.getParameter("whereValue")).thenReturn(whereValue);
        when(request.getParameter("orderBy")).thenReturn(orderBy);
        when(request.getParameter("sortOrder")).thenReturn(sortOrder);
        when(request.getParameter("recordsPerPage")).thenReturn(recordsPerPage);
        when(request.getParameter("currentPage")).thenReturn(currentPage);
        
        when(request.getParameter("status")).thenReturn("NEW");

        ActionCommandResult result = changeStatusActionCommand.execute(request, response);

        assertEquals("error", result.getPath());
        assertFalse(result.isRedirect());
    }
    
    @Test
    void testChangeStatusActionCommandWrongParameterOrderId() {
    	String whereName = "city_to";
        String whereValue = "Дніпро";
        String orderBy = "id";
        String sortOrder = "ASC";
        String recordsPerPage = "10";
        String currentPage = "1";
        
        when(request.getParameter("whereName")).thenReturn(whereName);
        when(request.getParameter("whereValue")).thenReturn(whereValue);
        when(request.getParameter("orderBy")).thenReturn(orderBy);
        when(request.getParameter("sortOrder")).thenReturn(sortOrder);
        when(request.getParameter("recordsPerPage")).thenReturn(recordsPerPage);
        when(request.getParameter("currentPage")).thenReturn(currentPage);
        
        when(request.getParameter("orderId")).thenReturn("12.2");
        when(request.getParameter("status")).thenReturn("NEW");

        ActionCommandResult result = changeStatusActionCommand.execute(request, response);

        assertEquals("error", result.getPath());
        assertFalse(result.isRedirect());
    }
    
    @Test
    void testChangeStatusActionCommandNullParameterStatus() {
    	String whereName = "city_to";
        String whereValue = "Дніпро";
        String orderBy = "id";
        String sortOrder = "ASC";
        String recordsPerPage = "10";
        String currentPage = "1";
        
        when(request.getParameter("whereName")).thenReturn(whereName);
        when(request.getParameter("whereValue")).thenReturn(whereValue);
        when(request.getParameter("orderBy")).thenReturn(orderBy);
        when(request.getParameter("sortOrder")).thenReturn(sortOrder);
        when(request.getParameter("recordsPerPage")).thenReturn(recordsPerPage);
        when(request.getParameter("currentPage")).thenReturn(currentPage);
        
        when(request.getParameter("orderId")).thenReturn("12");

        ActionCommandResult result = changeStatusActionCommand.execute(request, response);

        assertEquals("error", result.getPath());
        assertFalse(result.isRedirect());
    }
    
    @Test
    void testChangeStatusActionCommandWrongParameterStatus() {
    	String whereName = "city_to";
        String whereValue = "Дніпро";
        String orderBy = "id";
        String sortOrder = "ASC";
        String recordsPerPage = "10";
        String currentPage = "1";
        
        when(request.getParameter("whereName")).thenReturn(whereName);
        when(request.getParameter("whereValue")).thenReturn(whereValue);
        when(request.getParameter("orderBy")).thenReturn(orderBy);
        when(request.getParameter("sortOrder")).thenReturn(sortOrder);
        when(request.getParameter("recordsPerPage")).thenReturn(recordsPerPage);
        when(request.getParameter("currentPage")).thenReturn(currentPage);
        
        when(request.getParameter("orderId")).thenReturn("12");
        when(request.getParameter("status")).thenReturn("old");

        ActionCommandResult result = changeStatusActionCommand.execute(request, response);

        assertEquals("error", result.getPath());
        assertFalse(result.isRedirect());
    }
    
    @Test
    void testChangeStatusActionCommandRightParametersThrowException() throws ServiceException {
    	String whereName = "city_to";
        String whereValue = "Дніпро";
        String orderBy = "id";
        String sortOrder = "ASC";
        String recordsPerPage = "10";
        String currentPage = "1";
        
        when(request.getParameter("whereName")).thenReturn(whereName);
        when(request.getParameter("whereValue")).thenReturn(whereValue);
        when(request.getParameter("orderBy")).thenReturn(orderBy);
        when(request.getParameter("sortOrder")).thenReturn(sortOrder);
        when(request.getParameter("recordsPerPage")).thenReturn(recordsPerPage);
        when(request.getParameter("currentPage")).thenReturn(currentPage);
        
        when(request.getParameter("orderId")).thenReturn("12");
        when(request.getParameter("status")).thenReturn("NEW");

        doThrow(new ServiceException("Test message")).when(orderService).updateStatus(12, Status.NEW);

        ActionCommandResult result = changeStatusActionCommand.execute(request, response);

        assertEquals("error", result.getPath());
        assertFalse(result.isRedirect());
    }

    @AfterEach
    void tearDown() {
        response = null;
        request = null;
        changeStatusActionCommand = null;
        orderService = null;
    }
}