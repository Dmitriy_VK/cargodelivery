package org.finaltask.controller.command.impl;

import org.finaltask.controller.command.ActionCommandResult;
import org.finaltask.controller.command.impl.common.CalculatePageActionCommand;
import org.finaltask.entity.CargoType;
import org.finaltask.entity.City;
import org.finaltask.entity.Route;
import org.finaltask.service.CargoTypeService;
import org.finaltask.service.ServiceException;
import org.finaltask.service.impl.CargoTypeServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.powermock.reflect.Whitebox;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class CalculatePageActionCommandTest {
    private HttpServletResponse response;
    private HttpServletRequest request;
    private HttpSession session;
    private CalculatePageActionCommand calculatePageActionCommand;
    private Route route;
    private CargoTypeService cargoTypeService;

    @BeforeEach
    void init() {
        response = mock(HttpServletResponse.class);
        request = mock(HttpServletRequest.class);
        session = mock(HttpSession.class);
        calculatePageActionCommand = new CalculatePageActionCommand();
        route = new Route();
        route.setCityFrom(new City());
        route.setCityTo(new City());
        cargoTypeService = mock(CargoTypeServiceImpl.class);
        Whitebox.setInternalState(calculatePageActionCommand, "cargoTypeService", cargoTypeService);
    }

    @Test
    void testCalculatePageActionCommandRightParameters() throws ServiceException {
        when(request.getParameter("formData")).thenReturn("formData");
        when(request.getParameter("cargoTypeId")).thenReturn("25");
        when(request.getParameter("weight")).thenReturn("5.0");
        when(request.getParameter("volume")).thenReturn("3");
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("routeCalcPage")).thenReturn(route);
        when(session.getAttribute("locale")).thenReturn("uk");
        CargoType cargoType = new CargoType();
        cargoType.setId(25);
        List<CargoType> cargoTypeList = new ArrayList<>() {
            {
                add(new CargoType());
                add(cargoType);
            }
        };
        when(session.getAttribute("cargoTypeList")).thenReturn(cargoTypeList);
        when(cargoTypeService.findAllWithTranslatedNames(anyString())).thenReturn(cargoTypeList);

        ActionCommandResult result = calculatePageActionCommand.execute(request, response);

        assertEquals("calculateResult", result.getPath());
        assertTrue(result.isRedirect());
    }

    @Test
    void testCalculatePageActionCommandNullRoute() throws ServiceException {
        when(request.getParameter("formData")).thenReturn("formData");
        when(request.getParameter("cargoTypeId")).thenReturn("25");
        when(request.getParameter("weight")).thenReturn("5.0");
        when(request.getParameter("volume")).thenReturn("3");
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("locale")).thenReturn("uk");
        CargoType cargoType = new CargoType();
        cargoType.setId(25);
        List<CargoType> cargoTypeList = new ArrayList<>() {
            {
                add(new CargoType());
                add(cargoType);
            }
        };
        when(session.getAttribute("cargoTypeList")).thenReturn(cargoTypeList);
        when(cargoTypeService.findAllWithTranslatedNames(anyString())).thenReturn(cargoTypeList);

        ActionCommandResult result = calculatePageActionCommand.execute(request, response);

        assertEquals("error", result.getPath());
        assertFalse(result.isRedirect());
    }

    @Test
    void testCalculatePageActionCommandWrongParameterFormData() throws ServiceException {
        when(request.getParameter("formData")).thenReturn("form");
        when(request.getParameter("cargoTypeId")).thenReturn("25");
        when(request.getParameter("weight")).thenReturn("5.0");
        when(request.getParameter("volume")).thenReturn("3");
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("locale")).thenReturn("uk");
        CargoType cargoType = new CargoType();
        cargoType.setId(25);
        List<CargoType> cargoTypeList = new ArrayList<>() {
            {
                add(new CargoType());
                add(cargoType);
            }
        };
        when(session.getAttribute("cargoTypeList")).thenReturn(cargoTypeList);
        when(cargoTypeService.findAllWithTranslatedNames(anyString())).thenReturn(cargoTypeList);

        ActionCommandResult result = calculatePageActionCommand.execute(request, response);

        assertEquals("calcForm", result.getPath());
        assertFalse(result.isRedirect());
    }

    @Test
    void testCalculatePageActionCommandNullParameterFormDataAndCargoTypeServiceThrowServiceException() throws ServiceException {
        when(request.getParameter("cargoTypeId")).thenReturn("25");
        when(request.getParameter("weight")).thenReturn("5.0");
        when(request.getParameter("volume")).thenReturn("3");
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("locale")).thenReturn("uk");
        CargoType cargoType = new CargoType();
        cargoType.setId(25);
        List<CargoType> cargoTypeList = new ArrayList<>() {
            {
                add(new CargoType());
                add(cargoType);
            }
        };
        when(session.getAttribute("cargoTypeList")).thenReturn(cargoTypeList);
        when(cargoTypeService.findAllWithTranslatedNames(anyString())).thenThrow(new ServiceException("Test message"));

        ActionCommandResult result = calculatePageActionCommand.execute(request, response);

        assertEquals("error", result.getPath());
        assertFalse(result.isRedirect());
    }

    @Test
    void testCalculatePageActionCommandNullParameterCargoTypeId() throws ServiceException {
        when(request.getParameter("formData")).thenReturn("formData");
        when(request.getParameter("weight")).thenReturn("5.0");
        when(request.getParameter("volume")).thenReturn("3");
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("routeCalcPage")).thenReturn(route);
        when(session.getAttribute("locale")).thenReturn("uk");
        CargoType cargoType = new CargoType();
        cargoType.setId(25);
        List<CargoType> cargoTypeList = new ArrayList<>() {
            {
                add(new CargoType());
                add(cargoType);
            }
        };
        when(session.getAttribute("cargoTypeList")).thenReturn(cargoTypeList);
        when(cargoTypeService.findAllWithTranslatedNames(anyString())).thenReturn(cargoTypeList);

        ActionCommandResult result = calculatePageActionCommand.execute(request, response);

        assertEquals("calcForm", result.getPath());
        assertFalse(result.isRedirect());
    }

    @Test
    void testCalculatePageActionCommandWrongParameterCargoTypeId() throws ServiceException {
        when(request.getParameter("formData")).thenReturn("formData");
        when(request.getParameter("cargoTypeId")).thenReturn("25n");
        when(request.getParameter("weight")).thenReturn("5.0");
        when(request.getParameter("volume")).thenReturn("3");
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("routeCalcPage")).thenReturn(route);
        when(session.getAttribute("locale")).thenReturn("uk");
        CargoType cargoType = new CargoType();
        cargoType.setId(25);
        List<CargoType> cargoTypeList = new ArrayList<>() {
            {
                add(new CargoType());
                add(cargoType);
            }
        };
        when(session.getAttribute("cargoTypeList")).thenReturn(cargoTypeList);
        when(cargoTypeService.findAllWithTranslatedNames(anyString())).thenReturn(cargoTypeList);

        ActionCommandResult result = calculatePageActionCommand.execute(request, response);

        assertEquals("calcForm", result.getPath());
        assertFalse(result.isRedirect());
    }

    @Test
    void testCalculatePageActionCommandNullParameterWeight() throws ServiceException {
        when(request.getParameter("formData")).thenReturn("formData");
        when(request.getParameter("cargoTypeId")).thenReturn("25");
        when(request.getParameter("volume")).thenReturn("3");
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("routeCalcPage")).thenReturn(route);
        when(session.getAttribute("locale")).thenReturn("uk");
        CargoType cargoType = new CargoType();
        cargoType.setId(25);
        List<CargoType> cargoTypeList = new ArrayList<>() {
            {
                add(new CargoType());
                add(cargoType);
            }
        };
        when(session.getAttribute("cargoTypeList")).thenReturn(cargoTypeList);
        when(cargoTypeService.findAllWithTranslatedNames(anyString())).thenReturn(cargoTypeList);

        ActionCommandResult result = calculatePageActionCommand.execute(request, response);

        assertEquals("calcForm", result.getPath());
        assertFalse(result.isRedirect());
    }

    @Test
    void testCalculatePageActionCommandWrongParameterWeight() throws ServiceException {
        when(request.getParameter("formData")).thenReturn("formData");
        when(request.getParameter("cargoTypeId")).thenReturn("25");
        when(request.getParameter("weight")).thenReturn("-5.0");
        when(request.getParameter("volume")).thenReturn("3");
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("routeCalcPage")).thenReturn(route);
        when(session.getAttribute("locale")).thenReturn("uk");
        CargoType cargoType = new CargoType();
        cargoType.setId(25);
        List<CargoType> cargoTypeList = new ArrayList<>() {
            {
                add(new CargoType());
                add(cargoType);
            }
        };
        when(session.getAttribute("cargoTypeList")).thenReturn(cargoTypeList);
        when(cargoTypeService.findAllWithTranslatedNames(anyString())).thenReturn(cargoTypeList);

        ActionCommandResult result = calculatePageActionCommand.execute(request, response);

        assertEquals("calcForm", result.getPath());
        assertFalse(result.isRedirect());
    }

    @Test
    void testCalculatePageActionCommandNullParameterVolume() throws ServiceException {
        when(request.getParameter("formData")).thenReturn("formData");
        when(request.getParameter("cargoTypeId")).thenReturn("25");
        when(request.getParameter("weight")).thenReturn("5.0");
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("routeCalcPage")).thenReturn(route);
        when(session.getAttribute("locale")).thenReturn("uk");
        CargoType cargoType = new CargoType();
        cargoType.setId(25);
        List<CargoType> cargoTypeList = new ArrayList<>() {
            {
                add(new CargoType());
                add(cargoType);
            }
        };
        when(session.getAttribute("cargoTypeList")).thenReturn(cargoTypeList);
        when(cargoTypeService.findAllWithTranslatedNames(anyString())).thenReturn(cargoTypeList);

        ActionCommandResult result = calculatePageActionCommand.execute(request, response);

        assertEquals("calcForm", result.getPath());
        assertFalse(result.isRedirect());
    }

    @Test
    void testCalculatePageActionCommandWrongParameterVolume() throws ServiceException {
        when(request.getParameter("formData")).thenReturn("formData");
        when(request.getParameter("cargoTypeId")).thenReturn("25");
        when(request.getParameter("weight")).thenReturn("5.0");
        when(request.getParameter("volume")).thenReturn("-3");
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("routeCalcPage")).thenReturn(route);
        when(session.getAttribute("locale")).thenReturn("uk");
        CargoType cargoType = new CargoType();
        cargoType.setId(25);
        List<CargoType> cargoTypeList = new ArrayList<>() {
            {
                add(new CargoType());
                add(cargoType);
            }
        };
        when(session.getAttribute("cargoTypeList")).thenReturn(cargoTypeList);
        when(cargoTypeService.findAllWithTranslatedNames(anyString())).thenReturn(cargoTypeList);

        ActionCommandResult result = calculatePageActionCommand.execute(request, response);

        assertEquals("calcForm", result.getPath());
        assertFalse(result.isRedirect());
    }

    @Test
    void testCalculatePageActionCommandNullSessionCargoTypeList() throws ServiceException {
        when(request.getParameter("formData")).thenReturn("formData");
        when(request.getParameter("cargoTypeId")).thenReturn("25");
        when(request.getParameter("weight")).thenReturn("5.0");
        when(request.getParameter("volume")).thenReturn("3");
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("routeCalcPage")).thenReturn(route);
        when(session.getAttribute("locale")).thenReturn("uk");
        CargoType cargoType = new CargoType();
        cargoType.setId(25);
        List<CargoType> cargoTypeList = new ArrayList<>() {
            {
                add(new CargoType());
                add(cargoType);
            }
        };
        when(cargoTypeService.findAllWithTranslatedNames(anyString())).thenReturn(cargoTypeList);

        ActionCommandResult result = calculatePageActionCommand.execute(request, response);

        assertEquals("error", result.getPath());
        assertFalse(result.isRedirect());
    }
}