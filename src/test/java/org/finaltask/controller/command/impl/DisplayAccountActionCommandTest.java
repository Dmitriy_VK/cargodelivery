package org.finaltask.controller.command.impl;

import org.finaltask.controller.command.ActionCommandResult;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class DisplayAccountActionCommandTest {
    HttpServletResponse response;
    HttpServletRequest request;
    DisplayAccountActionCommand displayAccountActionCommand;

    @BeforeEach
    void init() {
        response = mock(HttpServletResponse.class);
        request = mock(HttpServletRequest.class);
        displayAccountActionCommand = new DisplayAccountActionCommand();
    }

    @Test
    void testDisplayAccountActionCommand() {
        when(request.getParameter("regResult")).thenReturn("true");

        ActionCommandResult result = displayAccountActionCommand.execute(request, response);

        assertEquals("registration", result.getPath());
        assertFalse(result.isRedirect());
    }

    @Test
    void testDisplayAccountActionCommandNullParameterRegResult() {
        ActionCommandResult result = displayAccountActionCommand.execute(request, response);

        assertEquals("error", result.getPath());
        assertFalse(result.isRedirect());
    }

    @AfterEach
    void tearDown() {
        response = null;
        request = null;
    }
}