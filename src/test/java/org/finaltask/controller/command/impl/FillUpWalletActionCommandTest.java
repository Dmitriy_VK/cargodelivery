package org.finaltask.controller.command.impl;

import org.finaltask.controller.command.ActionCommandResult;
import org.finaltask.controller.command.impl.user.FillUpWalletActionCommand;
import org.finaltask.entity.Account;
import org.finaltask.entity.Wallet;
import org.finaltask.service.ServiceException;
import org.finaltask.service.WalletService;
import org.finaltask.service.impl.WalletServiceImpl;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.powermock.reflect.Whitebox;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.math.BigDecimal;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;

class FillUpWalletActionCommandTest  {
    HttpServletResponse response;
    HttpServletRequest request;
    WalletService walletService;
    HttpSession session;
    FillUpWalletActionCommand fillUpWalletActionCommand;
    Account account;

    @BeforeEach
    void init() {
        response = mock(HttpServletResponse.class);
        request = mock(HttpServletRequest.class);
        session = mock(HttpSession.class);
        walletService = mock(WalletServiceImpl.class);
        fillUpWalletActionCommand = new FillUpWalletActionCommand();
        Whitebox.setInternalState(fillUpWalletActionCommand, "walletService", walletService);
        account = new Account();
        account.setWallet(new Wallet());
    }

    @Test
    void testFillUpWalletActionCommandRightParameters() throws ServiceException {
    	String amountOfMoney = "22.22";
        when(request.getParameter("formData")).thenReturn("formData");
        when(request.getParameter("amount")).thenReturn(amountOfMoney);
        when(request.getSession(false)).thenReturn(session);
        when(session.getAttribute("account")).thenReturn(account);
        when(walletService.findEntityById(anyInt())).thenReturn(new Wallet());

        ActionCommandResult result = fillUpWalletActionCommand.execute(request, response);

        assertEquals("fillUpWalletResult" + "?success=true&amountOfMoney=" + amountOfMoney, result.getPath());
        Assertions.assertTrue(result.isRedirect());
    }
    
    @Test
    void testFillUpWalletActionCommandNullParameterFormData() throws ServiceException {
    	String amountOfMoney = "22.22";
        when(request.getParameter("amount")).thenReturn(amountOfMoney);
        when(request.getSession(false)).thenReturn(session);
        when(session.getAttribute("account")).thenReturn(account);
        when(walletService.findEntityById(anyInt())).thenReturn(new Wallet());

        ActionCommandResult result = fillUpWalletActionCommand.execute(request, response);

        assertEquals("fillUpWallet", result.getPath());
        assertFalse(result.isRedirect());
    }
    
    @Test
    void testFillUpWalletActionCommandWrongParameterFormData() throws ServiceException {
    	String amountOfMoney = "22.22";
    	when(request.getParameter("formData")).thenReturn("form");
        when(request.getParameter("amount")).thenReturn(amountOfMoney);
        when(request.getSession(false)).thenReturn(session);
        when(session.getAttribute("account")).thenReturn(account);
        when(walletService.findEntityById(anyInt())).thenReturn(new Wallet());

        ActionCommandResult result = fillUpWalletActionCommand.execute(request, response);

        assertEquals("fillUpWallet", result.getPath());
        assertFalse(result.isRedirect());
    }
    
    @Test
    void testFillUpWalletActionCommandNullParameterAmount() throws ServiceException {
        when(request.getParameter("formData")).thenReturn("formData");
        when(request.getSession(false)).thenReturn(session);
        when(session.getAttribute("account")).thenReturn(account);
        when(walletService.findEntityById(anyInt())).thenReturn(new Wallet());

        ActionCommandResult result = fillUpWalletActionCommand.execute(request, response);

        assertEquals("fillUpWallet", result.getPath());
        assertFalse(result.isRedirect());
    }
    
    @Test
    void testFillUpWalletActionCommandWrongParameterAmount() throws ServiceException {
    	String amountOfMoney = "22.224";
        when(request.getParameter("formData")).thenReturn("formData");
        when(request.getParameter("amount")).thenReturn(amountOfMoney);
        when(request.getSession(false)).thenReturn(session);
        when(session.getAttribute("account")).thenReturn(account);
        when(walletService.findEntityById(anyInt())).thenReturn(new Wallet());

        ActionCommandResult result = fillUpWalletActionCommand.execute(request, response);

        assertEquals("fillUpWallet", result.getPath());
        assertFalse(result.isRedirect());
    }
    
    @Test
    void testFillUpWalletActionCommandWalletServiceThrowServiceExceptionWhenFillUpWallet() throws ServiceException {
    	String amountOfMoney = "22.22";
        when(request.getParameter("formData")).thenReturn("formData");
        when(request.getParameter("amount")).thenReturn(amountOfMoney);
        when(request.getSession(false)).thenReturn(session);
        when(session.getAttribute("account")).thenReturn(account);
        doThrow(new ServiceException("Test message")).when(walletService).fillUpWallet(0, new BigDecimal("22.22"));
        when(walletService.findEntityById(anyInt())).thenReturn(new Wallet());

        ActionCommandResult result = fillUpWalletActionCommand.execute(request, response);

        assertEquals("error", result.getPath());
        assertFalse(result.isRedirect());
    }
    
    @Test
    void testFillUpWalletActionCommandWalletServiceThrowServiceExceptionWhenFindEntityById() throws ServiceException {
    	String amountOfMoney = "22.22";
        when(request.getParameter("formData")).thenReturn("formData");
        when(request.getParameter("amount")).thenReturn(amountOfMoney);
        when(request.getSession(false)).thenReturn(session);
        when(session.getAttribute("account")).thenReturn(account);
        
        when(walletService.findEntityById(0)).thenThrow(new ServiceException("Test message"));

        ActionCommandResult result = fillUpWalletActionCommand.execute(request, response);

        assertEquals("error", result.getPath());
        assertFalse(result.isRedirect());
    }
    
    @Test
    void testFillUpWalletActionCommandNullSession() throws ServiceException {
    	String amountOfMoney = "22.22";
        when(request.getParameter("formData")).thenReturn("formData");
        when(request.getParameter("amount")).thenReturn(amountOfMoney);
        
        when(session.getAttribute("account")).thenReturn(account);
        when(walletService.findEntityById(anyInt())).thenReturn(new Wallet());

        ActionCommandResult result = fillUpWalletActionCommand.execute(request, response);

        assertEquals("error", result.getPath());
        assertFalse(result.isRedirect());
    }
    
    @Test
    void testFillUpWalletActionCommandNullAccountInSession() throws ServiceException {
    	String amountOfMoney = "22.22";
        when(request.getParameter("formData")).thenReturn("formData");
        when(request.getParameter("amount")).thenReturn(amountOfMoney);
        when(request.getSession(false)).thenReturn(session);
        when(walletService.findEntityById(anyInt())).thenReturn(new Wallet());

        ActionCommandResult result = fillUpWalletActionCommand.execute(request, response);

        assertEquals("error", result.getPath());
        assertFalse(result.isRedirect());
    }

    @AfterEach
    void tearDown() {
        response = null;
        request = null;
        walletService = null;
        session = null;
        fillUpWalletActionCommand = null;
        account = null;
    }
}