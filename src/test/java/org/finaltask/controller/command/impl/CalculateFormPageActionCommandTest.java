package org.finaltask.controller.command.impl;

import org.finaltask.controller.command.ActionCommandResult;
import org.finaltask.controller.command.impl.common.CalculateFormPageActionCommand;
import org.finaltask.entity.CargoType;
import org.finaltask.entity.Route;
import org.finaltask.service.CargoTypeService;
import org.finaltask.service.RouteService;
import org.finaltask.service.ServiceException;
import org.finaltask.service.impl.CargoTypeServiceImpl;
import org.finaltask.service.impl.RouteServiceImpl;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.powermock.reflect.Whitebox;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class CalculateFormPageActionCommandTest {
    private HttpServletResponse response;
    private HttpServletRequest request;
    private RouteService routeService;
    private CargoTypeService cargoTypeService;
    private HttpSession session;
    private CalculateFormPageActionCommand calculateFormPageActionCommand;

    @BeforeEach
    void init() {
        response = mock(HttpServletResponse.class);
        request = mock(HttpServletRequest.class);
        session = mock(HttpSession.class);
        routeService = mock(RouteServiceImpl.class);
        cargoTypeService = mock(CargoTypeServiceImpl.class);
        calculateFormPageActionCommand = new CalculateFormPageActionCommand();
        Whitebox.setInternalState(calculateFormPageActionCommand, "routeService", routeService);
        Whitebox.setInternalState(calculateFormPageActionCommand, "cargoTypeService", cargoTypeService);
    }

    @Test
    void testCalculateFormPageActionCommandRightId() throws ServiceException {
        when(request.getParameter("id")).thenReturn("25");
        when(request.getSession()).thenReturn(session);
        when(routeService.findEntityById(anyInt())).thenReturn(new Route());
        List<CargoType> cargoTypeList = new ArrayList<>();
        when(cargoTypeService.findAllWithTranslatedNames(anyString())).thenReturn(cargoTypeList);

        ActionCommandResult result = calculateFormPageActionCommand.execute(request, response);

        assertEquals("calcForm", result.getPath());
        assertFalse(result.isRedirect());
    }

    @Test
    void testCalculateFormPageActionCommandWrongId() throws ServiceException {
        when(request.getParameter("id")).thenReturn("a25b");
        when(request.getSession()).thenReturn(session);
        when(routeService.findEntityById(anyInt())).thenReturn(new Route());
        List<CargoType> cargoTypeList = new ArrayList<>();
        when(cargoTypeService.findAllWithTranslatedNames(anyString())).thenReturn(cargoTypeList);

        ActionCommandResult result = calculateFormPageActionCommand.execute(request, response);

        assertEquals("error", result.getPath());
        assertFalse(result.isRedirect());
    }

    @Test
    void testCalculateFormPageActionCommandNullId() throws ServiceException {
        when(request.getSession()).thenReturn(session);
        when(routeService.findEntityById(anyInt())).thenReturn(new Route());
        List<CargoType> cargoTypeList = new ArrayList<>();
        when(cargoTypeService.findAllWithTranslatedNames(anyString())).thenReturn(cargoTypeList);

        ActionCommandResult result = calculateFormPageActionCommand.execute(request, response);

        assertEquals("error", result.getPath());
        assertFalse(result.isRedirect());
    }

    @Test
    void testCalculateFormPageActionCommandRouteServiceThrowException() throws ServiceException {
        when(request.getParameter("id")).thenReturn("25");
        when(request.getSession()).thenReturn(session);
        when(routeService.findEntityById(anyInt())).thenThrow(new ServiceException("Test message"));
        List<CargoType> cargoTypeList = new ArrayList<>();
        when(cargoTypeService.findAllWithTranslatedNames(anyString())).thenReturn(cargoTypeList);

        ActionCommandResult result = calculateFormPageActionCommand.execute(request, response);

        assertEquals("error", result.getPath());
        assertFalse(result.isRedirect());
    }

    @Test
    void testCalculateFormPageActionCommandcargoTypeServiceThrowException() throws ServiceException {
        when(request.getParameter("id")).thenReturn("25");
        when(request.getSession()).thenReturn(session);
        when(routeService.findEntityById(anyInt())).thenReturn(new Route());
        when(cargoTypeService.findAllWithTranslatedNames(anyString())).thenThrow(new ServiceException("Test message"));

        ActionCommandResult result = calculateFormPageActionCommand.execute(request, response);

        assertEquals("error", result.getPath());
        assertFalse(result.isRedirect());
    }

    @AfterEach
    void tearDown() {
        response = null;
        request = null;
        routeService = null;
        session = null;
        calculateFormPageActionCommand = null;
    }
}