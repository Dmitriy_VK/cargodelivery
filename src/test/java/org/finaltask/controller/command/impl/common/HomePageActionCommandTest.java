package org.finaltask.controller.command.impl.common;

import org.finaltask.controller.command.ActionCommandResult;
import org.finaltask.entity.Route;
import org.finaltask.service.RouteService;
import org.finaltask.service.ServiceException;
import org.finaltask.service.impl.RouteServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.powermock.reflect.Whitebox;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class HomePageActionCommandTest {
    private HttpServletResponse response;
    private HttpServletRequest request;
    private HttpSession session;
    private HomePageActionCommand homePageActionCommand;
    private RouteService routeService;

    @BeforeEach
    void setUp() {
        response = mock(HttpServletResponse.class);
        request = mock(HttpServletRequest.class);
        session = mock(HttpSession.class);
        homePageActionCommand = new HomePageActionCommand();
        routeService = mock(RouteServiceImpl.class);
        Whitebox.setInternalState(homePageActionCommand, "routeService", routeService);
    }

    @Test
    void testHomePageActionCommandRightParameters() throws ServiceException {
        when(request.getParameter("orderBy")).thenReturn("city_from");
        when(request.getParameter("sortOrder")).thenReturn("ASC");
        when(request.getParameter("where")).thenReturn("city_id_to");
        when(request.getParameter("cityId")).thenReturn("3");
        when(request.getParameter("recordsPerPage")).thenReturn("10");
        when(request.getParameter("Page")).thenReturn("2");
        when(request.getSession()).thenReturn(session);

        when(session.getAttribute("locale")).thenReturn("uk");
        when(routeService.getNumberOfRows(anyString(), anyLong())).thenReturn(11);

        List<Route> routeList = new ArrayList<>() {
            {
                add(new Route(32L));
                add(new Route(15L));
            }
        };

        when(routeService.findAllWithCityNames(
                anyString(),
                anyString(),
                anyString(),
                anyString(),
                anyLong(),
                anyInt(),
                anyInt())).thenReturn(routeList);

        ActionCommandResult result = homePageActionCommand.execute(request, response);

        assertEquals("home", result.getPath());
        assertFalse(result.isRedirect());
    }

    @Test
    void testHomePageActionCommandRouteServiceThrowsServiceException() throws ServiceException {
        when(request.getParameter("orderBy")).thenReturn("city_from");
        when(request.getParameter("sortOrder")).thenReturn("ASC");
        when(request.getParameter("where")).thenReturn("city_id_to");
        when(request.getParameter("cityId")).thenReturn("3");
        when(request.getParameter("recordsPerPage")).thenReturn("10");
        when(request.getParameter("Page")).thenReturn("2");
        when(request.getSession()).thenReturn(session);

        when(session.getAttribute("locale")).thenReturn("uk");
        when(routeService.getNumberOfRows(anyString(), anyLong())).thenThrow(new ServiceException("Test message"));

        List<Route> routeList = new ArrayList<>() {
            {
                add(new Route(32L));
                add(new Route(15L));
            }
        };

        when(routeService.findAllWithCityNames(
                anyString(),
                anyString(),
                anyString(),
                anyString(),
                anyLong(),
                anyInt(),
                anyInt())).thenReturn(routeList);

        ActionCommandResult result = homePageActionCommand.execute(request, response);

        assertEquals("error", result.getPath());
        assertFalse(result.isRedirect());
    }
}