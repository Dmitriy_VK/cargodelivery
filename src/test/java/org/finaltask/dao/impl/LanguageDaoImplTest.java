package org.finaltask.dao.impl;

import org.finaltask.dao.DBException;
import org.finaltask.entity.Account;
import org.finaltask.entity.Language;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class LanguageDaoImplTest {
    private Connection connection;
    private PreparedStatement prdStatement;
    private ResultSet resultSet;
    private Language testLanguage;
    private static final String ID = "id";
    private static final String NAME = "name";
    private static final String CODE = "code";
    @BeforeEach
    void init() {
        connection = mock(Connection.class);
        prdStatement = mock(PreparedStatement.class);
        resultSet = mock(ResultSet.class);
        testLanguage = new Language();
        testLanguage.setId(25L);
        testLanguage.setName("English");
        testLanguage.setCode("en");
    }

    @Test
    void testFindAll() {
        assertThrows(UnsupportedOperationException.class, () -> new LanguageDaoImpl(connection).findAll());
    }

    @Test
    void testFindEntityById() {
        assertThrows(UnsupportedOperationException.class, () -> new LanguageDaoImpl(connection).findEntityById(25L));
    }

    @Test
    void findEntityByLanguageCode() throws SQLException, DBException {
        when(connection.prepareStatement(any(String.class))).thenReturn(prdStatement);

        when(prdStatement.executeQuery()).thenReturn(resultSet);
        when(resultSet.next()).thenReturn(true).thenReturn(false);

        when(resultSet.getLong(ID)).thenReturn(25L);
        when(resultSet.getString(NAME)).thenReturn("English");
        when(resultSet.getString(CODE)).thenReturn("en");

        LanguageDaoImpl languageDao = new LanguageDaoImpl(connection);
        Language language = languageDao.findEntityByLanguageCode("en");

        assertTrue(isEqualLanguage(testLanguage, language));
    }

    @Test
    void testDeleteById() {
        assertThrows(UnsupportedOperationException.class, () -> new LanguageDaoImpl(connection).delete(25L));
    }

    @Test
    void testDeleteByEntity() {
        assertThrows(UnsupportedOperationException.class, () -> new LanguageDaoImpl(connection).delete(new Language()));
    }

    @Test
    void testCreate() {
        assertThrows(UnsupportedOperationException.class, () -> new LanguageDaoImpl(connection).create(new Language()));
    }

    @Test
    void testUpdate() {
        assertThrows(UnsupportedOperationException.class, () -> new LanguageDaoImpl(connection).update(new Language()));
    }

    private boolean isEqualLanguage(Language l1, Language l2) {
        return l1.getId() == l2.getId() &&
                l1.getName().equals(l2.getName()) &&
                l1.getCode().equals(l2.getCode());
    }
}