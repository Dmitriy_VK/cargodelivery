package org.finaltask.dao.impl;

import org.finaltask.dao.DBException;
import org.finaltask.entity.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class OrderDaoImplTest {
    private Connection connection;
    private PreparedStatement prdStatement;
    private ResultSet resultSet;
    private Order testOrder;
    private static final String ID = "id";
    private static final String ACCOUNT_ID = "account_id";
    private static final String LOGIN = "login";
    private static final String ADDRESS = "address";
    private static final String CARGO_TYPE_ID = "cargo_type_id";
    private static final String CARGO_TYPE_NAME = "cargo_type";
    private static final String WEIGHT = "weight";
    private static final String VOLUME = "volume";
    private static final String STATUS = "status";
    private static final String PRICE = "price";
    private static final String DELIVERY_DATE = "delivery_date";
    private static final String ROUTE_ID = "route_id";
    private static final String CREATE_TIME = "create_time";
    private static final String NAME = "name";
    private static final String SURNAME = "surname";
    private static final String CITY_ID_FROM = "city_id_from";
    private static final String CITY_ID_TO = "city_id_to";
    private static final String CITY_FROM = "city_from";
    private static final String CITY_TO = "city_to";
    private static final String DISTANCE = "distance";

    @BeforeEach
    void setUp() {
        connection = mock(Connection.class);
        prdStatement = mock(PreparedStatement.class);
        resultSet = mock(ResultSet.class);
        testOrder = new Order();
        testOrder.setId(25L);
        testOrder.setAccount(new Account());
        testOrder.getAccount().setAccountDetails(new AccountDetails());
        testOrder.setRoute(new Route());
        testOrder.setCargoType(new CargoType());
        testOrder.setId(25L);
        testOrder.getAccount().setId(32L);
        testOrder.setAddress("Main street");
        testOrder.getCargoType().setId(45L);
        testOrder.setWeight(5.7);
        testOrder.setVolume(3);
        testOrder.setDeliveryDate(Date.valueOf("2023-04-18"));
        testOrder.setOrderStatus(Status.PROCESSED);
        testOrder.setPrice(25.36);
        testOrder.getRoute().setId(15L);
        testOrder.setCreateTime(LocalDateTime.parse("2023-04-18T11:17:32"));
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void testFindAll() {
        assertThrows(UnsupportedOperationException.class, () -> new OrderDaoImpl(connection).findAll());
    }

    @Test
    void testFindEntityById() throws SQLException, DBException {
        when(connection.prepareStatement(any(String.class))).thenReturn(prdStatement);

        when(prdStatement.executeQuery()).thenReturn(resultSet);
        when(resultSet.next()).thenReturn(true).thenReturn(false);

        when(resultSet.getLong(ID)).thenReturn(25L);
        when(resultSet.getLong(ACCOUNT_ID)).thenReturn(32L);
        when(resultSet.getString(ADDRESS)).thenReturn("Main street");
        when(resultSet.getLong(CARGO_TYPE_ID)).thenReturn(45L);
        when(resultSet.getDouble(WEIGHT)).thenReturn(5.7);
        when(resultSet.getInt(VOLUME)).thenReturn(3);
        when(resultSet.getDate(DELIVERY_DATE)).thenReturn(Date.valueOf("2023-04-18"));
        when(resultSet.getString(STATUS)).thenReturn(Status.PROCESSED.name());
        when(resultSet.getDouble(PRICE)).thenReturn(25.36);
        when(resultSet.getLong(ROUTE_ID)).thenReturn(15L);
        when(resultSet.getTimestamp(CREATE_TIME)).thenReturn(Timestamp.valueOf("2023-04-18 11:17:32"));

        OrderDaoImpl orderDao = new OrderDaoImpl(connection);
        Order order = orderDao.findEntityById(25L);

        assertTrue(isEqualOrders(testOrder, order));
    }

    @Test
    void testDeleteById() {
        assertThrows(UnsupportedOperationException.class, () -> new OrderDaoImpl(connection).delete(25L));
    }

    @Test
    void testDeleteByEntity() {
        assertThrows(UnsupportedOperationException.class, () -> new OrderDaoImpl(connection).delete(new Order()));
    }

    @Test
    void testCreate() throws SQLException, DBException {
        when(connection.prepareStatement(any(String.class), anyInt())).thenReturn(prdStatement);

        when(prdStatement.executeUpdate()).thenReturn(1);
        when(prdStatement.getGeneratedKeys()).thenReturn(resultSet);
        when(resultSet.next()).thenReturn(true).thenReturn(false);

        OrderDaoImpl orderDao = new OrderDaoImpl(connection);
        assertTrue(orderDao.create(testOrder));
    }

    @Test
    void testUpdate() {
        assertThrows(UnsupportedOperationException.class, () -> new OrderDaoImpl(connection).update(new Order()));
    }

    @Test
    void testFindAllWithAccountDetailsAndCityNames() throws SQLException, DBException {
        when(connection.prepareStatement(any(String.class))).thenReturn(prdStatement);

        when(prdStatement.executeQuery()).thenReturn(resultSet);
        when(resultSet.next()).thenReturn(true).thenReturn(true).thenReturn(false);

        when(resultSet.getLong(ID)).thenReturn(25L);
        when(resultSet.getLong(ACCOUNT_ID)).thenReturn(32L);
        when(resultSet.getString(ADDRESS)).thenReturn("Main street");
        when(resultSet.getLong(CARGO_TYPE_ID)).thenReturn(45L);
        when(resultSet.getDouble(WEIGHT)).thenReturn(5.7);
        when(resultSet.getInt(VOLUME)).thenReturn(3);
        when(resultSet.getDate(DELIVERY_DATE)).thenReturn(Date.valueOf("2023-04-18"));
        when(resultSet.getString(STATUS)).thenReturn(Status.PROCESSED.name());
        when(resultSet.getDouble(PRICE)).thenReturn(25.36);
        when(resultSet.getLong(ROUTE_ID)).thenReturn(15L);
        when(resultSet.getTimestamp(CREATE_TIME)).thenReturn(Timestamp.valueOf("2023-04-18 11:17:32"));

        OrderDaoImpl orderDao = new OrderDaoImpl(connection);
        List<Order> orderList = orderDao.findAllWithAccountDetailsAndCityNames(
                25L,
                "orderBy",
                "sortOrder",
                "whereName",
                "whereValue",
                10,
                10);

        assertTrue(isEqualOrders(testOrder, orderList.get(0)));
        assertTrue(isEqualOrders(testOrder, orderList.get(1)));
    }

    @Test
    void testFindAllByCityWithAccountDetailsAndCityNames() throws SQLException, DBException {
        when(connection.prepareStatement(any(String.class))).thenReturn(prdStatement);

        when(prdStatement.executeQuery()).thenReturn(resultSet);
        when(resultSet.next()).thenReturn(true).thenReturn(true).thenReturn(false);

        when(resultSet.getLong(ID)).thenReturn(25L);
        when(resultSet.getLong(ACCOUNT_ID)).thenReturn(32L);
        when(resultSet.getString(ADDRESS)).thenReturn("Main street");
        when(resultSet.getLong(CARGO_TYPE_ID)).thenReturn(45L);
        when(resultSet.getDouble(WEIGHT)).thenReturn(5.7);
        when(resultSet.getInt(VOLUME)).thenReturn(3);
        when(resultSet.getDate(DELIVERY_DATE)).thenReturn(Date.valueOf("2023-04-18"));
        when(resultSet.getString(STATUS)).thenReturn(Status.PROCESSED.name());
        when(resultSet.getDouble(PRICE)).thenReturn(25.36);
        when(resultSet.getLong(ROUTE_ID)).thenReturn(15L);
        when(resultSet.getTimestamp(CREATE_TIME)).thenReturn(Timestamp.valueOf("2023-04-18 11:17:32"));

        OrderDaoImpl orderDao = new OrderDaoImpl(connection);
        List<Order> orderList = orderDao.findAllByCityWithAccountDetailsAndCityNames(
                25L,
                "orderBy",
                "sortOrder",
                "whereName",
                "whereValue",
                10,
                10);

        assertTrue(isEqualOrders(testOrder, orderList.get(0)));
        assertTrue(isEqualOrders(testOrder, orderList.get(1)));
    }

    @Test
    void testFindAllWithCityNamesByAccountId() throws SQLException, DBException {
        when(connection.prepareStatement(any(String.class))).thenReturn(prdStatement);

        when(prdStatement.executeQuery()).thenReturn(resultSet);
        when(resultSet.next()).thenReturn(true).thenReturn(true).thenReturn(false);

        when(resultSet.getLong(ID)).thenReturn(25L);
        when(resultSet.getLong(ACCOUNT_ID)).thenReturn(32L);
        when(resultSet.getString(ADDRESS)).thenReturn("Main street");
        when(resultSet.getLong(CARGO_TYPE_ID)).thenReturn(45L);
        when(resultSet.getDouble(WEIGHT)).thenReturn(5.7);
        when(resultSet.getInt(VOLUME)).thenReturn(3);
        when(resultSet.getDate(DELIVERY_DATE)).thenReturn(Date.valueOf("2023-04-18"));
        when(resultSet.getString(STATUS)).thenReturn(Status.PROCESSED.name());
        when(resultSet.getDouble(PRICE)).thenReturn(25.36);
        when(resultSet.getLong(ROUTE_ID)).thenReturn(15L);
        when(resultSet.getTimestamp(CREATE_TIME)).thenReturn(Timestamp.valueOf("2023-04-18 11:17:32"));

        OrderDaoImpl orderDao = new OrderDaoImpl(connection);
        List<Order> orderList = orderDao.findAllWithCityNamesByAccountId(
                25L,
                32L,
                "sortOrder",
                "whereName",
                Status.PROCESSED.name(),
                10,
                10);

        assertTrue(isEqualOrders(testOrder, orderList.get(0)));
        assertTrue(isEqualOrders(testOrder, orderList.get(1)));
    }

    @Test
    void testUpdateStatus() throws SQLException, DBException {
        when(connection.prepareStatement(any(String.class))).thenReturn(prdStatement);

        OrderDaoImpl orderDao = new OrderDaoImpl(connection);

        assertTrue(isEqualOrders(testOrder, orderDao.updateStatus(testOrder)));
    }

    @Test
    void testGetNumberOfRowsByAccountIdAndStatus() throws SQLException, DBException {
        when(connection.prepareStatement(any(String.class))).thenReturn(prdStatement);

        when(prdStatement.executeQuery()).thenReturn(resultSet);
        when(resultSet.next()).thenReturn(true).thenReturn(false);

        when(resultSet.getInt(1)).thenReturn(9);

        OrderDaoImpl orderDao = new OrderDaoImpl(connection);

        assertEquals(9, orderDao.getNumberOfRowsByAccountIdAndStatus(25L, Status.NEW.name()));
    }

    @Test
    void testGetNumberOfRowsWithCondition() throws SQLException, DBException {
        when(connection.prepareStatement(any(String.class))).thenReturn(prdStatement);

        when(prdStatement.executeQuery()).thenReturn(resultSet);
        when(resultSet.next()).thenReturn(true).thenReturn(false);

        when(resultSet.getInt(1)).thenReturn(9);

        OrderDaoImpl orderDao = new OrderDaoImpl(connection);

        assertEquals(9, orderDao.getNumberOfRowsWithCondition("whereName", "whereValue"));
    }

    @Test
    void getNumberOfRowsByCityLike() throws SQLException, DBException {
        when(connection.prepareStatement(any(String.class))).thenReturn(prdStatement);

        when(prdStatement.executeQuery()).thenReturn(resultSet);
        when(resultSet.next()).thenReturn(true).thenReturn(false);

        when(resultSet.getInt(1)).thenReturn(9);

        OrderDaoImpl orderDao = new OrderDaoImpl(connection);

        assertEquals(9, orderDao.getNumberOfRowsByCityLike("whereName", "whereValue"));
    }

    private boolean isEqualOrders(Order o1, Order o2) {
        return o1.getId() == o2.getId() &&
                o1.getAccount().getId() == o2.getAccount().getId() &&
                o1.getAddress().equals(o2.getAddress()) &&
                o1.getCargoType().getId() == o2.getCargoType().getId() &&
                Double.compare(o1.getWeight(), o2.getWeight()) == 0 &&
                o1.getVolume() == o2.getVolume() &&
                o1.getDeliveryDate().equals(o2.getDeliveryDate()) &&
                o1.getOrderStatus() == o2.getOrderStatus() &&
                Double.compare(o1.getPrice(), o2.getPrice()) == 0 &&
                o1.getRoute().getId() == o2.getRoute().getId() &&
                o1.getCreateTime().equals(o2.getCreateTime());
    }
}