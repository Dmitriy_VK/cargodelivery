package org.finaltask.dao.impl;

import org.finaltask.dao.DBException;
import org.finaltask.entity.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class AccountDaoImplTest {
    private Connection connection;
    private PreparedStatement prdStatement;
    private ResultSet resultSet;
    private Account testAccount;
    private static final String ID = "id";
    private static final String LOGIN = "login";
    private static final String PASSWORD = "password";
    private static final String ROLE_ID = "role_id";
    private static final String ROLE_NAME = "role";
    private static final String WALLET_ID = "wallet_id";
    private static final String VALUE = "value";
    private static final String ACCOUNT_DETAILS_ID = "account_details_id";
    private static final String NAME = "name";
    private static final String SURNAME = "surname";
    private static final String EMAIL = "email";
    private static final String PHONE = "phone";
    private static final String CITY = "city";

    @BeforeEach
    void init() {
        connection = mock(Connection.class);
        prdStatement = mock(PreparedStatement.class);
        resultSet = mock(ResultSet.class);
        testAccount = new Account();
        testAccount.setId(25L);
        testAccount.setLogin("Login");
        testAccount.setPassword("12345");
        testAccount.setRole(new Role());
        testAccount.getRole().setName(RoleEnum.MANAGER);
        testAccount.getRole().setId(2L);
        testAccount.setWallet(new Wallet());
        testAccount.getWallet().setId(23L);
        testAccount.setAccountDetails(new AccountDetails());
        testAccount.getAccountDetails().setId(3L);
    }
    
    @Test
    void testFindAll() {
        assertThrows(UnsupportedOperationException.class, () -> new AccountDaoImpl(connection).findAll());
    }

    @Test
    void testFindEntityById() throws SQLException, DBException {
        when(connection.prepareStatement(any(String.class))).thenReturn(prdStatement);
        
        when(prdStatement.executeQuery()).thenReturn(resultSet);
        when(resultSet.next()).thenReturn(true).thenReturn(false);
        
        when(resultSet.getLong(ID)).thenReturn(25L);
        when(resultSet.getString(LOGIN)).thenReturn("Login");
        when(resultSet.getString(PASSWORD)).thenReturn("12345");
        when(resultSet.getLong(ROLE_ID)).thenReturn(2L);
        when(resultSet.getLong(WALLET_ID)).thenReturn(23L);
        when(resultSet.getLong(ACCOUNT_DETAILS_ID)).thenReturn(3L);

        AccountDaoImpl accountDao = new AccountDaoImpl(connection);
        Account account = accountDao.findEntityById(2);
        assertTrue(isEqual(testAccount, account));
    }
    
    @Test
    void testDeleteById() {
        assertThrows(UnsupportedOperationException.class, () -> new AccountDaoImpl(connection).delete(25L));
    }
    
    @Test
    void testDeleteByAccount() {
        assertThrows(UnsupportedOperationException.class, () -> new AccountDaoImpl(connection).delete(testAccount));
    }
    
    @Test
    void testUpdate() {
        assertThrows(UnsupportedOperationException.class, () -> new AccountDaoImpl(connection).update(testAccount));
    }
    
    @Test
    void testUpdateFieldThrowDBException() throws SQLException {
    	when(connection.prepareStatement(any(String.class))).thenReturn(prdStatement);
    	when(prdStatement.executeUpdate()).thenThrow(new SQLException());
    	
    	assertThrows(DBException.class, () -> new AccountDaoImpl(connection).updateField(25L, "key", "value"));
    }
    

    @Test
    void testCreate() throws DBException, SQLException {
        when(connection.prepareStatement(any(String.class), anyInt())).thenReturn(prdStatement);
        when(resultSet.next()).thenReturn(true).thenReturn(false);
        when(prdStatement.executeUpdate()).thenReturn(1);
        when(prdStatement.getGeneratedKeys()).thenReturn(resultSet);

        AccountDaoImpl accountDao = new AccountDaoImpl(connection);
        assertTrue(accountDao.create(testAccount));
    }


    @Test
    void testFindEntityByLogin() throws SQLException, DBException {
    	when(connection.prepareStatement(any(String.class))).thenReturn(prdStatement);
    	
    	when(prdStatement.executeQuery()).thenReturn(resultSet);
        when(resultSet.next()).thenReturn(true).thenReturn(false);
    	
    	when(resultSet.getLong(ID)).thenReturn(25L);
        when(resultSet.getString(LOGIN)).thenReturn("Login");
        when(resultSet.getString(PASSWORD)).thenReturn("12345");
        when(resultSet.getLong(ROLE_ID)).thenReturn(2L);
        when(resultSet.getLong(WALLET_ID)).thenReturn(23L);
        when(resultSet.getLong(ACCOUNT_DETAILS_ID)).thenReturn(3L);
        
        
        
        AccountDaoImpl accountDao = new AccountDaoImpl(connection);
        Account account = accountDao.findEntityByLogin("Login");
        assertTrue(isEqual(testAccount, account));
    	
    }

    @Test
    void testFindEntityByRole() throws DBException, SQLException {
    	when(connection.prepareStatement(any(String.class))).thenReturn(prdStatement);
    	
    	when(prdStatement.executeQuery()).thenReturn(resultSet);
        when(resultSet.next()).thenReturn(true).thenReturn(false);
    	
    	when(resultSet.getLong(ID)).thenReturn(25L);
        when(resultSet.getString(LOGIN)).thenReturn("Login");
        when(resultSet.getString(PASSWORD)).thenReturn("12345");
        when(resultSet.getLong(ROLE_ID)).thenReturn(2L);
        when(resultSet.getLong(WALLET_ID)).thenReturn(23L);
        when(resultSet.getLong(ACCOUNT_DETAILS_ID)).thenReturn(3L);
        
        AccountDaoImpl accountDao = new AccountDaoImpl(connection);
        Account account = accountDao.findEntityByRole(RoleEnum.USER);
        assertTrue(isEqual(testAccount, account));
    }
    
    @Test
    void testFindAllWithAccountDetails() throws DBException, SQLException {
    	when(connection.prepareStatement(any(String.class))).thenReturn(prdStatement);
    	
    	when(prdStatement.executeQuery()).thenReturn(resultSet);
        when(resultSet.next()).thenReturn(true).thenReturn(true).thenReturn(false);
    	
    	when(resultSet.getLong(ID)).thenReturn(25L);
        when(resultSet.getString(LOGIN)).thenReturn("Login");
        when(resultSet.getLong(ROLE_ID)).thenReturn(2L);
        when(resultSet.getString(ROLE_NAME)).thenReturn("manager");
        when(resultSet.getLong(WALLET_ID)).thenReturn(23L);
        when(resultSet.getDouble(VALUE)).thenReturn(5.23);
        when(resultSet.getLong(ACCOUNT_DETAILS_ID)).thenReturn(3L);
        when(resultSet.getString(NAME)).thenReturn("Dmytro");
        when(resultSet.getString(SURNAME)).thenReturn("Petrov");
        when(resultSet.getString(EMAIL)).thenReturn("myemail@gmail.com");
        when(resultSet.getString(PHONE)).thenReturn("+380852562389");
        when(resultSet.getString(CITY)).thenReturn("Kyiv");

        testAccount.getRole().setName(RoleEnum.MANAGER);
        testAccount.getWallet().setValue(5.23);
        testAccount.getAccountDetails().setName("Dmytro");
        testAccount.getAccountDetails().setSurname("Petrov");
        testAccount.getAccountDetails().setEmail("myemail@gmail.com");
        testAccount.getAccountDetails().setPhone("+380852562389");
        testAccount.getAccountDetails().setCity("Kyiv");
        
        AccountDaoImpl accountDao = new AccountDaoImpl(connection);
        List<Account> accountList = accountDao.findAllWithAccountDetails(RoleEnum.USER, "id", "DESC", 5, 1);

        assertTrue(isEqualFullAccount(testAccount, accountList.get(0)));
        assertTrue(isEqualFullAccount(testAccount, accountList.get(1)));
    }

    private boolean isEqual(Account a1, Account a2) {
        return a1.getId() == a2.getId() &&
                a1.getLogin().equals(a2.getLogin()) &&
                a1.getPassword().equals(a2.getPassword()) &&
                a1.getRole().getId() == a2.getRole().getId() &&
                a1.getWallet().getId() == a2.getWallet().getId() &&
                a1.getAccountDetails().getId() == a2.getAccountDetails().getId();
    }
    
    private boolean isEqualFullAccount(Account a1, Account a2) {
        return a1.getId() == a2.getId() &&
                a1.getLogin().equals(a2.getLogin()) &&
                a1.getRole().getId() == a2.getRole().getId() &&
                a1.getWallet().getId() == a2.getWallet().getId() &&
                a1.getAccountDetails().getId() == a2.getAccountDetails().getId() &&
                a1.getRole().getName() == a2.getRole().getName() &&
                a1.getWallet().getValue() == a2.getWallet().getValue() &&
                a1.getAccountDetails().getName().equals(a2.getAccountDetails().getName()) &&
                a1.getAccountDetails().getSurname().equals(a2.getAccountDetails().getSurname()) &&
                a1.getAccountDetails().getEmail().equals(a2.getAccountDetails().getEmail()) &&
                a1.getAccountDetails().getPhone().equals(a2.getAccountDetails().getPhone()) &&
                a1.getAccountDetails().getCity().equals(a2.getAccountDetails().getCity());
    }
}