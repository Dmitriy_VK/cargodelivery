package org.finaltask.dao.impl;

import org.finaltask.dao.DBException;
import org.finaltask.entity.CargoType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.sql.*;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class CargoTypeDaoImplTest {
    private Connection connection;
    private PreparedStatement prdStatement;
    private Statement statement;
    private ResultSet resultSet;
    private CargoType testCargoType;
    static final String ID = "id";
    static final String NAME = "name";
    static final String RATIO = "ratio";
    static final String TRANSLATION = "translation";
    @BeforeEach
    void init() {
        connection = mock(Connection.class);
        resultSet = mock(ResultSet.class);
        testCargoType = new CargoType();
        testCargoType.setId(25L);
        testCargoType.setName("cargo");
        testCargoType.setRatio(0.75);
    }

    @Test
    void testFindAll() throws SQLException, DBException {
        statement = mock(Statement.class);

        when(connection.createStatement()).thenReturn(statement);

        when(statement.executeQuery(any(String.class))).thenReturn(resultSet);

        when(resultSet.next()).thenReturn(true).thenReturn(true).thenReturn(false);
        when(resultSet.getLong(ID)).thenReturn(25L).thenReturn(32L);
        when(resultSet.getString(NAME)).thenReturn("cargo").thenReturn("parcel");
        when(resultSet.getDouble(RATIO)).thenReturn(0.75).thenReturn(0.5);

        CargoTypeDaoImpl cargoTypeDao = new CargoTypeDaoImpl(connection);
        List<CargoType> cargoTypeList = cargoTypeDao.findAll();

        assertTrue(isEqualsCargoType(testCargoType, cargoTypeList.get(0)));

        testCargoType.setId(32L);
        testCargoType.setName("parcel");
        testCargoType.setRatio(0.5);

        assertTrue(isEqualsCargoType(testCargoType, cargoTypeList.get(1)));
    }

    @Test
    void testFindAllWithTranslatedNames() throws SQLException, DBException {
        prdStatement = mock(PreparedStatement.class);

        when(connection.prepareStatement(any(String.class))).thenReturn(prdStatement);

        when(prdStatement.executeQuery()).thenReturn(resultSet);

        when(resultSet.next()).thenReturn(true).thenReturn(true).thenReturn(false);
        when(resultSet.getLong(ID)).thenReturn(25L).thenReturn(32L);
        when(resultSet.getString(TRANSLATION)).thenReturn("cargo").thenReturn("parcel");
        when(resultSet.getDouble(RATIO)).thenReturn(0.75).thenReturn(0.5);

        CargoTypeDaoImpl cargoTypeDao = new CargoTypeDaoImpl(connection);
        List<CargoType> cargoTypeList = cargoTypeDao.findAllWithTranslatedNames(2L);

        assertTrue(isEqualsCargoType(testCargoType, cargoTypeList.get(0)));

        testCargoType.setId(32L);
        testCargoType.setName("parcel");
        testCargoType.setRatio(0.5);

        assertTrue(isEqualsCargoType(testCargoType, cargoTypeList.get(1)));
    }

    @Test
    void testFindEntityById() throws SQLException, DBException {
        prdStatement = mock(PreparedStatement.class);

        when(connection.prepareStatement(any(String.class))).thenReturn(prdStatement);

        when(prdStatement.executeQuery()).thenReturn(resultSet);

        when(resultSet.next()).thenReturn(true).thenReturn(false);
        when(resultSet.getLong(ID)).thenReturn(25L);
        when(resultSet.getString(NAME)).thenReturn("cargo");
        when(resultSet.getDouble(RATIO)).thenReturn(0.75);

        CargoTypeDaoImpl cargoTypeDao = new CargoTypeDaoImpl(connection);
        CargoType cargoType = cargoTypeDao.findEntityById(25L);

        assertTrue(isEqualsCargoType(testCargoType, cargoType));
    }

    @Test
    void testFindEntityByIdWithTranslatedName() throws SQLException, DBException {
        prdStatement = mock(PreparedStatement.class);

        when(connection.prepareStatement(any(String.class))).thenReturn(prdStatement);

        when(prdStatement.executeQuery()).thenReturn(resultSet);

        when(resultSet.next()).thenReturn(true).thenReturn(false);
        when(resultSet.getLong(ID)).thenReturn(25L);
        when(resultSet.getString(TRANSLATION)).thenReturn("cargo");
        when(resultSet.getDouble(RATIO)).thenReturn(0.75);

        CargoTypeDaoImpl cargoTypeDao = new CargoTypeDaoImpl(connection);
        CargoType cargoType = cargoTypeDao.findEntityByIdWithTranslatedName(25L, 2L);

        assertTrue(isEqualsCargoType(testCargoType, cargoType));
    }

    @Test
    void testDeleteById() {
        assertThrows(UnsupportedOperationException.class, () -> new CargoTypeDaoImpl(connection).delete(25L));
    }

    @Test
    void testDeleteByEntity() {
        assertThrows(UnsupportedOperationException.class, () -> new CargoTypeDaoImpl(connection).delete(new CargoType()));
    }

    @Test
    void create() {
        assertThrows(UnsupportedOperationException.class, () -> new CargoTypeDaoImpl(connection).create(new CargoType()));
    }

    @Test
    void update() {
        assertThrows(UnsupportedOperationException.class, () -> new CargoTypeDaoImpl(connection).update(new CargoType()));
    }
    private boolean isEqualsCargoType(CargoType c1, CargoType c2) {
        return c1.getId() == c2.getId() &&
                c1.getName().equals(c2.getName()) &&
                Double.compare(c1.getRatio(), c2.getRatio()) == 0;
    }
}